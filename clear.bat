del draco_py\_draco.cpp
del draco_py\_draco.pyd
del draco\extension\decode.cpp
del draco\extension\encode.cpp
del draco\extension\internal.cpp

rd /s /q build
rd /s /q dist
rd /s /q draco_python.egg-info
pip uninstall draco-python -y