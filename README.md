# draco-python : draco-like API python module

[![GitHub license](https://img.shields.io/github/license/draco_py/draco_py.svg)](https://github.com/draco_py/draco_py)
[![travis](https://travis-ci.org/Sirokujira/draco_py.svg?branch=master)](https://travis-ci.org/Sirokujira/draco_py)
[![coveralls](https://img.shields.io/coveralls/draco_py/draco_py.svg)](https://coveralls.io/github/draco_py/draco_py)
[![Read the Docs](https://readthedocs.org/projects/draco_py/badge/?version=stable)](https://docs-draco_py.chainer.org/en/stable/)


## Installation

For detailed instructions on installing draco-python, see [the installation guide](https://rtd/en/stable/install.html).

You can install draco-python using `pip`:

```sh
(Source Package)
$ pip install draco-python
```

## License

MIT License (see `LICENSE` file).

draco_py is designed based on draco API (see `docs/LICENSE_THIRD_PARTY` file).


