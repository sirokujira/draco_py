.. _draco-python_reference:

*****************************
draco-python Reference Manual
*****************************

This is the official reference of draco-python, a Mesh/PointCloud Files Decode/Encode interface.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Reference
=========

.. module:: draco_py

.. toctree::
   :maxdepth: 2

   draco_py