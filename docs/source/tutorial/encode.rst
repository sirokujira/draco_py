Encode of draco-python
--------------

.. currentmodule:: draco-python

In this section, you will learn about the following things:

* Encode of :class:`draco-python.Encoder`
* Set Encode Option

Encode of draco-python
~~~~~~~~~~~~~~~~~~~~~~

draco-python is a Mesh/PointCloud Files Decode/Encode interface.
In the following code, cp is an abbreviation of draco-python, as np is numpy as is customarily done:

.. doctest::

   >>> import draco-py as draco


You can see its creation of identical to ``draco``'s one, except that ``draco`` is replaced with ``draco-py``.


binaryfile readdata of draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

Encode Option setting draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

Binarydata encode of draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

