Decode of draco-python
--------------

.. currentmodule:: draco-python

In this section, you will learn about the following things:

* Decode of :class:`draco-python/Decoder`
* Set Decode Option

Decode of draco-python
~~~~~~~~~~~~~~~~~~~~~~

draco-python is a Mesh/PointCloud Files Decode/Encode interface.
In the following code, cp is an abbreviation of draco-python, as np is numpy as is customarily done:

.. doctest::

   >>> import draco-py as draco


You can see its creation of identical to ``draco``'s one, except that ``draco`` is replaced with ``draco-py``.


binaryfile readdata of draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

Decode Option setting draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

Binarydata Decode of draco-python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doctest::

   >>> import draco-py as draco

