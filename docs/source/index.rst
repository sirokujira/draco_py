============================================
draco-python -- draco-like API
============================================

This is the `draco-python <https://github.com/sirokujira/draco-python>`_ documentation.

.. module:: draco-py

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   overview
   install
   tutorial/index
   reference/index
   developers
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
