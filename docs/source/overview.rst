.. _overview:

draco-python Overview
=============

.. module:: draco-python

`draco-python <https://github.com/sirokujira/draco-python>`_ is an implementation of draco-compatible interface.

The following is a brief overview of supported subset of draco interface:

- Encode
- Decode

