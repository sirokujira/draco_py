Installation Guide
==================

.. contents:: :local:

Recommended Environments
------------------------

We recommend these Linux distributions.

* `Ubuntu <http://www.ubuntu.com/>`_ 14.04/16.04 LTS 64bit
* `MacOS <https://www.apple.com/macos/>`_ 10.10/10.11
* `Windows <https://www.microsoft.com/>`_ 7/8.1/10 64bit

The following versions of Python can be used: 3.5.0+, and 3.6.0+.

.. note::

   We are testing draco-python automatically with Jenkins, where all the above *recommended* environments are tested.

draco-python is supported on Python 3.5.0+, 3.6.0+.
draco-python uses C++ compiler such as g++.
You need to install it before installing draco-python.
This is typical installation method for each platform::

  # Ubuntu 14.04
  $ apt-get install g++
  export PATH=;%PATH%
  
  # MacOS 
  export PATH=;%PATH%

  # Windows
  `Visual Studio 2015 C++ Compiler Tools <https://www.visualstudio.com/vs/older-downloads/>`_ 
  On a Windows box you would run the following command to generate Visual Studio 2015 projects:
  C:\Users\nobody> mkdir build
  C:\Users\nobody> cd build
  C:\Users\nobody> cmake path/to/draco -G "Visual Studio 14 2015"
  To generate 64-bit Windows Visual Studio 2015 projects:
  C:\Users\nobody> cmake path/to/draco -G "Visual Studio 14 2015 Win64"
  `Visual Studio 2017 C++ Compiler Tools <https://www.visualstudio.com/downloads/>`_
  On a Windows box you would run the following command to generate Visual Studio 2017 projects:
  C:\Users\nobody> cmake path/to/draco -G "Visual Studio 15 2017"
  To generate 64-bit Windows Visual Studio 2015 projects:
  C:\Users\nobody> cmake path/to/draco -G "Visual Studio 15 2017 Win64"

  git clone https://github.com/sirokujira/draco_py
  cd draco_py
  python setup.py install

If you use old ``setuptools``, upgrade it::

  $ pip install -U setuptools


Dependencies
------------

Before installing draco-python, we recommend to upgrade ``setuptools`` if you are using an old one::

  $ pip install -U setuptools

The following Python packages are required to install draco-python.
The latest version of each package will automatically be installed if missing.

* `draco <https://github.com/google/draco/>`_ 0.0.4


Install draco-python
--------------------

Install draco-python via pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We recommend to install draco-python via pip::

  $ pip install draco-python

.. note::

   All optional draco related libraries, need to be installed before installing draco-python.
   After you update these libraries, please reinstall draco-python because you need to compile and link to the newer version of them.


Install draco-python from source
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The tarball of the source tree is available via ``pip download draco-python`` or from `the release notes page <https://github.com/sirokujira/draco-python/releases>`_.
You can use ``setup.py`` to install draco-python from the tarball::

  $ tar zxf draco-python-x.x.x.tar.gz
  $ cd draco-python-x.x.x
  $ python setup.py install

You can also install the development version of draco-python from a cloned Git repository::

  $ git clone https://github.com/google/draco-python.git
  $ cd draco/Python
  $ python setup.py install


.. _install_error:

When an error occurs...
~~~~~~~~~~~~~~~~~~~~~~~

Use ``-vvvv`` option with ``pip`` command.
That shows all logs of installation.
It may help you::

  $ pip install draco-python -vvvv


.. _install_draco:

Build draco-python with draco.lib
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

draco-python uses draco functions.
draco-python depends static library.
cmake create path build/Release Folder.
cmake command execute build Folder Path.

  # Ubuntu 14.04
  $ cmake -DCMAKE_BUILD_TYPE=release ..
  $ make
  
  # MacOSX
  $ cmake -G Xcode -DCMAKE_BUILD_TYPE=release ..
  $ xcodebuild 
  
  # Windows
  $ cmake -G "Visual Studio 14 2015 Win64"  -DCMAKE_BUILD_TYPE=release ..
  $ msbuild draco.sln

Install draco-python for developers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

draco-python uses Cython (>=0.25).
Developers need to use Cython to regenerate C++ sources from ``pyx`` files.
We recommend to use ``pip`` with ``-e`` option for editable mode::

  $ pip install -U cython
  $ cd /path/to/draco-python/source
  $ pip install -e .

Users need not to install Cython as a distribution package of draco-python only contains generated sources.


Uninstall draco-python
--------------

Use pip to uninstall draco-python::

  $ pip uninstall draco-python

.. note::

   When you upgrade draco-python, ``pip`` sometimes install the new version without removing the old one in ``site-packages``.
   In this case, ``pip uninstall`` only removes the latest one.
   To ensure that Chainer is completely removed, run the above command repeatedly until ``pip`` returns an error.


Upgrade draco-python
--------------------

Just use ``pip`` with ``-U`` option::

  $ pip install -U draco-python


Reinstall draco-python
----------------------

If you want to reinstall draco-python, please uninstall draco-python and then install it.
We recommend to use ``--no-cache-dir`` option as ``pip`` sometimes uses cache::

  $ pip uninstall draco-python
  $ pip install draco-python --no-cache-dir
When you install draco-python without draco, and after that you want to use draco, please reinstall draco-python.
You need to reinstall draco-python when you want to upgrade draco.

FAQ
---

