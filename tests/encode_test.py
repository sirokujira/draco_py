import sys
import os.path
import unittest

import draco_py as draco

from nose.plugins.attrib import attr


@attr('next_releases')
class PyEncodeTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.ObjDecoder()

    # std::unique_ptr<draco::Mesh> CreateTestMesh() const
    def _CreateTestMesh(self):
        mesh_builder = draco.TriangleSoupMeshBuilder()

        # Create a simple mesh with one face.
        mesh_builder.Start(1)

        # Add one position attribute and two texture coordinate attributes.
        pos_att_id = mesh_builder.AddAttribute(
            draco.GeometryAttributeType_POSITION, 3, draco.DataType_DT_FLOAT32)
        tex_att_id_0 = mesh_builder.AddAttribute(
            draco.GeometryAttributeType_TEX_COORD, 2, draco.DataType_DT_FLOAT32)
        tex_att_id_1 = mesh_builder.AddAttribute(
            draco.GeometryAttributeType_TEX_COORD, 2, draco.DataType_DT_FLOAT32)

#         # Initialize the attribute values.
#         mesh_builder.SetAttributeValuesForFace(
#             pos_att_id, int(0),
#             draco.Vector3f(0.f, 0.f, 0.f).data(),
#             draco.Vector3f(1.f, 0.f, 0.f).data(),
#             draco.Vector3f(1.f, 1.f, 0.f).data()
#             )
#
#         mesh_builder.SetAttributeValuesForFace(
#             tex_att_id_0, /* draco.FaceIndex(0)*/ int(0), draco.Vector2f(0.f, 0.f).data(),
#             draco.Vector2f(1.f, 0.f).data(), draco.Vector2f(1.f, 1.f).data());
#
#         mesh_builder.SetAttributeValuesForFace(
#             tex_att_id_1, /* draco.FaceIndex(0)*/ int(0), draco.Vector2f(0.f, 0.f).data(),
#             draco.Vector2f(1.f, 0.f).data(), draco.Vector2f(1.f, 1.f).data());
#
#         return mesh_builder.Finalize()
        return None

    # int GetQuantizationBitsFromAttribute(const draco::PointAttribute *att) const {
    def _GetQuantizationBitsFromAttribute(self, attr):

        if attr == None:
            return -1

        transform = draco.AttributeQuantizationTransform()
        if transform.InitFromAttribute(attr) == False:
            return -1

        return transform.quantization_bits()

    # void
    # def _VerifyNumQuantizationBits(const draco::EncoderBuffer &buffer,
    #                                  int pos_quantization,
    #                                  int tex_coord_0_quantization,
    #                                  int tex_coord_1_quantization) const {
    def _VerifyNumQuantizationBits(self, buffer,
                                   pos_quantization,
                                   tex_coord_0_quantization,
                                   tex_coord_1_quantization):
        decoder = draco.Decoder()

        # Skip the dequantization for the attributes which will allow us to get
        # the number of quantization bits used during encoding.
        decoder.SetSkipAttributeTransform(draco.GeometryAttributeType_POSITION)
        decoder.SetSkipAttributeTransform(
            draco.GeometryAttributeType_TEX_COORD)

        in_buffer = draco.DecoderBuffer()
        in_buffer.Init(buffer.data(), buffer.size())

        # auto mesh = decoder.DecodeMeshFromBuffer(&in_buffer).value()
        mesh = decoder.DecodeMeshFromBuffer(in_buffer)

        self.assertNotEqual(mesh, None)
        self.assertEqual(self._GetQuantizationBitsFromAttribute(
            mesh.attribute(0)), pos_quantization)
        self.assertEqual(self._GetQuantizationBitsFromAttribute(
            mesh.attribute(1)), tex_coord_0_quantization)
        self.assertEqual(self._GetQuantizationBitsFromAttribute(
            mesh.attribute(2)), tex_coord_1_quantization)

    def testExpertEncoderQuantization(self):
        # This test verifies that the expert encoder can quantize individual
        # attributes even if they have the same type.
        # auto mesh = self._CreateTestMesh()
        mesh = self._CreateTestMesh()
        self.assertNotEqual(mesh, None)

        # draco::ExpertEncoder encoder(*mesh.get());
        encoder = draco.ExpertEncoder()
        encoder.SetAttributeQuantization(0, 16)  # Position quantization.
        encoder.SetAttributeQuantization(1, 15)  # Tex-coord 0 quantization.
        encoder.SetAttributeQuantization(2, 14)  # Tex-coord 1 quantization.

        # draco::EncoderBuffer buffer;
        buffer = draco.EncoderBuffer()
        # encoder.EncodeToBuffer(&buffer);
        encoder.EncodeToBuffer(buffer)
        self._VerifyNumQuantizationBits(buffer, 16, 15, 14)

    def testEncoderQuantization(self):
        # This test verifies that Encoder applies the same quantization to all
        # attributes of the same type.
        # auto mesh = self._CreateTestMesh();
        mesh = self._CreateTestMesh()
        self.assertNotEqual(mesh, None)

        encoder = draco.Encoder()
        encoder.SetAttributeQuantization(
            draco.GeometryAttributeType_POSITION, 16)
        encoder.SetAttributeQuantization(
            draco.GeometryAttributeType_TEX_COORD, 15)

        buffer = draco.EncoderBuffer()
        # encoder.EncodeMeshToBuffer(*mesh.get(), &buffer)
        mesh = encoder.EncodeMeshToBuffer(buffer)
        self._VerifyNumQuantizationBits(buffer, 16, 15, 15)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyEncodeTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
