# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils

# base : obj_decoder_test.cc


class PyObjDecoderTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.ObjDecoder()

#       template <class Geometry>
#     def std::unique_ptr<Geometry> _DecodeObj(const std::string &file_name) const {
#         const std::string path = GetTestFileFullPath(file_name);
#         ObjDecoder decoder;
#         std::unique_ptr<Geometry> geometry(new Geometry());
#         if (!decoder.DecodeFromFile(path, geometry.get()))
#           return None
#         return geometry;

#     def _DecodeObjWithMetadata(self, file_name):
#         infile = open(file_name, 'rb')
#         data = infile.read()
#         data_len = len(data)
#
#         buffer = draco.DecoderBuffer()
#         buffer.Init(data)
#         geom_type = draco.GetEncodedGeometryType(buffer)
#
#         mesh = None
#         if geom_type == draco.EncodedGeometryType_TRIANGULAR_MESH:
#             mesh = draco.DecodeMeshFromBuffer(buffer)
#         elif geom_type == draco.EncodedGeometryType_POINT_CLOUD:
#             pc = draco.DecodePointCloudFromBuffer(buffer)
#         else:
#             mesh = None
#
#         return mesh

    def _test_decoding(self, file_name):
        filepath = test_utils.GetTestFileFullPath(file_name)

        # const std::unique_ptr<Mesh> mesh(_DecodeObj<Mesh>(filepath));
        mesh = draco.Mesh()
        self.dec.DecodeFromFile_Mesh(filepath, mesh)
        # << "Failed to load test model " << filepath;
        self.assertNotEqual(mesh, None)
        self.assertGreater(mesh.num_faces(), 0)

        # const std::unique_ptr<PointCloud> pc(_DecodeObj<PointCloud>(filepath));
        # pc = mesh.points
        # self.assertNotEqual(pc, None) # << "Failed to load test model " << filepath;
        # self.assertGreater(pc.num_points(), 0u)
        # self.assertGreater(mesh.num_points(), 0)

    def testExtraVertexOBJ(self):
        file_name = 'extra_vertex.obj'
        self._test_decoding(file_name)

    def testParialAttributesOBJ(self):
        file_name = 'cube_att_partial.obj'
        self._test_decoding(file_name)

    def testSubObjects(self):
        # Tests loading an Obj with sub objects.
        file_name = 'cube_att_sub_o.obj'
        filepath = test_utils.GetTestFileFullPath(file_name)

        # const std::unique_ptr<Mesh> mesh(DecodeObj<Mesh>(file_name));
        mesh = draco.Mesh()
        flag = self.dec.DecodeFromFile_Mesh(filepath, mesh)
        self.assertEqual(flag, True)
        # << "Failed to load test model " << file_name;
        self.assertNotEqual(mesh, None)
        self.assertGreater(mesh.num_faces(), 0)

        # A sub object attribute should be the fourth attribute of the mesh (in this case).
        self.assertEqual(mesh.num_attributes(), 4)
        # self.assertEqual(mesh.attribute(3).Attribute_type(), GeometryAttribute::GENERIC);
        self.assertEqual(mesh.attribute(3).Attribute_type(),
                         draco.GeometryAttributeType_GENERIC)

        # There should be 3 different sub objects used in the model.
        self.assertEqual(mesh.attribute(3).GetSize(), 3)
        # Verify that the sub object attribute has custom id == 1.
        # self.assertEqual(mesh.attribute(3).Custom_id(), 1);

    def testQuadOBJ(self):
        # Tests loading an Obj with quad faces.
        file_name = 'cube_quads.obj'
        filepath = test_utils.GetTestFileFullPath(file_name)
        # const std::unique_ptr<Mesh> mesh(DecodeObj<Mesh>(file_name));
        mesh = draco.Mesh()
        flag = self.dec.DecodeFromFile_Mesh(filepath, mesh)
        self.assertEqual(flag, True)
        # << "Failed to load test model " << file_name;
        self.assertNotEqual(mesh, None)
        self.assertEqual(mesh.num_faces(), 12)

        self.assertEqual(mesh.num_attributes(), 3)
        # Four points per quad face.
        self.assertEqual(mesh.num_points(), 4 * 6)

    def testComplexPolyOBJ(self):
        # Tests that we fail to load an obj with complex polygon (expected failure).
        file_name = 'complex_poly.obj'
        filepath = test_utils.GetTestFileFullPath(file_name)
        # const std::unique_ptr<Mesh> mesh(DecodeObj<Mesh>(file_name));
        mesh = draco.Mesh()
        flag = self.dec.DecodeFromFile_Mesh(filepath, mesh)
        self.assertEqual(flag, False)
        self.assertNotEqual(mesh, None)
        self.assertEqual(mesh.num_faces(), 0)

    def testObjDecodingAll(self):
        self._test_decoding('bunny_norm.obj')
        self._test_decoding('cube_att.obj')
        self._test_decoding('cube_att_partial.obj')
        self._test_decoding('cube_att_sub_o.obj')
        self._test_decoding('cube_quads.obj')
        # NG
        # self._test_decoding('cube_subd.obj')
        # self._test_decoding('eof_test.obj')

        # self._test_decoding('extra_vertex.obj')
        # self._test_decoding('mat_test.obj')
        # self._test_decoding('one_face_123.obj')
        # self._test_decoding('one_face_312.obj')
        # self._test_decoding('one_face_321.obj')
        # self._test_decoding('sphere.obj')
        # self._test_decoding('test_nm.obj')
        # self._test_decoding('test_nm_trans.obj')
        # self._test_decoding('test_sphere.obj')
        # self._test_decoding('three_faces_123.obj')
        # self._test_decoding('three_faces_312.obj')
        # self._test_decoding('two_faces_123.obj')
        # self._test_decoding('two_faces_312.obj')


def _encode(path):
    # Encode path for use in C++.
    if isinstance(path, bytes):
        return path
    else:
        return path.encode(sys.getfilesystemencoding())


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyObjDecoderTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
