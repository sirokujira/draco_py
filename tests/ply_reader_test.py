# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils


from nose.plugins.attrib import attr


@attr('next_releases')
class PyPlyReaderTest(unittest.TestCase):

    def setUp(self):
        pass

    # protected:
    # std::vector<char> ReadPlyFile(const std::string &file_name) const {
    def _ReadPlyFile(self, file_name):
        file_path = test_utils.GetTestFileFullPath(file_name)
        # std::ifstream file(path.c_str(), std::ios::binary);
        # if (!file)
        #   return std::vector<char>();
        #
        # auto is_size = file.tellg();
        # file.seekg(0, std::ios::end);
        # is_size = file.tellg() - is_size;
        # file.seekg(0, std::ios::beg);
        # std::vector<char> data(is_size);
        # file.read(&data[0], is_size);
        infile = open(file_path, 'rb')
        data = infile.read()
        data_len = len(data)
        return data, data_len

    def testReader(self):
        file_name = "test_pos_color.ply"
        # const std::vector<char> data = ReadPlyFile(file_name);
        data, data_len = self._ReadPlyFile(file_name)
        buf = draco.DecoderBuffer()
        # buf.Init(data, data_len)
        buf.Init(data)
        reader = draco.PlyReader()
        self.assertTrue(reader.Read(buf))
        self.assertEqual(reader.num_elements(), 2)
        self.assertEqual(reader.element(0).num_properties(), 7)
        self.assertEqual(reader.element(1).num_properties(), 1)
        self.assertTrue(reader.element(1).property(0).is_list())

        self.assertTrue(reader.element(0).GetPropertyByName("red") != None)
        # const PlyProperty *const prop = reader.element(0).GetPropertyByName("red")
        prop = reader.element(0).GetPropertyByName("red")
        # PlyPropertyReader<uint8_t> reader_uint8(prop);
        # PlyPropertyReader<uint32_t> reader_uint32(prop);
        # PlyPropertyReader<float> reader_float(prop);
        reader_uint8 = PlyPropertyReader[uint8_t](prop)
        reader_uint32 = PlyPropertyReader[uint32_t](prop)
        reader_float = PlyPropertyReader[float](prop)

        # for (int i = 0; i < reader.element(0).num_entries(); ++i)
        # for i in range(0, reader.element(0).num_entries()):
        test = reader.element(0)
        test2 = test.num_entries()
        for i in range(0, test2):
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_uint32.ReadValue(i))
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_float.ReadValue(i))

    def testReaderAscii(self):
        file_name = "test_pos_color.ply"
        data, data_len = self._ReadPlyFile(file_name)
        buf = draco.DecoderBuffer()
        # buf.Init(data, data_len)
        buf.Init(data)
        reader = draco.PlyReader()
        self.assertTrue(reader.Read(buf))

        file_name_ascii = "test_pos_color_ascii.ply"
        # const std::vector<char> data_ascii = ReadPlyFile(file_name_ascii)
        data_ascii, data_ascii_len = self._ReadPlyFile(file_name_ascii)
        # buf.Init(data_ascii.data(), data_ascii.size())
        buf.Init(data_ascii)
        reader_ascii = draco.PlyReader()
        self.assertTrue(reader_ascii.Read(buf))
        self.assertEqual(reader.num_elements(), reader_ascii.num_elements())
        self.assertEqual(reader.element(0).num_properties(),
                         reader_ascii.element(0).num_properties())

        self.assertTrue(reader.element(0).GetPropertyByName("x") != None)
        # const PlyProperty *const prop = reader.element(0).GetPropertyByName("x")
        prop = reader.element(0).GetPropertyByName("x")
        # const PlyProperty *const prop_ascii = reader_ascii.element(0).GetPropertyByName("x")
        prop_ascii = reader_ascii.element(0).GetPropertyByName("x")
        # PlyPropertyReader<float> reader_float(prop);
        reader_float = PlyPropertyReader[float](prop)
        # PlyPropertyReader<float> reader_float_ascii(prop_ascii);
        reader_float_ascii = PlyPropertyReader[float](prop_ascii)

        # for (int i = 0; i < reader.element(0).num_entries(); ++i):
        for i in range(0, reader.element(0).num_entries()):
            # self.assertNotEqualAR(reader_float.ReadValue(i), reader_float_ascii.ReadValue(i), 1e-4f)
            # NG
            # self.assertNotEqual(reader_float.ReadValue(i), reader_float_ascii.ReadValue(i), 1e-4f)
            pass

    def testReaderExtraWhitespace(self):
        file_name = 'test_extra_whitespace.ply'
        data, data_len = self._ReadPlyFile(file_name)
        buf = draco.DecoderBuffer()
        # buf.Init(data, data_len)
        buf.Init(data)
        reader = draco.PlyReader()
        self.assertTrue(reader.Read(buf))

        self.assertEqual(reader.num_elements(), 2)
        self.assertEqual(reader.element(0).num_properties(), 7)
        self.assertEqual(reader.element(1).num_properties(), 1)
        self.assertTrue(reader.element(1).property(0).is_list())

        self.assertTrue(reader.element(0).GetPropertyByName("red") != None)
        # const PlyProperty *const prop = reader.element(0).GetPropertyByName("red");
        prop = reader.element(0).GetPropertyByName("red")
        # PlyPropertyReader<uint8_t> reader_uint8(prop);
        # PlyPropertyReader<uint32_t> reader_uint32(prop);
        # PlyPropertyReader<float> reader_float(prop);

        reader_uint8 = PlyPropertyReader[uint8_t](prop)
        reader_uint32 = PlyPropertyReader[uint32_t](prop)
        reader_float = PlyPropertyReader[float](prop)

        # for (int i = 0; i < reader.element(0).num_entries(); ++i):
        for i in range(0, reader.element(0).num_entries()):
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_uint32.ReadValue(i))
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_float.ReadValue(i))

    def testReaderMoreDataTypes(self):
        file_name = "test_more_datatypes.ply"

        data, data_len = self._ReadPlyFile(file_name)
        buf = draco.DecoderBuffer()
        # buf.Init(data, data_len)
        buf.Init(data)
        reader = draco.PlyReader()
        self.assertTrue(reader.Read(buf))

        self.assertEqual(reader.num_elements(), 2)
        self.assertEqual(reader.element(0).num_properties(), 7)
        self.assertEqual(reader.element(1).num_properties(), 1)
        self.assertTrue(reader.element(1).property(0).is_list())

        self.assertTrue(reader.element(0).GetPropertyByName("red") != None)
        # const PlyProperty *const prop = reader.element(0).GetPropertyByName("red");
        prop = reader.element(0).GetPropertyByName("red")

        reader_uint8 = PlyPropertyReader[uint8_t](prop)
        reader_uint32 = PlyPropertyReader[uint32_t](prop)
        reader_float = PlyPropertyReader[float](prop)

        # for (int i = 0; i < reader.element(0).num_entries(); ++i) {
        for i in range(0, reader.element(0).num_entries()):
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_uint32.ReadValue(i))
            self.assertEqual(reader_uint8.ReadValue(i),
                             reader_float.ReadValue(i))


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyPlyReaderTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
