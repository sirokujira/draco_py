# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils


class PyPointCloudSequentialEncodingTest(unittest.TestCase):

    def setUp(self):
        pass

    # std::unique_ptr<PointCloud> DecodeObj(const std::string &file_name) const {
    def _DecodeObj(self, file_name):
        path = test_utils.GetTestFileFullPath(file_name)
        decoder = draco.ObjDecoder()
        pc = draco.PointCloud()
        if decoder.DecodeFromFile_PointCloud(path, pc) == False:
            return None

        return pc

    # std::unique_ptr<PointCloud> EncodeAndDecodePointCloud(const PointCloud *pc)
    def _EncodeAndDecodePointCloud(self, pc):
        buffer = draco.EncoderBuffer()
        encoder = draco.PointCloudSequentialEncoder()
        # EncoderOptions options = EncoderOptions::CreateDefaultOptions();
        # options = draco.EncoderOptions()
        options = draco.dracoEncoderOptions()
        encoder.SetPointCloud(pc)
        if encoder.Encode(options, buffer).ok() == False:
            return None

        dec_buffer = draco.DecoderBuffer()
        # dec_buffer.Init(buffer.data(), buffer.size())
        dec_buffer.Init(buffer.GetData())
        decoder = draco.PointCloudSequentialDecoder()

        out_pc = draco.PointCloud()
        # DecoderOptions dec_options;
        # dec_options = draco.DecoderOptions()
        dec_options = draco.dracoDecoderOptions()
        # if (!decoder.Decode(dec_options, &dec_buffer, out_pc.get()).ok())
        if decoder.Decode(dec_options, dec_buffer, out_pc).ok() == False:
            return None

        return out_pc

    def _TestEncoding(self, file_name):
        pc = self._DecodeObj(file_name)
        self.assertNotEqual(pc, None)

        decoded_pc = self._EncodeAndDecodePointCloud(pc)
        self.assertNotEqual(decoded_pc, None)
        self.assertEqual(decoded_pc.num_points(), pc.num_points())

    def testDoesEncodeAndDecode(self):
        self._TestEncoding('test_nm.obj')

    def testEncodingPointCloudWithMetadata(self):
        pc = self._DecodeObj('test_nm.obj')
        self.assertNotEqual(pc, None)
        # Add metadata to point cloud.
        metadata = draco.GeometryMetadata()
        # const uint32_t pos_att_id = pc->GetNamedAttributeId(GeometryAttribute::POSITION);
        pos_att_id = pc.GetNamedAttributeId(
            draco.GeometryAttributeType_POSITION)
        # std::unique_ptr<AttributeMetadata> pos_metadata = std::unique_ptr<AttributeMetadata>(new AttributeMetadata(pos_att_id));
        pos_metadata = draco.AttributeMetadata(pos_att_id)
        pos_metadata.AddEntryString('name', 'position')
        # metadata->AddAttributeMetadata(std::move(pos_metadata));
        metadata.AddAttributeMetadata(pos_metadata)

        # pc->AddMetadata(std::move(metadata));
        pc.AddMetadata(metadata)

        # std::unique_ptr<PointCloud> decoded_pc = EncodeAndDecodePointCloud(pc.get());
        decoded_pc = self._EncodeAndDecodePointCloud(pc)
        # self.assertNotEqual(decoded_pc.get(), None);
        self.assertNotEqual(decoded_pc, None)

        # const GeometryMetadata *const pc_metadata = decoded_pc->GetMetadata();
        pc_metadata = decoded_pc.GetMetadata()
        self.assertNotEqual(pc_metadata, None)
        # Test getting attribute metadata by id.
        # self.assertNotEqual(pc_metadata->GetAttributeMetadata(pos_att_id), None);
        self.assertNotEqual(pc_metadata.GetAttributeMetadata(pos_att_id), None)

        # Test getting attribute metadata by entry name value pair.
        # const AttributeMetadata *const requested_att_metadata = pc_metadata->GetAttributeMetadataByStringEntry("name", "position");
        requested_att_metadata = pc_metadata.GetAttributeMetadataByStringEntry(
            'name', 'position')

        self.assertNotEqual(requested_att_metadata, None)
        # NG
        # self.assertEqual(requested_att_metadata.attribute_id(), pos_att_id)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyPointCloudSequentialEncodingTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
