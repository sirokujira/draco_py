# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils

from nose.plugins.attrib import attr


@attr('next_releases')
class PyPointCloudIoTest(unittest.TestCase):

    def setUp(self):
        pass

    def _test_compression_method(self, method, expected_num_attributes, file_name):
        encoded_pc = draco.ReadPointCloudFromFile(
            test_utils.GetTestFileFullPath(file_name))
        # << "Failed to load test model " << file_name
        self.assertNotEqual(encoded_pc, None)
        # self.assertGreater(encoded_pc.num_attributes(), expected_num_attributes)
        self.assertEqual(encoded_pc.num_attributes(), expected_num_attributes)
        # << "Failed to load test model: " << file_name
        # << " wrong number of attributes" << std::endl;

        # Set quantization.
        # options = EncoderOptions::CreateDefaultOptions();
        options = draco.dracoEncoderOptions()
        for i in range(0, draco.GeometryAttributeType_NAMED_ATTRIBUTES_COUNT):
            options.SetAttributeInt(i, 'quantization_bits', 14)

        # TODO:
        # std::stringstream ss;
        # WritePointCloudIntoStream(encoded_pc.get(), ss, method, options);
        # self.assertTrue(ss.good())

        # TODO:
        # std::unique_ptr<PointCloud> decoded_pc;
        decoded_pc = draco.PointCloud()
        # ReadPointCloudFromStream(&decoded_pc, ss);
        # decoded_pc = draco.ReadPointCloudFromStream(ss)
        # self.assertTrue(ss.good())

        # for (int i = 0; i <= GeometryAttribute::NAMED_ATTRIBUTES_COUNT; i++):
        for i in range(0, draco.GeometryAttributeType_NAMED_ATTRIBUTES_COUNT):
            self.assertEqual(encoded_pc.NumNamedAttributes(i),
                             decoded_pc.NumNamedAttributes(i))

        self.assertEqual(encoded_pc.num_points(), decoded_pc.num_points())

    def testEncodeSequentialPointCloudTestNmObj(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING, 2, 'test_nm.obj')

    def testEncodeSequentialPointCloudTestPosObj(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING, 1, 'point_cloud_test_pos.obj')

    def testEncodeSequentialPointCloudTestPosPly(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING, 1, 'point_cloud_test_pos.ply')

    def EncodeSequentialPointCloudTestPosNormObj(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING, 2, 'point_cloud_test_pos_norm.obj')

    def EncodeSequentialPointCloudTestPosNormPly(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING, 2, 'point_cloud_test_pos_norm.ply')

    def _EncodeKdTreePointCloudTestPosObj(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_KD_TREE_ENCODING, 1, 'point_cloud_test_pos.obj')

    def testEncodeKdTreePointCloudTestPosPly(self):
        self._test_compression_method(
            draco.PointCloudEncodingMethod_POINT_CLOUD_KD_TREE_ENCODING, 1, 'point_cloud_test_pos.ply')

    def testObjFileInput(self):
        # Tests whether loading obj point clouds from files works as expected.
        pc = draco.ReadPointCloudFromFile(
            test_utils.GetTestFileFullPath('test_nm.obj'))
        # << "Failed to load the obj point cloud."
        self.assertNotEqual(pc, None)
        # EXPECT_EQ(pc->num_points(), 97) # << "Obj point cloud not loaded properly."
        # << "Obj point cloud not loaded properly."
        self.assertNotEqual(pc.num_points(), 97)

    # Test if we handle wrong input for all file extensions.
    def testWrongFileObj(self):
        pc = draco.ReadPointCloudFromFile(
            test_utils.GetTestFileFullPath('wrong_file_name.obj'))
        self.assertEqual(pc, None)

    def testWrongFilePly(self):
        pc = draco.ReadPointCloudFromFile(
            test_utils.GetTestFileFullPath('wrong_file_name.ply'))
        self.assertEqual(pc, None)

    def testWrongFile(self):
        pc = draco.ReadPointCloudFromFile(
            test_utils.GetTestFileFullPath('wrong_file_name'))
        self.assertEqual(pc, None)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyPointCloudIoTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
