import sys
import os.path
import unittest

import draco_py as draco
import test_utils


class PyPlyDecoderTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.PlyDecoder()

    def _DecodePly(self, file_name):
        infile = open(file_name, 'rb')
        data = infile.read()
        data_len = len(data)

        buffer = draco.DecoderBuffer()
        buffer.Init(data)
        geom_type = draco.GetEncodedGeometryType(buffer)

        mesh = None
        if geom_type == draco.EncodedGeometryType_TRIANGULAR_MESH:
            mesh = draco.DecodeMeshFromBuffer(buffer)
        elif geom_type == draco.EncodedGeometryType_POINT_CLOUD:
            pc = draco.DecodePointCloudFromBuffer(buffer)
        else:
            mesh = None

        return mesh

    # return mesh
    def _test_decoding_method(self, file_name, num_faces, num_points):
        # std::unique_ptr<Mesh> mesh(DecodePly<Mesh>(file_name));
        mesh = draco.Mesh()
        self.dec.DecodeFromFile_Mesh(file_name, mesh)
        # << "Failed to load test model " << file_name;
        self.assertNotEqual(mesh, None)
        self.assertEqual(mesh.num_faces(), num_faces)

        # const std::unique_ptr<PointCloud> pc(DecodePly<PointCloud>(file_name));
        # pc = mesh.GetPointCloud()
        self.assertEqual(mesh.num_points(), num_points)
        # self.assertNotEqual(pc, None) # << "Failed to load test model " << file_name;
        # self.assertEqual(pc.num_points(), num_points)
        return mesh

    def testPlyDecoding(self):
        file_name = 'test_pos_color.ply'
        filepath = test_utils.GetTestFileFullPath(file_name)
        self._test_decoding_method(filepath, 224, 114)

    def testPlyNormals(self):
        file_name = 'cube_att.ply'
        filepath = test_utils.GetTestFileFullPath(file_name)
        mesh = self._test_decoding_method(filepath, 12, 3 * 8)
        self.assertNotEqual(mesh, None)
        # const int att_id = mesh.GetNamedAttributeId(draco.GeometryAttributeType_NORMAL)
        att_id = mesh.GetNamedAttributeId(draco.GeometryAttributeType_NORMAL)
        self.assertGreater(att_id, 0)
        # const PointAttribute *const att = mesh->attribute(att_id);
        att = mesh.attribute(att_id)
        self.assertEqual(att.GetSize(), 6)  # 6 unique normal values.


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyPlyDecoderTest))
    return suite


if __name__ == '__main__':
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
