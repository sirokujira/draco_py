# Copyright 2017 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils

from nose.plugins.attrib import attr


@attr('next_releases')
class PyPointCloudTest(unittest.TestCase):

    def setUp(self):
        pass

    def testAttributeDeletion(self):
        pc = draco.PointCloud()
        # Test whether we can correctly delete an attribute from a point cloud.
        # Create some attributes for the point cloud.
        pos_att = draco.GeometryAttribute()
        pos_att.Init(draco.GeometryAttributeType_POSITION, None,
                     3, draco.DataType_DT_FLOAT32, False, 12, 0)
        norm_att = draco.GeometryAttribute()
        norm_att.Init(draco.GeometryAttributeType_NORMAL, None,
                      3, draco.DataType_DT_FLOAT32, False, 12, 0)
        gen_att = draco.GeometryAttribute()
        gen_att.Init(draco.GeometryAttributeType_GENERIC, None,
                     3, draco.DataType_DT_FLOAT32, False, 12, 0)

        # Add one position, two normal and two generic attributes.
        pc.AddAttribute(pos_att, False, 0)
        pc.AddAttribute(gen_att, False, 0)
        pc.AddAttribute(norm_att, False, 0)
        pc.AddAttribute(gen_att, False, 0)
        pc.AddAttribute(norm_att, False, 0)

        self.assertEqual(pc.num_attributes(), 5)
        self.assertEqual(pc.attribute(0).attribute_type(),
                         draco.GeometryAttributeType_POSITION)
        self.assertEqual(pc.attribute(3).attribute_type(),
                         draco.GeometryAttributeType_GENERIC)

        # Delete generic attribute.
        pc.DeleteAttribute(1)
        self.assertEqual(pc.num_attributes(), 4)
        self.assertEqual(pc.attribute(1).attribute_type(),
                         draco.GeometryAttributeType_NORMAL)
        self.assertEqual(pc.NumNamedAttributes(
            draco.GeometryAttributeType_NORMAL), 2)
        self.assertEqual(pc.GetNamedAttributeId(
            draco.GeometryAttributeType_NORMAL, 1), 3)

        # Delete the first normal attribute.
        pc.DeleteAttribute(1)
        self.assertEqual(pc.num_attributes(), 3)
        self.assertEqual(pc.attribute(1).attribute_type(),
                         draco.GeometryAttributeType_GENERIC)
        self.assertEqual(pc.NumNamedAttributes(
            draco.GeometryAttributeType_NORMAL), 1)
        self.assertEqual(pc.GetNamedAttributeId(
            draco.GeometryAttributeType_NORMAL, 0), 2)

    def testPointCloudWithMetadata(self):
        pc = draco.PointCloud()
        # std::unique_ptr<draco::GeometryMetadata> metadata = std::unique_ptr<draco::GeometryMetadata>(new draco::GeometryMetadata());
        metadata = draco.GeometryMetadata()

        pos_att = draco.GeometryAttribute()
        pos_att.Init(draco.GeometryAttributeType_POSITION, None,
                     3, draco.DataType_DT_FLOAT32, False, 12, 0)
        # const uint32_t pos_att_id = pc.AddAttribute(pos_att, False, 0);
        pos_att_id = pc.AddAttribute(pos_att, False, 0)

        # Test adding attribute metadata.
        # std::unique_ptr<draco::AttributeMetadata> pos_metadata = std::unique_ptr<draco::AttributeMetadata>(new draco::AttributeMetadata(pos_att_id))
        pos_metadata = draco.AttributeMetadata(pos_att_id)

        pos_metadata.AddEntryString("name", "position")
        # metadata.AddAttributeMetadata(std::move(pos_metadata))
        # pc.AddMetadata(std::move(metadata))
        metadata.AddAttributeMetadata(pos_metadata)
        pc.AddMetadata(metadata)

        # const draco::GeometryMetadata *pc_metadata = pc.GetMetadata();
        pc_metadata = draco.GeometryMetadata()
        self.assertNotEqual(pc_metadata, None)
        self.assertNotEqual(pc_metadata.GetAttributeMetadata(pos_att_id), None)

        # Test direct adding attribute metadata to point cloud.
        material_att = draco.GeometryAttribute()
        material_att.Init(draco.GeometryAttributeType_GENERIC,
                          None, 3, draco.DataType_DT_FLOAT32, False, 12, 0)

        material_att_id = pc.AddAttribute(material_att, False, 0)
        material_metadata = draco.AttributeMetadata(material_att_id)
        material_metadata.AddEntryString("name", "material")
        # pc.AddAttributeMetadata(std::move(material_metadata));
        pc.AddAttributeMetadata(material_metadata)

        self.assertNotEqual(
            pc_metadata.GetAttributeMetadata(material_att_id), None)
        # const draco::AttributeMetadata*
        requested_att_metadata = pc_metadata.GetAttributeMetadataByStringEntry(
            "name", "material")
        self.assertNotEqual(requested_att_metadata, None)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyPointCloudTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
