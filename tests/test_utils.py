import sys
import os.path


def GetTestFileFullPath(filename):
    filepath = 'tests' + os.path.sep + 'testdata' + os.path.sep + filename
    return _encode(filepath)


def GetTestFileInvalidFullPath(filename):
    filepath = 'tests' + os.path.sep + 'testdata' + \
        os.path.sep + 'invalid' + os.path.sep + filename
    return _encode(filepath)


def _encode(path):
    # Encode path for use in C++.
    if isinstance(path, bytes):
        return path
    else:
        return path.encode(sys.getfilesystemencoding())
