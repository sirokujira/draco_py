# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils


class PyMeshAreEquivalentTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.ObjDecoder()

    def _DecodeObj(self, file_name):
        mesh = draco.Mesh()
        decoder = draco.ObjDecoder()
        if decoder.DecodeFromFile_Mesh(file_name, mesh) == False:
            return None
        return mesh

    def testOnIndenticalMesh(self):
        file_name = "test_nm.obj"
        file_path = test_utils.GetTestFileFullPath(file_name)
        mesh = self._DecodeObj(file_path)
        # << "Failed to load test model." << file_path
        self.assertNotEqual(mesh, None)
        equiv = draco.MeshAreEquivalent()
        self.assertTrue(equiv(mesh, mesh))

    def testPermutedOneFace(self):
        file_name_0 = "one_face_123.obj"
        file_name_1 = "one_face_312.obj"
        file_name_2 = "one_face_321.obj"
        file_path_0 = test_utils.GetTestFileFullPath(file_name_0)
        file_path_1 = test_utils.GetTestFileFullPath(file_name_1)
        file_path_2 = test_utils.GetTestFileFullPath(file_name_2)
        mesh_0 = self._DecodeObj(file_path_0)
        mesh_1 = self._DecodeObj(file_path_1)
        mesh_2 = self._DecodeObj(file_path_2)
        # << "Failed to load test model." << file_path_0
        self.assertNotEqual(mesh_0, None)
        # << "Failed to load test model." << file_path_1
        self.assertNotEqual(mesh_1, None)
        # << "Failed to load test model." << file_path_2
        self.assertNotEqual(mesh_2, None)
        equiv = draco.MeshAreEquivalent()
        self.assertTrue(equiv(mesh_0, mesh_0))
        self.assertTrue(equiv(mesh_0, mesh_1))   # Face rotated.
        self.assertFalse(equiv(mesh_0, mesh_2))  # Face inverted.

    def testPermutedTwoFaces(self):
        file_name_0 = "two_faces_123.obj"
        file_name_1 = "two_faces_312.obj"
        file_path_0 = test_utils.GetTestFileFullPath(file_name_0)
        file_path_1 = test_utils.GetTestFileFullPath(file_name_1)
        mesh_0 = self._DecodeObj(file_path_0)
        mesh_1 = self._DecodeObj(file_path_1)
        # << "Failed to load test model." << file_path_0;
        self.assertNotEqual(mesh_0, None)
        # << "Failed to load test model." << file_path_1;
        self.assertNotEqual(mesh_1, None)
        equiv = draco.MeshAreEquivalent()
        self.assertTrue(equiv(mesh_0, mesh_0))
        self.assertTrue(equiv(mesh_1, mesh_1))
        self.assertTrue(equiv(mesh_0, mesh_1))

    def testPermutedThreeFaces(self):
        file_name_0 = "three_faces_123.obj"
        file_name_1 = "three_faces_312.obj"
        file_path_0 = test_utils.GetTestFileFullPath(file_name_0)
        file_path_1 = test_utils.GetTestFileFullPath(file_name_1)
        mesh_0 = self._DecodeObj(file_path_0)
        mesh_1 = self._DecodeObj(file_path_1)
        # << "Failed to load test model." << file_name_0;
        self.assertNotEqual(mesh_0, None)
        # << "Failed to load test model." << file_name_1;
        self.assertNotEqual(mesh_1, None)
        equiv = draco.MeshAreEquivalent()
        self.assertTrue(equiv(mesh_0, mesh_0))
        self.assertTrue(equiv(mesh_1, mesh_1))
        self.assertTrue(equiv(mesh_0, mesh_1))


# TODO: TestNG
#     # This test checks that the edgebreaker algorithm does not change the mesh up
#     # to the order of faces and vertices.
#     def testOnBigMesh(self):
#         file_name = "test_nm.obj";
#         file_path = test_utils.GetTestFileFullPath(file_name)
#         mesh0 = self._DecodeObj(file_path)
#         self.assertNotEqual(mesh0, None) # << "Failed to load test model." << file_name;
#
#         mesh1 = draco.Mesh()
#         # std::stringstream ss;
#         # WriteMeshIntoStream(mesh0.get(), ss, MESH_EDGEBREAKER_ENCODING);
#         # ReadMeshFromStream(&mesh1, ss);
#         # self.assertTrue(ss.good()) # << "Mesh IO failed.";
#
#         equiv = draco.MeshAreEquivalent()
#         self.assertTrue(equiv(mesh0, mesh0))
#         self.assertTrue(equiv(mesh1, mesh1))
#         self.assertTrue(equiv(mesh0, mesh1))


def GetTestFileFullPath(filename):
    filepath = 'tests' + os.path.sep + 'testdata' + os.path.sep + filename
    return _encode(filepath)


def _encode(path):
    # Encode path for use in C++.
    if isinstance(path, bytes):
        return path
    else:
        return path.encode(sys.getfilesystemencoding())


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyMeshAreEquivalentTest))
    return suite


if __name__ == '__main__':
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
