# Copyright 2016 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils

# base : metadata_encoder_test.cc


class PyMeshEncoderTest(unittest.TestCase):
    def setUp(self):
        self.dec = draco.ObjDecoder()
        # self.enc = draco.Encoder()

    def _DecodeObj(self, path):
        # const std::string path = test_utils.GetTestFileFullPath(file_name);
        mesh = draco.Mesh()
        decoder = draco.ObjDecoder()
        if decoder.DecodeFromFile_Mesh(path, mesh) == False:
            return None

        return mesh

    def testSequentialEncodeGoldenMesh(self):
        # sequential
        method = draco.MeshEncoderMethod_MESH_SEQUENTIAL_ENCODING
        self._EncodeGoldenMesh(method, 'sequential')

    def testEdgebreakerEncodeGoldenMesh(self):
        # edgebreaker
        method = draco.MeshEncoderMethod_MESH_EDGEBREAKER_ENCODING
        self._EncodeGoldenMesh(method, 'edgebreaker')

    def _EncodeGoldenMesh(self, method, type):
        file_name = 'test_nm.obj'
        filepath = test_utils.GetTestFileFullPath(file_name)

        golden_file_name = file_name
        golden_file_name = golden_file_name + '.'
        golden_file_name = golden_file_name + type  # sequential or edgebreaker
        golden_file_name = golden_file_name + '.1.1.0.drc'

        mesh = self._DecodeObj(filepath)
        # << "Failed to load test model " << file_name
        self.assertNotEqual(mesh, None)

        # EncoderOptions options = CreateDefaultEncoderOptions();
        # options = draco.CreateDefaultEncoderOptions()
        options = draco.dracoEncoderOptions()

        encoder = draco.ExpertEncoder(mesh)
        encoder.SetEncodingMethod(method)
        encoder.SetAttributeQuantization(0, 20)

        # EncoderBuffer buffer;
        buffer = draco.EncoderBuffer()
        status = encoder.EncodeToBuffer(buffer)
        self.assertNotEqual(buffer, None)

        # Check that the encoded mesh was really encoded with the selected method.
        # DecoderBuffer decoder_buffer;
        # decoder_buffer.Init(buffer.data(), buffer.size());
        decoder_buffer = draco.DecoderBuffer()
        decoder_buffer.Init(buffer.GetData())

        decoder_buffer.Advance(8)  # Skip the header to the encoding method id.
        encoded_method = decoder_buffer.Decode()
        self.assertEqual(encoded_method, method)

        # Unknown
        # if FLAGS_update_golden_files == False:
        #     EXPECT_TRUE(
        #         CompareGoldenFile(golden_file_name, buffer.data(), buffer.size()))
        #         # << "Encoded data is different from the golden file. Please verify that "
        #            "the"
        #            " encoding works as expected and update the golden file if necessary"
        #            " (run the test with --update_golden_files flag).";
        # else
        #     # Save the files into the local folder.
        #     EXPECT_TRUE(
        #         GenerateGoldenFile(golden_file_name, buffer.data(), buffer.size()))
        #         # << "Failed to generate new golden file for " << file_name;


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyMeshEncoderTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
