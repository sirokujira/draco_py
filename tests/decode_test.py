# Copyright 2017 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
import test_utils


class PyDecodeTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.ObjDecoder()

    def testSkipAttributeTransform(self):
        file_name = 'test_nm_quant.0.9.0.drc'
        # Tests that decoders can successfully skip attribute transform.
        # input_file(draco::GetTestFileFullPath(file_name), std::ios::binary);
        filepath = test_utils.GetTestFileFullPath(file_name)

        input_file = open(filepath, 'rb')
        self.assertTrue(input_file)

        # Read the file stream into a buffer.
        data = input_file.read()
        file_size = len(data)
        # input_file.read(data.data(), file_size);
        # self.assertFalse(data.empty())
        # print(file_size)

        # Create a draco decoding buffer. Note that no data is copied in this step.
        buffer = draco.DecoderBuffer()
        # buffer.Init(data.data(), data.size())
        buffer.Init(data)
        # buffer.Init2(data)

        decoder = draco.Decoder()
        # Make sure we skip dequantization for the position attribute.
        decoder.SetSkipAttributeTransform(draco.GeometryAttributeType_POSITION)

        # Decode the input data into a geometry.
        pc = decoder.DecodePointCloudFromBuffer(buffer)
        # pc = draco.DecodePointCloudFromBuffer(buffer)
        self.assertNotEqual(pc, None)

        # const draco::PointAttribute *const pos_att = pc.GetNamedAttribute(draco.GeometryAttributeType_POSITION)
        pos_att = pc.GetNamedAttribute(draco.GeometryAttributeType_POSITION)
        self.assertNotEqual(pos_att, None)

        # Ensure the position attribute is of type int32_t and that it has a valid
        # attribute transform.
        # self.assertEqual(pos_att.Data_type(), draco.DataType_DT_INT32)
        # self.assertNotEqual(pos_att.GetAttributeTransformData(), None)

        # Normal attribute should be left transformed.
        # const draco::PointAttribute *const
        norm_att = pc.GetNamedAttribute(draco.GeometryAttributeType_NORMAL)
        # self.assertEqual(norm_att.Data_type(), draco.DataType_DT_FLOAT32)
        # self.assertEqual(norm_att.GetAttributeTransformData(), None)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyDecodeTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
