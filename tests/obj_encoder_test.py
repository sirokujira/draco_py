import sys
import os.path
import unittest

import draco_py as draco
import test_utils

from nose.plugins.attrib import attr


@attr('next_releases')
class PyObjEncoderTest(unittest.TestCase):

    def setUp(self):
        self.dec = draco.ObjDecoder()

    def _DecodeFromObjFile(self, file_name):
        mesh = draco.Mesh()
        self.dec.DecodeFromFile_Mesh(file_name, mesh)
        self.dec.set_use_metadata(True)
        if self.dec.DecodeFromFile_Mesh(file_name, mesh) == False:
            return None

        return mesh

    def _CompareMeshes(self, mesh0, mesh1):
        self.assertEqual(mesh0.num_faces(), mesh1.num_faces())
        self.assertEqual(mesh0.num_attributes(), mesh1.num_attributes())

        for att_id in range(0, mesh0.num_attributes()):
            self.assertEqual(mesh0.Attribute(att_id).size(),
                             mesh1.Attribute(att_id).size())

    def _EncodeAndDecodeMesh(self, mesh):
        encoder_buffer = draco.EncoderBuffer()
        encoder = draco.ObjEncoder()

        if encoder.EncodeToButter_Mesh(mesh, encoder_buffer) == False:
            return None

        decoder_buffer = draco.DecoderBuffer()
        # decoder_buffer.Init(encoder_buffer.data(), encoder_buffer.size())
        decoder_buffer.Init(encoder_buffer.GetData())
        decoded_mesh = draco.Mesh()

        decoder = draco.ObjDecoder()
        decoder.set_use_metadata(True)

        # if decoder.DecodeFromBuffer(&decoder_buffer, decoded_mesh()) == False:
        if decoder.DecodeFromBuffer_Mesh(decoder_buffer, decoded_mesh) == False:
            return None

        return decoded_mesh

    def _test_encoding(self, file_name):
        # const std::unique_ptr<Mesh> mesh(DecodeFromObjFile<Mesh>(file_name));
        file_path = test_utils.GetTestFileFullPath(file_name)

        mesh = self._DecodeFromObjFile(file_path)

        # << "Failed to load test model " << file_path
        self.assertNotEqual(mesh, None)
        self.assertGreater(mesh.num_faces(), 0)
        self.assertGreater(mesh.num_points(), 0)

        decoded_mesh = self._EncodeAndDecodeMesh(mesh)
        # print(mesh)
        # print(decoded_mesh)
        self._CompareMeshes(mesh, decoded_mesh)
        pass

    def testHasSubObject(self):
        self._test_encoding('cube_att_sub_o.obj')

    def testHasMaterial(self):
        file_name = 'mat_test.obj'
        file_path = test_utils.GetTestFileFullPath(file_name)

        mesh0 = self._DecodeFromObjFile(file_path)
        self.assertNotEqual(mesh0, None)
        mesh1 = self._EncodeAndDecodeMesh(mesh0)
        self.assertNotEqual(mesh1, None)
        self.assertEqual(mesh0.num_faces(), mesh1.num_faces())
        self.assertEqual(mesh0.num_attributes(), mesh1.num_attributes())

        # Position attribute should be the same.
        self.assertEqual(mesh0.attribute(0).size(), mesh1.attribute(0).size())

        # Since |mesh1| is decoded from buffer, it has not material file.
        # So the size of material attribute is the number of materials used in the obj
        # file which is 6. The size of material attribute of |mesh0| decoded from
        # the obj file will be the number of materials defined in the .mtl file.
        self.assertEqual(mesh0.attribute(1).size(), 29)
        self.assertEqual(mesh1.attribute(1).size(), 6)

    def testObjEncodingAll(self):
        self._test_encoding('cube_att_sub_o.obj')
        self._test_encoding('bunny_norm.obj')
        self._test_encoding('cube_att.obj')
        self._test_encoding('cube_att_partial.obj')
        self._test_encoding('cube_quads.obj')
        self._test_encoding('cube_subd.obj')
        self._test_encoding('extra_vertex.obj')
        self._test_encoding('one_face_123.obj')
        self._test_encoding('one_face_312.obj')
        self._test_encoding('one_face_321.obj')
        self._test_encoding('sphere.obj')
        self._test_encoding('test_nm.obj')
        self._test_encoding('test_nm_trans.obj')
        self._test_encoding('test_sphere.obj')
        self._test_encoding('three_faces_123.obj')
        self._test_encoding('three_faces_312.obj')
        self._test_encoding('two_faces_123.obj')
        self._test_encoding('two_faces_312.obj')
        pass
        # c Source
        # self._test_encoding('bunny_norm.obj')
        # self._test_encoding('cube_att.obj')
        # self._test_encoding('cube_att_partial.obj')
        # self._test_encoding('cube_quads.obj')
        # self._test_encoding('cube_subd.obj')
        # self._test_encoding('extra_vertex.obj')
        # self._test_encoding('one_face_123.obj')
        # self._test_encoding('one_face_312.obj')
        # self._test_encoding('one_face_321.obj')
        # self._test_encoding('sphere.obj')
        # self._test_encoding('test_nm.obj')
        # self._test_encoding('test_nm_trans.obj')
        # self._test_encoding('test_sphere.obj')
        # self._test_encoding('three_faces_123.obj')
        # self._test_encoding('three_faces_312.obj')
        # self._test_encoding('two_faces_123.obj')
        # self._test_encoding('two_faces_312.obj')


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyObjEncoderTest))
    return suite


if __name__ == '__main__':
    # unittest.main()
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
