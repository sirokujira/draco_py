# Copyright 2017 The Draco Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import os.path
import unittest

import draco_py as draco
# import test_utils


class PyDecoderOptionsTest(unittest.TestCase):

    def setUp(self):
        pass

    def testOptions(self):
        # This test verifies that we can update global and attribute options of the
        # DecoderOptions class instance.
        # options = draco.DecoderOptions()
        options = draco.dracoDecoderOptions()
        options.SetGlobalInt('test', 3)
        self.assertEqual(options.GetGlobalInt('test', -1), 3)

        options.SetAttributeInt(
            draco.GeometryAttributeType_POSITION, 'test', 1)
        options.SetAttributeInt(draco.GeometryAttributeType_GENERIC, 'test', 2)

        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_TEX_COORD, 'test', -1), 3)
        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_POSITION, 'test', -1), 1)
        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_GENERIC, 'test', -1), 2)

    def testAttributeOptionsAccessors(self):
        # This test verifies that we can query options stored in DecoderOptions class instance.
        # options = draco.DecoderOptions()
        options = draco.dracoDecoderOptions()
        options.SetGlobalInt('test', 1)
        options.SetAttributeInt(
            draco.GeometryAttributeType_POSITION, 'test', 2)
        options.SetAttributeInt(
            draco.GeometryAttributeType_TEX_COORD, 'test', 3)

        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_POSITION, 'test', -1), 2)
        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_POSITION, 'test2', -1), -1)
        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_TEX_COORD, 'test', -1), 3)
        self.assertEqual(
            options.GetAttributeInt(draco.GeometryAttributeType_NORMAL, 'test', -1), 1)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(PyDecoderOptionsTest))
    return suite


if __name__ == '__main__':
    testSuite = suite()
    unittest.TextTestRunner().run(testSuite)
