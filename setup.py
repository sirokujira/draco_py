# -*- coding: utf-8 -*-
from __future__ import print_function
from collections import defaultdict
from Cython.Distutils import build_ext
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from setuptools import setup, find_packages, Extension

import subprocess
import numpy
import sys
import platform
import os
import time

setup_requires = []
install_requires = [
    'filelock',
    'nose',
    'numpy',
    'Cython>=0.25.2',
]

if platform.system() == "Windows":
    # Check 32bit or 64bit
    is_64bits = sys.maxsize > 2**32
    # if is_64bits == True

    draco_root = os.path.dirname(os.path.abspath(__file__)) + '/draco'

    # Python Version Check
    info = sys.version_info

    # Visual Studio 2015
    if info.major == 3 and info.minor >= 5:
        pass
    else:
        if info.major == 2 and info.minor == 7:
            import _msvccompiler
            import distutils.msvc9compiler
            def find_vcvarsall(version):
                # 2017
                # return "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build/vcvarsall.bat"
                vcvarsall, vcruntime = _msvccompiler._find_vcvarsall('x64')
                if vcvarsall is not None:
                    print('set msvc2017/2015 compiler')
                    print(vcvarsall)
                    return vcvarsall
                else:
                    print('no set msvc2017/2015 compiler')
                    return None

            distutils.msvc9compiler.find_vcvarsall=find_vcvarsall
        else:
            print('no building Python Version')
            sys.exit(1)

    ext_args = defaultdict(list)

    # set include path
    ext_args['include_dirs'].append(numpy.get_include())

    inc_dirs = [draco_root + '/src', draco_root + '/src/draco']

    for inc_dir in inc_dirs:
        ext_args['include_dirs'].append(inc_dir)

    # set library path
    # lib_dirs = [draco_root + '/build/Debug']
    lib_dirs = [draco_root + '/build/Release']

    for lib_dir in lib_dirs:
        ext_args['library_dirs'].append(lib_dir)

    libreleases = ['draco', 'dracodec', 'dracoenc']

    for librelease in libreleases:
        ext_args['libraries'].append(librelease)

    ext_args['extra_compile_args'].append('/EHsc')
    print(ext_args)
else:
    # Not 'Windows'
    draco_root = os.path.dirname(os.path.abspath(__file__)) + "/draco"

    ext_args = defaultdict(list)
    ext_args['include_dirs'].append(numpy.get_include())

    inc_dirs = [draco_root + '/src', draco_root + '/src/draco']

    for inc_dir in inc_dirs:
        ext_args['include_dirs'].append(inc_dir)

    # set library path
    # *.a files(draco/dracodec/draencode) directory path
    lib_dirs = [draco_root + '/build']

    for lib_dir in lib_dirs:
        ext_args['library_dirs'].append(lib_dir)

    libreleases = ['draco', 'dracodec', 'dracoenc']

    for librelease in libreleases:
        ext_args['libraries'].append(librelease)

    # before draco module build setting
    # ext_args['extra_compile_args'].append('-fPIC')
    ext_args['extra_compile_args'].append('-std=c++11')


# common
setup(name='draco-python',
      description='A python wrapper for the draco library',
      url='http://github.com/sirokujira/draco_py',
      version='0.0.4',
      author='Tooru Oonuma',
      author_email='t753github@gmail.com',
      license='BSD',
      packages=find_packages(),
      zip_safe=False,
      setup_requires=setup_requires,
      install_requires=install_requires,
      tests_require=['mock', 'nose'],
      ext_modules=[
          Extension(
              'draco_py._draco',
              ["draco_py/_draco.pyx", 'draco_py/mini_draco.cpp', 'draco_py/attr_draco.cpp'],
              language="c++", **ext_args
          ),
          # Win32 Error
          # Extension(
          #     'draco_py.extension.numpy_decode',
          #     ["draco_py/extension/numpy_decode.pyx"],
          #     language="c++", **ext_args
          # ),
          # Extension(
          #     'draco_py.extension.numpy_encode',
          #     ["draco_py/extension/numpy_encode.pyx"],
          #     language="c++", **ext_args
          # ),
      ],
      cmdclass={'build_ext': build_ext},
      classifiers=[
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
      ],
      )
