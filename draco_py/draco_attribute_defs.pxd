# -*- coding: utf-8 -*-

from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.memory cimport unique_ptr
from libcpp.string cimport string

# 
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint32_t

###############################################################################
# Types
###############################################################################


## attributes
# attribute_octahedron_transform.h
# attribute_quantization_transform.h
###


# geometry_indices.h

# base
# template <class ValueTypeT, class TagT>
# class IndexType {
cdef extern from "draco/attributes/geometry_indices.h" namespace "draco" nogil:
    cdef cppclass IndexType[ValueTypeT, TagT]:
        IndexType()
        # IndexType(ValueTypeT value)
        ValueTypeT value()
        IndexType operator++()
        IndexType operator--()
        IndexType operator+(IndexType)
        IndexType operator+(ValueTypeT)
        IndexType operator-(IndexType)
        IndexType operator-(ValueTypeT)


# Index of an attribute value entry stored in a GeometryAttribute.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, AttributeValueIndex)
# struct AttributeValueIndex_tag_type_ {}
# typedef IndexType[int32_t, PointIndex_tag_type_] AttributeValueIndex;
cdef extern from "draco/attributes/geometry_indices.h" namespace "draco" nogil:
    cdef struct AttributeValueIndex_tag_type_:
        AttributeValueIndex_tag_type_()


ctypedef IndexType[int32_t, AttributeValueIndex_tag_type_] AttributeValueIndex

# Index of a point in a PointCloud.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, PointIndex)
# struct PointIndex_tag_type_ {}
# typedef IndexType[int32_t, PointIndex_tag_type_] PointIndex;
cdef extern from "draco/attributes/geometry_indices.h" namespace "draco" nogil:
    cdef struct PointIndex_tag_type_:
        PointIndex_tag_type_()


ctypedef IndexType[int32_t, PointIndex_tag_type_] PointIndex

# 
# Constants denoting invalid indices.
# static constexpr AttributeValueIndex kInvalidAttributeValueIndex(std::numeric_limits<int32_t>::min() / 2)
# static constexpr PointIndex kInvalidPointIndex(std::numeric_limits<int32_t>::min() / 2)
###

# ctypedef int THREE "3"
# convert cpp : typedef int 3;
# geometry_attribute.h
# The class provides access to a specific attribute which is stored in a
# DataBuffer, such as normals or coordinates. However, the GeometryAttribute
# class does not own the buffer and the buffer itself may store other data
# unrelated to this attribute (such as data for other attributes in which case
# we can have multiple GeometryAttributes accessing one buffer). Typically,
# all attributes for a point (or corner, face) are stored in one block, which
# is advantageous in terms of memory access. The length of the entire block is
# given by the byte_stride, the position where the attribute starts is given by
# the byte_offset, the actual number of bytes that the attribute occupies is
# given by the data_type and the number of components.
cdef extern from "draco/attributes/geometry_attribute.h" namespace "draco" nogil:
    cdef cppclass GeometryAttribute:
        GeometryAttribute()

        # Initializes and enables the attribute.
        # void Init(
        #       Type attribute_type, DataBuffer *buffer, int8_t num_components,
        #         core.DataType data_type, bool normalized, int64_t byte_stride,
        #         int64_t byte_offset)
        void Init(Type2 attribute_type, core.DataBuffer *buffer, int8_t num_components,
                  core.DataType data_type, bool normalized, int64_t byte_stride, 
                  int64_t byte_offset)

        # bool IsValid() const
        bool IsValid()

        # Copies data from the source attribute to the this attribute.
        # This attribute must have a valid buffer allocated otherwise the operation
        # is going to fail and return false.
        # bool CopyFrom(const GeometryAttribute &src_att);
        bool CopyFrom(const GeometryAttribute src_att)

        # Function for getting a attribute value with a specific format.
        # Unsafe. Caller must ensure the accessed memory is valid.
        # T is the attribute data type.
        # att_components_t is the number of attribute components.
        # template <typename T, int att_components_t>
        # std::array<T, att_components_t> GetValue(AttributeValueIndex att_index) const

        # Function for getting a attribute value with a specific format.
        # T is the attribute data type.
        # att_components_t is the number of attribute components.
        # template <typename T, int att_components_t>
        # bool GetValue(AttributeValueIndex att_index,std::array<T, att_components_t> *out) const

        # Returns the byte position of the attribute entry in the data buffer.
        # inline int64_t GetBytePos(AttributeValueIndex att_index) const

        # inline const uint8_t *GetAddress(AttributeValueIndex att_index) const
        uint8_t* GetAddress(AttributeValueIndex att_index)

        # Fills out_data with the raw value of the requested attribute entry.
        # out_data must be at least byte_stride_ long.
        # void GetValue(AttributeValueIndex att_index, void *out_data) const

        # Function for conversion of a attribute to a specific output format.
        # OutT is the desired data type of the attribute.
        # out_att_components_t is the number of components of the output format.
        # Returns false when the conversion failed.
        # template <typename OutT, int out_att_components_t>
        # bool ConvertValue(AttributeValueIndex att_id, OutT *out_val)
        # http://grokbase.com/t/gg/cython-users/128vxp33jv/wrapping-c-template-struct
        # bool ConvertValue[OutT, int](AttributeValueIndex att_id, OutT *out_val)
        # ctypedef int THREE "3"
        # bool ConvertValue[OutT, ParamT](AttributeValueIndex att_id, OutT *out_val)
        # bool ConvertValue[OutT, 3](AttributeValueIndex att_id, OutT *out_val)
        # bool ConvertValue[OutT, THREE](AttributeValueIndex att_id, OutT *out_val)

        # Function for conversion of a attribute to a specific output format.
        # The |out_value| must be able to store all components of a single attribute
        # entry.
        # OutT is the desired data type of the attribute.
        # Returns false when the conversion failed.
        # template <typename OutT>
        # bool ConvertValue(AttributeValueIndex att_index, OutT *out_value) const
        bool ConvertValue[OutT](AttributeValueIndex att_index, OutT *out_value)

        # bool operator==(const GeometryAttribute &va) const;

        # # Returns the type of the attribute indicating the nature of the attribute.
        Type2 attribute_type()
        void set_attribute_type(Type2 type)

        # # Returns the data type that is stored in the attribute.
        # DataType data_type() const
        core.DataType data_type()

        # Returns the number of components that are stored for each entry.
        # For position attribute this is usually three (x,y,z),
        # while texture coordinates have two components (u,v).
        # int8_t num_components() const
        int8_t num_components()

        # Indicates whether the data type should be normalized before interpretation,
        # that is, it should be divided by the max value of the data type.
        # bool normalized() const
        bool normalized()
        
        # # The buffer storing the entire data of the attribute.
        # const DataBuffer *buffer() const
        const core.DataBuffer *buffer()

        # Returns the number of bytes between two attribute entries, this is, at
        # least size of the data types times number of components.
        # int64_t byte_stride() const
        int64_t byte_stride()

        # The offset where the attribute starts within the block of size byte_stride.
        # int64_t byte_offset() const
        int64_t byte_offset()

        void set_byte_offset(int64_t byte_offset)

        # DataBufferDescriptor buffer_descriptor() const
        # DataBufferDescriptor buffer_descriptor()

        # uint32_t unique_id() const
        uint32_t unique_id()

        # void set_unique_id(uint32_t id)
        void set_unique_id(uint32_t id)

        # friend struct GeometryAttributeHasher;


###


# attribute_transform_data.h
# Class for holding parameter values for an attribute transform of a
# PointAttribute. This can be for example quantization data for an attribute
# that holds quantized values. This class provides only a basic storage for
# attribute transform parameters and it should be accessed only through wrapper
# classes for a specific transform (e.g. AttributeQuantizationTransform).
cdef extern from "draco/attributes/attribute_transform.h" namespace "draco" nogil:
    cdef cppclass AttributeTransformData:
        AttributeTransformData()
        # AttributeTransformData(const AttributeTransformData &data) = default;

        # # Returns the type of the attribute transform that is described by the class.
        # AttributeTransformType transform_type() const
        # void set_transform_type(AttributeTransformType type)
        AttributeTransformType transform_type()
        void set_transform_type(AttributeTransformType type)

        # # Returns a parameter value on a given |byte_offset|.
        # template <typename DataTypeT>
        # DataTypeT GetParameterValue(int byte_offset) const
        DataTypeT GetParameterValue [DataTypeT](int byte_offset)

        # # Sets a parameter value on a given |byte_offset|.
        # template <typename DataTypeT>
        # void SetParameterValue(int byte_offset, const DataTypeT &in_data)
        void SetParameterValue[DataTypeT](int byte_offset, const DataTypeT &in_data)

        # # Sets a parameter value at the end of the |buffer_|.
        # template <typename DataTypeT>
        # void AppendParameterValue(const DataTypeT &in_data)
        void AppendParameterValue [DataTypeT](const DataTypeT &in_data)


###

# Hashing support

# Function object for using Attribute as a hash key.
# struct GeometryAttributeHasher
#     size_t operator()(const GeometryAttribute &va) const


# Function object for using GeometryAttribute::Type as a hash key.
# struct GeometryAttributeTypeHasher
#     size_t operator()(const GeometryAttribute::Type &at) const
#     size_t operator()(const Type2 &at) const


# point_attribute.h
# Class for storing point specific data about each attribute. In general,
# multiple points stored in a point cloud can share the same attribute value
# and this class provides the necessary mapping between point ids and attribute
# value ids.
cdef extern from "draco/attributes/point_attribute.h" namespace "draco" nogil:
    cdef cppclass PointAttribute(GeometryAttribute):
        PointAttribute()
        # explicit PointAttribute(const GeometryAttribute &att);

        # Make sure the move constructor is defined (needed for better performance
        # when new attributes are added to PointCloud).
        # PointAttribute(PointAttribute &&attribute) = default;
        # PointAttribute &operator=(PointAttribute &&attribute) = default;

        # Copies attribute data from the provided |src_att| attribute.
        void CopyFrom(const PointAttribute &src_att)

        # Prepares the attribute storage for the specified number of entries.
        # bool Reset(size_t num_attribute_values);
        bool Reset(size_t num_attribute_values)

        # size_t size() const
        size_t size()

        # AttributeValueIndex mapped_index(PointIndex point_index) const
        AttributeValueIndex mapped_index(PointIndex point_index)

        # DataBuffer *buffer() const
        core.DataBuffer* buffer()

        # bool is_mapping_identity() const
        bool is_mapping_identity()

        # size_t indices_map_size() const
        size_t indices_map_size()

        # const uint8_t *GetAddressOfMappedIndex(PointIndex point_index) const
        
        # Sets the new number of unique attribute entries for the attribute.
        # void Resize(size_t new_num_unique_entries)
        void Resize(size_t new_num_unique_entries)

        # Functions for setting the type of mapping between point indices and attribute entry ids.
        # This function sets the mapping to implicit, where point indices are equal to attribute entry indices.
        void SetIdentityMapping()

        # This function sets the mapping to be explicitly using the indices_map_
        # array that needs to be initialized by the caller.
        void SetExplicitMapping(size_t num_points)

        # Set an explicit map entry for a specific point index.
        # void SetPointMapEntry(PointIndex point_index, AttributeValueIndex entry_index)

        # Sets a value of an attribute entry. The input value must be allocated to
        # cover all components of a single attribute entry.
        # void SetAttributeValue(AttributeValueIndex entry_index, const void *value)

        # Same as GeometryAttribute::GetValue(), but using point id as the input.
        # Mapping to attribute value index is performed automatically.
        # void GetMappedValue(PointIndex point_index, void *out_data) const

        # Deduplicate |in_att| values into |this| attribute. |in_att| can be equal to |this|.
        # Returns -1 if the deduplication failed.
        # AttributeValueIndex::ValueType DeduplicateValues(const GeometryAttribute &in_att);

        # Same as above but the values read from |in_att| are sampled with the
        # provided offset |in_att_offset|.
        # AttributeValueIndex::ValueType DeduplicateValues(const GeometryAttribute &in_att, AttributeValueIndex in_att_offset)

        # Set attribute transform data for the attribute. The data is used to store
        # the type and parameters of the transform that is applied on the attribute data (optional).
        # void SetAttributeTransformData(std::unique_ptr<AttributeTransformData> transform_data)

        # const AttributeTransformData *GetAttributeTransformData() const
        const AttributeTransformData* GetAttributeTransformData()

        # friend struct PointAttributeHasher;


###

# Hash functor for the PointAttribute class.
# struct PointAttributeHasher
#     size_t operator()(const PointAttribute &attribute) const

# attribute_transform.h
# Virtual base class for various attribute transforms, enforcing common
# interface where possible.
cdef extern from "draco/attributes/attribute_transform.h" namespace "draco" nogil:
    cdef cppclass AttributeTransform:
        AttributeTransform()
        # virtual ~AttributeTransform() = default;
        
        # # Return attribute transform type.
        # virtual AttributeTransformType Type() const = 0;
        AttributeTransformType Type()
        
        # # Try to init transform from attribute.
        # virtual bool InitFromAttribute(const PointAttribute &attribute) = 0;
        bool InitFromAttribute(const PointAttribute &attribute)
        
        # Copy parameter values into the provided AttributeTransformData instance.
        # virtual void CopyToAttributeTransformData(AttributeTransformData *out_data) const = 0;
        void CopyToAttributeTransformData(AttributeTransformData *out_data)
        
        # bool TransferToAttribute(PointAttribute *attribute) const;
        bool TransferToAttribute(PointAttribute *attribute)


###


###############################################################################
# Enum
###############################################################################

# public:
# # Supported attribute types.
# enum Type {
#   INVALID = -1,
#   # Named attributes start here. The difference between named and generic
#   # attributes is that for named attributes we know their purpose and we
#   # can apply some special methods when dealing with them (e.g. during
#   # encoding).
#   POSITION = 0,
#   NORMAL,
#   COLOR,
#   TEX_COORD,
#   # A special id used to mark attributes that are not assigned to any known
#   # predefined use case. Such attributes are often used for a shader specific
#   # data.
#   GENERIC,
#   # Total number of different attribute types.
#   # Always keep behind all named attributes.
#   NAMED_ATTRIBUTES_COUNT,

cdef extern from "draco/attributes/geometry_attribute.h" namespace "draco":
    ctypedef enum Type2 "draco::GeometryAttribute::Type":
        Type_INVALID "draco::GeometryAttribute::INVALID"
        Type_POSITION "draco::GeometryAttribute::POSITION"
        Type_NORMAL "draco::GeometryAttribute::NORMAL"
        Type_COLOR "draco::GeometryAttribute::COLOR"
        Type_TEX_COORD "draco::GeometryAttribute::TEX_COORD"
        Type_GENERIC "draco::GeometryAttribute::GENERIC"
        Type_NAMED_ATTRIBUTES_COUNT "draco::GeometryAttribute::NAMED_ATTRIBUTES_COUNT"


###


# attribute_transform_type.h
# List of all currently supported attribute transforms.
cdef extern from "draco/attributes/attribute_transform_type.h" namespace "draco":
    cdef enum AttributeTransformType:
        ATTRIBUTE_INVALID_TRANSFORM = -1,
        ATTRIBUTE_NO_TRANSFORM = 0,
        ATTRIBUTE_QUANTIZATION_TRANSFORM = 1,
        ATTRIBUTE_OCTAHEDRON_TRANSFORM = 2,


