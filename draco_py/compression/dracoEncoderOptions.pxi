# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
# from cython cimport address


cdef extern from "mini_draco.h":
    # void draco_set_default_configure(compress.EncoderOptions* options) except +
    compress.EncoderOptions* draco_get_default_encode_configure() except +


cdef class dracoEncoderOptions:
    """
    """
    def __cinit__(self):
        # NG
        # self.options = compress.CreateDefaultEncoderOptions()
        # self.options = <compress.EncoderOptions*> &(compress.CreateDefaultEncoderOptions())
        # self.options = <compress.EncoderOptions*> cython.address(compress.CreateDefaultEncoderOptions())
        # draco_set_default_configure(self.options)
        self.options = draco_get_default_encode_configure()


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    # static EncoderOptions CreateDefaultOptions();
    # Sets the global options that serve to control the overal behavior of an
    # encoder as well as a fallback for attribute options if they are not set.
    # void SetGlobalOptions(const Options &o)
    # def SetGlobalOptions(self, const Options &o):
    #     self.options.SetGlobalOptions(o)


    def SetGlobalInt(self, propertyName, int val):
        self.options.SetGlobalInt(self._encode(propertyName), val)


    # return int
    def GetGlobalInt(self, propertyName, int default_val):
        return self.options.GetGlobalInt(self._encode(propertyName), default_val)


    # return bool
    def GetGlobalBool(self, propertyName, bool default_val):
        return self.options.GetGlobalBool(self._encode(propertyName), default_val)


    def SetGlobalBool(self, propertyName, bool val):
        self.options.SetGlobalBool(self._encode(propertyName), val)


    def SetAttributeInt(self, int attrType, propertyName, int val):
        self.options.SetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), val)


    def GetAttributeInt(self, int attrType, propertyName, int default_val):
        return self.options.GetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), default_val)


    def SetAttributeBool(self, int attrType, propertyName, bool val):
        self.options.SetAttributeBool(<attribute.Type2>(attrType), self._encode(propertyName), val)


    def GetAttributeBool(self, int attrType, propertyName, bool default_val):
        return self.options.GetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), default_val)

    
    # void SetGlobalString(const string &name, const string &val)
    # def SetGlobalString(self, const string &name, const string &val):
    #    self.options.SetGlobalString(name, val)


    # string GetGlobalString(const string &name, const string &default_val) const
    # def GetGlobalString(self, const string &name, const string &val):
    #     return self.options.GetGlobalString(name, val)
    
    # void SetAttributeString(int32_t att_id, const string &name, const string &val)
    # def SetAttributeString(self, int attrType, propertyName, val):
    #     self.option.SetAttributeString(attrType, propertyName, val)


    # Get an option for a specific attribute. If the option is not found in an
    # attribute specific storage, the implementation will return a global option
    # of the given name (if available).
    # string GetAttributeString(int32_t att_id, const string &name, const string &default_val) const;
    # return string?
    # def GetAttributeString(self, int attrType, propertyName, val):
    #     self.option.GetAttributeString(attrType, propertyName, val)


    # Sets options for a specific attribute in a target PointCloud.
    # void SetAttributeOptions(int32_t att_id, const Options &o)
    # def SetAttributeOptions(self, att_id, o):
    #     return self.options.SetAttributeOptions()


    # Options *GetAttributeOptions(int32_t att_id)
    # def GetAttributeOptions(self, int att_id):
    #     return self.options.GetAttributeOptions(att_id)


    # Sets options for all attributes of a given type in the target point cloud.
    # void SetNamedAttributeOptions(const PointCloud &pc, GeometryAttribute::Type att_type, const Options &o)
    # def SetNamedAttributeOptions(self, const PointCloud &pc, GeometryAttribute::Type att_type, const Options &o):
    #     self.options.SetNamedAttributeOptions(pc, att_type, o)


    # Options *GetNamedAttributeOptions(const PointCloud &pc, GeometryAttribute::Type att_type)
    # def GetNamedAttributeOptions(self, const PointCloud &pc, GeometryAttribute::Type att_type):
    #     self.options.GetNamedAttributeOptions(pc, att_type)


    # Sets the list of features enabled by the encoder.
    # void SetFeatureOptions(const Options &o)
    # def SetFeatureOptions(self, const Options &o):
    #     self.options.SetFeatureOptions(o)


    # Returns speed options with default value of 5.
    def GetEncodingSpeed(self):
        return self.options.GetEncodingSpeed()


    def GetDecodingSpeed(self):
        return self.options.GetDecodingSpeed()


    def GetSpeed(self):
        return self.options.GetSpeed()


    # Sets a given feature as supported or unsupported by the target decoder.
    # Encoder will always use only supported features when encoding the input geometry.
    # void SetSupportedFeature(const string &name, bool supported)
    def SetSupportedFeature(self, name, supported):
        self.options.SetSupportedFeature(name, supported)


    # bool IsFeatureSupported(const string &name) const
    def IsFeatureSupported(self, name):
        self.options.IsFeatureSupported(name)


# cdef dracoCreateDefaultOptions():
#     p = dracoEncoderOptions()
#     # cdef compress.EncoderOptions options
#     options = compress.CreateDefaultEncoderOptions()
#     p.options = options
#     # return p
#     # return compress.CreateDefaultEncoderOptions()

