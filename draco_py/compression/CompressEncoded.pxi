# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_io_defs as io
cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
cimport move

from libcpp.memory cimport unique_ptr

def GetEncodedGeometryType(DecoderBuffer buffer):
    # geom_type = compress.GetEncodedGeometryType(buffer.module)
    # StatusOr
    geom_type = (compress.GetEncodedGeometryType(buffer.module)).value()
    return geom_type


def DecodeMeshFromBuffer(DecoderBuffer buffer):
    mesh = Mesh(buffer)
    return mesh


def DecodePointCloudFromBuffer(DecoderBuffer buffer):
    pointcloud = PointCloud(buffer)
    return pointcloud


# 
def ReadMeshFromFile(filepath):
    mesh_data = Mesh()
    cdef unique_ptr[mesh.Mesh] unique_mesh
    unique_mesh = io.ReadMeshFromFile(filepath)
    # NG(unique_mesh function leave pointer release)
    # mesh_data.mesh = unique_mesh.get()
    mesh_data.mesh = unique_mesh.release()
    mesh_data.points = move(mesh_data.mesh)
    return mesh_data


def ReadPointCloudFromFile(filepath):
    points_data = PointCloud()
    cdef unique_ptr[point_cloud.PointCloud] unique_points
    unique_points = io.ReadPointCloudFromFile(filepath)
    # NG(unique_mesh function leave pointer release)
    # mesh_data.mesh = unique_mesh.get()
    points_data.points = unique_points.release()
    return points_data


# return EncodeBuffer
def EncodeMeshToBuffer(Mesh mesh, dracoEncoderOptions options):
    buffer = EncoderBuffer ()
    
    # debug
    # NG
    # print(str(deref(mesh.mesh)))
    # print(str(deref(options.options)))
    
    # bool EncodeMeshToBuffer(const mesh.Mesh &m, const dracoEncoderOptions &options, core.EncoderBuffer *out_buffer)
    # compress.EncodeMeshToBuffer(deref(mesh.mesh), deref(options.options), buffer.module)
    compress.EncodeMeshToBuffer(deref(mesh.mesh), buffer.module)
    
    # OK
    # print(buffer.module.size())
    return buffer

def SetEncodingMethod(dracoEncoderOptions options):
    # compress.SetEncodingMethod(options.options, 0)
    compress.SetEncodingMethod(0)

def EncodePointCloudToBuffer(PointCloud pc, dracoEncoderOptions options):
    buffer = EncoderBuffer ()
    # EncodePointCloudToBuffer
    compress.EncodePointCloudToBuffer(deref(pc.points), buffer.module)
    return buffer

# def SetNamedAttributeQuantization(dracoEncoderOptions options, PointCloud pc, int type, int quantization_bits):
#     compress.SetNamedAttributeQuantization(options.options, deref(pc.points), <attribute.Type2>type, quantization_bits)

def SetSpeedOptions(dracoEncoderOptions options, int encoding_speed,int decoding_speed):
    compress.SetSpeedOptions(encoding_speed, decoding_speed)

# cdef unique_ptr[mesh.Mesh] unique_mesh
# unique_mesh = compress.DecodeMeshFromBuffer(buffer.module)
# # NG
# # self.mesh = <mesh.Mesh*>unique_mesh.get()
# # pointer manage move
# self.mesh = <mesh.Mesh*>unique_mesh.release()