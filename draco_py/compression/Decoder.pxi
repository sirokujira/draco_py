# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.memory cimport unique_ptr

cdef class Decoder:
    """
    """
    def __cinit__(self):
        self.me = new compress.Decoder()


    def DecodePointCloudFromBuffer(self, DecoderBuffer buffer):
        # core.StatusOr[unique_ptr[point_cloud.PointCloud]] 
        # StatusOr[unique_ptr[Mesh]]
        # cdef core.StatusOr[unique_ptr[point_cloud.PointCloud]] unique_pc
        # unique_pc = self.me.DecodePointCloudFromBuffer(buffer.module)
        # cdef unique_ptr[point_cloud.PointCloud] data = unique_pc.value()
        cdef unique_ptr[point_cloud.PointCloud] data
        data = (self.me.DecodePointCloudFromBuffer(buffer.module)).value()
        
        pc = PointCloud()
        pc.points = <point_cloud.PointCloud*>data.release()
        return pc


    # """
    # Decodes a triangular mesh from the provided buffer. The mesh must be filled
    # with data that was encoded using the EncodeMeshToBuffer method in encode.h.
    # The function will return nullptr in case the input is invalid or if it was
    # encoded with the EncodePointCloudToBuffer method.
    # unique_ptr[mesh.Mesh] DecodeMeshFromBuffer(core.DecoderBuffer *in_buffer)
    # """
    def DecodeMeshFromBuffer(self, DecoderBuffer buffer):
        # core.StatusOr[unique_ptr[mesh.Mesh]] 
        # cdef core.StatusOr[unique_ptr[mesh.Mesh]] unique_mesh
        # unique_mesh = self.me.DecodeMeshFromBuffer(buffer.module)
        # cdef unique_ptr[mesh.Mesh] data = unique_mesh.value()
        cdef unique_ptr[mesh.Mesh] data
        data = (self.me.DecodeMeshFromBuffer(buffer.module)).value()
        
        meshdata = Mesh()
        meshdata.mesh = <mesh.Mesh*>data.release()
        return meshdata


    def SetSkipAttributeTransform(self, att_type):
        # self.me.SetSkipAttributeTransform(<attribute.Type2> att_type)
        # self.me.SetSkipAttributeTransform(att_type)
        pass


