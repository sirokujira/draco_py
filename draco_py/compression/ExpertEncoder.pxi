# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

cimport _draco
from cython cimport address
from cython.operator cimport dereference as deref, preincrement as inc

cdef class ExpertEncoder:
    """
    """

    def __cinit__(self, init=None):
        if init is None:
            pass
        # elif isinstance(init, _draco.PointCloud):
        #     self.me = new compress.ExpertEncoder_PointCloud(deref(<_draco.PointCloud> init).points))
        elif isinstance(init, _draco.Mesh):
            self.me = new compress.ExpertEncoder(deref((<_draco.Mesh> init).mesh))
            # self.me = new compress.ExpertEncoder(init.mesh)
        else:
            raise TypeError("Can't initialize a ExpertEncoder from a %s" % type(init))


    # return core.Status?
    def EncodeToBuffer(self, _draco.EncoderBuffer out_buffer):
        retStatus = Status()
        retStatus.status = self.me.EncodeToBuffer(out_buffer.module)
        return retStatus


    def Reset(self):
        self.me.Reset()


    def ResetOptions(self, _draco.dracoEncoderOptions options):
        # self.me.ResetOptions(options.options)
        self.me.ResetOptions(deref(options.options))


    def SetSpeedOptions(self, int encoding_speed, int decoding_speed):
        self.me.SetSpeedOptions(encoding_speed, decoding_speed)


    # def SetAttributeQuantization(self, int32_t attribute_id, int quantization_bits):
    def SetAttributeQuantization(self, int attribute_id, int quantization_bits):
        self.me.SetAttributeQuantization(attribute_id, quantization_bits)


    def SetUseBuiltInAttributeCompression(self, bool enabled):
        self.me.SetUseBuiltInAttributeCompression(enabled)


    def SetEncodingMethod(self, int encoding_method):
        self.me.SetEncodingMethod(encoding_method)


    # def SetAttributePredictionScheme(self, int32_t attribute_id, int prediction_scheme_method):
    def SetAttributePredictionScheme(self, int attribute_id, int prediction_scheme_method):
        self.me.SetAttributePredictionScheme(attribute_id, prediction_scheme_method)


