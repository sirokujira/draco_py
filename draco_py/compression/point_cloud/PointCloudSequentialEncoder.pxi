# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud


from libc.stdint cimport int32_t

from libcpp cimport bool
# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cdef class PointCloudSequentialEncoder:
    """
    """
    def __cinit__(self, init=None):
        if init is None:
            self.points = new compress.PointCloudSequentialEncoder()
        else:
            raise TypeError("Can't initialize a PointCloudSequentialEncoder from a %s" % type(init))


    def SetPointCloud(self, PointCloud pc):
        (<compress.PointCloudEncoder*> self.points).SetPointCloud(deref(pc.points))


    # def Encode(self, EncoderOptions options, EncoderBuffer buffer):
    def Encode(self, dracoEncoderOptions options, EncoderBuffer buffer):
        status = Status()
        status.status = (<compress.PointCloudEncoder*> self.points).Encode(deref(options.options), buffer.module)
        return status


    # return AttributesEncoder*
    def attributes_encoder(self, int i):
        (<compress.PointCloudEncoder*> self.points).attributes_encoder(i)


    # bool 
    def MarkParentAttribute(self, int parent_att_id):
        return (<compress.PointCloudEncoder*> self.points).MarkParentAttribute(<int32_t> parent_att_id)


    # return const attribute.PointAttribute* 
    def GetPortableAttribute(self, int point_attribute_id):
        # return (<compress.PointCloudEncoder*> self.points).GetPortableAttribute(<int32_t> point_attribute_id)
        (<compress.PointCloudEncoder*> self.points).GetPortableAttribute(<int32_t> point_attribute_id)


    def buffer(self):
        encBuf = EncoderBuffer()
        encBuf.module = (<compress.PointCloudEncoder*> self.points).buffer()
        return encBuf


    # const EncoderOptions* 
    def options(self):
        (<compress.PointCloudEncoder*> self.points).options()


    def point_cloud(self):
        # pc = PointCloud()
        # pc.points = (<compress.PointCloudEncoder*> self.points).point_cloud()
        # return pc
        (<compress.PointCloudEncoder*> self.points).point_cloud()


###