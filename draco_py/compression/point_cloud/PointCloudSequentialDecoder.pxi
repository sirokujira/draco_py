# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cdef class PointCloudSequentialDecoder:
    """
    """
    def __cinit__(self, init=None):
        if init is None:
            self.points = new compress.PointCloudSequentialDecoder()
        else:
            raise TypeError("Can't initialize a PointCloudSequentialDecoder from a %s" % type(init))
    def Decode(self, dracoDecoderOptions options, DecoderBuffer buffer, PointCloud pc):
        status = Status()
        status.status = (<compress.PointCloudDecoder*> self.points).Decode(deref(options.options), buffer.module, pc.points)
        return status


    # return const attribute.PointAttribute* 
    def GetPortableAttribute(self, int point_attribute_id):
        # return (<compress.PointCloudDecoder*> self.points).GetPortableAttribute(<int32_t> point_attribute_id)
        (<compress.PointCloudDecoder*> self.points).GetPortableAttribute(<int32_t> point_attribute_id)


    # return uint16_t
    def bitstream_version(self):
        return (<compress.PointCloudDecoder*> self.points).bitstream_version()


    # return int32_t 
    def num_attributes_decoders(self):
        return (<compress.PointCloudDecoder*> self.points).num_attributes_decoders()


    def buffer(self):
        decBuf = DecoderBuffer()
        decBuf.module = (<compress.PointCloudDecoder*> self.points).buffer()
        return decBuf


    # const DecoderOptions* 
    def options(self):
        (<compress.PointCloudDecoder*> self.points).options()


    def point_cloud(self):
        # pc = PointCloud()
        # pc.points = (<compress.PointCloudDecoder*> self.points).point_cloud()
        # return pc
        (<compress.PointCloudDecoder*> self.points).point_cloud()


###