# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cdef class PointCloudKdTreeDecoder:
    """
    """
    def __cinit__(self, init=None):
        if init is None:
            self.points = new compress.PointCloudKdTreeDecoder()
        else:
            raise TypeError("Can't initialize a PointCloudKdTreeDecoder from a %s" % type(init))


###

