# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
from cython cimport address
from cython.operator cimport dereference as deref, preincrement as inc


cdef class DecoderOptions:
    """
    """
    def __cinit__(self):
        self.options = new compress.DecoderOptions()
        pass


    def _encode(path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    def SetGlobalInt(self, propertyName, int i):
        self.options.SetGlobalInt(_encode(propertyName), i)


    def SetAttributeInt(self, attrType, int i):
        self.options.SetAttributeInt(attrType, i)


    def GetAttributeInt(self, attrType, propertyName, int i):
        return self.options.GetAttributeInt(attrType, _encode(propertyName), i)


cdef CreateDefaultOptions():
    p = DecoderOptions()
    p.options = options
    return p

