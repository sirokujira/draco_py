# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

cimport _draco

cdef class Encoder:
    """
    """
    def __cinit__(self):
        self.me = new compress.Encoder()


    def EncodePointCloudToBuffer(self, _draco.PointCloud pc):
        # core.Status EncodePointCloudToBuffer(const point_cloud.PointCloud &pc, core.EncoderBuffer *out_buffer)
        # Encodes mesh to the provided buffer.
        buffer = EncoderBuffer()
        self.me.EncodePointCloudToBuffer(deref(pc.points), buffer.module)
        return buffer


    def EncodeMeshToBuffer(self, _draco.Mesh mesh):
        # core.Status EncodeMeshToBuffer(const mesh.Mesh &m, core.EncoderBuffer *out_buffer)
        buffer = EncoderBuffer()
        self.me.EncodeMeshToBuffer(deref(mesh.mesh), buffer.module)
        return buffer


    def SetEncodingMethod(self, int encoding_method):
        self.me.SetEncodingMethod(encoding_method)


    def SetSpeedOptions(self, int encoding_speed, int decoding_speed):
        self.me.SetSpeedOptions(encoding_speed, decoding_speed)


    # Sets the above quantization directly for a specific attribute |options|.
    # void SetAttributeQuantization(GeometryAttribute::Type type, int quantization_bits)
    def SetAttributeQuantization(self, int type, int quantization_bits):
        self.me.SetAttributeQuantization(<attribute.Type2>type, quantization_bits)


