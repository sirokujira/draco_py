# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp


cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
from cython cimport address
from cython.operator cimport dereference as deref, preincrement as inc
# from cython cimport address

# cdef extern from "mini_draco.h":
#    compress.DecoderOptions* draco_get_default_decode_configure() except +


cdef class dracoDecoderOptions:
    """
    """
    def __init__(self):
        # self.options = draco_get_default_decode_configure()
        # self.options = Decoder.Options()
        self.options = new compress.DecoderOptions()


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    def SetGlobalInt(self, propertyName, int val):
        self.options.SetGlobalInt(self._encode(propertyName), val)


    # return int
    def GetGlobalInt(self, propertyName, int default_val):
        return self.options.GetGlobalInt(self._encode(propertyName), default_val)


    # return bool
    def GetGlobalBool(self, propertyName, bool default_val):
        return self.options.GetGlobalBool(self._encode(propertyName), default_val)


    def SetGlobalBool(self, propertyName, bool val):
        self.options.SetGlobalBool(self._encode(propertyName), val)


    # return bool
    def IsGlobalOptionSet(self, name):
        self.options.IsGlobalOptionSet(self._encode(name))


    def SetAttributeInt(self, int attrType, propertyName, int val):
        self.options.SetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), val)


    def GetAttributeInt(self, int attrType, propertyName, int default_val):
        return self.options.GetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), default_val)


    def SetAttributeBool(self, int attrType, propertyName, bool val):
        self.options.SetAttributeBool(<attribute.Type2>(attrType), self._encode(propertyName), val)


    def GetAttributeBool(self, int attrType, propertyName, bool default_val):
        return self.options.GetAttributeInt(<attribute.Type2>(attrType), self._encode(propertyName), default_val)


#     def SetAttributeOptions(self, AttributeKeyT att_key, Options &options):
#         self.options.SetAttributeOptions(att_key, options.options)
# 
# 
#     def SetGlobalOptions(self, Options &options):
#         self.options.SetGlobalOptions(options.options)
# 
# 
#     # return Options
#     def SetGlobalOptions(self, Options &options):
#         # Returns |Options| instance for the specified options class if it exists.
#         const core.Options *FindAttributeOptions(const AttributeKeyT &att_key)
# 
# 
#     # return Options
#     def GetGlobalOptions(self):
#         const core.Options &GetGlobalOptions()
