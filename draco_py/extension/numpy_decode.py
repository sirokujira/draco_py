import numpy as np
import draco_py as draco


def convert_numpy_data(path, dtype=np.float32, point=True):
    """Convert draco files.
    
    Args:
        path (str): A path of draco file.
        dtype: The type of array. The default value is :obj:`~numpy.float32`.
        point (bool): This option .
            If :obj:`True`, .
            If :obj:`False`, .

        Returns:
        ~numpy.ndarray: An point.
    """
    mesh = draco.GetDecodedData(path)
    arraySize = mesh.num_points()
    # print(arraySize)

    x = []
    pos_att = mesh.GetNamedAttribute(draco.GeometryAttributeType_POSITION)
    # pos_nArray = np.zeros((arraySize,3), dtype=np.float32)
    for i in range(0, arraySize):
        val_index = pos_att.mapped_index(i)
        # print('val_index : ' + str(val_index))
        position = [0, 0, 0]
        # isFlag = pos_att.ConvertValueFloatPointer2(val_index, position)
        # print(isFlag)
        # print(position)
        position = pos_att.ConvertValueFloatPointer2(val_index, position)
        x.append(position)

    pos_nArray = np.array(x, dtype=np.float32)
    print(pos_nArray)
    # print(pos_nArray.shape)
    return pos_nArray
