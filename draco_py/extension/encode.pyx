from __future__ import division
import sys

import numpy
import six

import draco_py
from draco_py import draco_py as draco

cimport cpython
cimport cython
cimport numpy
from libcpp cimport vector
from libcpp cimport memory

from draco_py.extension cimport internal

cdef class numpy2dracoEncode:

    """3-dimensional array on .
    This class implements a subset of methods of :class:`numpy.ndarray`.
    Args:
    Attributes:
    """

    def __init__(self, shape, dtype=float, memptr=None):
        cdef Py_ssize_t x
        self._shape = internal.get_size(shape)
        for x in self._shape:
            if x < 0:
                raise ValueError('Negative dimensions are not allowed')
        self.dtype = numpy.dtype(dtype)
        self.size = internal.prod_ssize_t(self._shape)

        if memptr is None:
            self.data = memory.alloc(self.size * self.dtype.itemsize)
        else:
            self.data = memptr
        self.base = None


    property shape:
        """Lengths of axes.

        Setter of this property involves reshaping without copy. If the array
        cannot be reshaped without copy, it raises an exception.

        .. seealso: :attr:`numpy.ndarray.shape`

        """

        def __get__(self):
            return tuple(self._shape)

        def __set__(self, newshape):
            cdef vector.vector[Py_ssize_t] shape, strides
            if not cpython.PySequence_Check(newshape):
                newshape = (newshape,)
            shape = internal.infer_unknown_dimension(newshape, self.size)
            strides = self._get_strides_for_nocopy_reshape(self._shape, shape)
            if strides.size() != shape.size():
                raise AttributeError('incompatible shape')
            self._shape = shape
            self._strides = strides
            self._update_f_contiguity()

    @property
    def strides(self):
        """Strides of axes in bytes.

        .. seealso:: :attr:`numpy.ndarray.strides`

        """
        return tuple(self._strides)


    @property
    def ndim(self):
        """Number of dimensions.

        ``a.ndim`` is equivalent to ``len(a.shape)``.

        .. seealso:: :attr:`numpy.ndarray.ndim`

        """
        return self._shape.size()


    @property
    def itemsize(self):
        """Size of each element in bytes.

        .. seealso:: :attr:`numpy.ndarray.itemsize`

        """
        return self.dtype.itemsize


    @property
    def nbytes(self):
        """Size of whole elements in bytes.

        It does not count skips between elements.

        .. seealso:: :attr:`numpy.ndarray.nbytes`

        """
        return self.size * self.dtype.itemsize


    # -------------------------------------------------------------------------
    # Other attributes
    # -------------------------------------------------------------------------
    @property
    def T(self):
        """Shape-reversed view of the array.

        If ndim < 3, then this is just a reference to the array itself.

        """
        if self.ndim < 3:
            return self
        else:
            return self._transpose(vector.vector[Py_ssize_t]())

    # -------------------------------------------------------------------------
    # Array conversion
    # -------------------------------------------------------------------------
    cpdef tolist(self):
        """Converts the array to a (possibly nested) Python list.

        Returns:
            list: The possibly nested Python list of array elements.

        .. seealso:: :meth:`numpy.ndarray.tolist`

        """
        return self.get().tolist()


    cpdef tofile(self, fid, sep='', format='%s'):
        """Writes the array to a file.

        .. seealso:: :meth:`numpy.ndarray.tolist`

        """
        self.get().tofile(fid, sep, format)


    cpdef dump(self, file):
        """Dumps a pickle of the array to a file.
        Dumped file can be read back to :class:`cupy.ndarray` by
        :func:`cupy.load`.
        """
        six.moves.cPickle.dump(self, file, -1)


    cpdef dumps(self):
        """Dumps a pickle of the array to a string."""
        return six.moves.cPickle.dumps(self, -1)


    cpdef vector.vector[Py_ssize_t] _get_strides_for_nocopy_reshape(self,
            numpy.ndarray a, vector.vector[Py_ssize_t] & newshape) except *:
        cdef vector.vector[Py_ssize_t] newstrides
        cdef Py_ssize_t size, itemsize, ndim, dim, last_stride
        size = a.size
        if size != internal.prod_ssize_t(newshape):
            return newstrides

        itemsize = a.itemsize
        if size == 1:
            newstrides.assign(<Py_ssize_t>newshape.size(), itemsize)
            return newstrides

        cdef vector.vector[Py_ssize_t] shape, strides
        internal.get_reduced_dims(a._shape, a._strides, itemsize, shape, strides)

        ndim = shape.size()
        dim = 0
        sh = shape[0]
        st = strides[0]
        last_stride = shape[0] * strides[0]
        for size in newshape:
            if size <= 1:
                newstrides.push_back(last_stride)
                continue
            if dim >= ndim or shape[dim] % size != 0:
                newstrides.clear()
                break
            shape[dim] //= size
            last_stride = shape[dim] * strides[dim]
            newstrides.push_back(last_stride)
            if shape[dim] == 1:
                dim += 1
        return newstrides
