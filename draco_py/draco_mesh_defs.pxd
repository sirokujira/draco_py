# -*- coding: utf-8 -*-

from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.memory cimport unique_ptr

# main
cimport draco_attribute_defs as attribute
cimport draco_core_defs as core
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t

###############################################################################
# Types
###############################################################################

# corner_table.h
# namespace draco
# CornerTable is used to represent connectivity of triangular meshes.
# For every corner of all faces, the corner table stores the index of the
# opposite corner in the neighboring face (if it exists) as illustrated in the
# figure below (see corner |c| and it's opposite corner |o|).
# //
#     *
#    /c\
#   /   \
#  /n   p\
# *-------*
#  \     /
#   \   /
#    \o/
#     *
# //
# All corners are defined by unique CornerIndex and each triplet of corners
# that define a single face id always ordered consecutively as:
#     { 3 * FaceIndex, 3 * FaceIndex + 1, 3 * FaceIndex +2 }.
# This representation of corners allows CornerTable to easily retrieve Next and
# Previous corners on any face (see corners |n| and |p| in the figure above).
# Using the Next, Previous, and Opposite corners then enables traversal of any
# 2-manifold surface.
# If the CornerTable is constructed from a non-manifold surface, the input
# non-manifold edges and vertices are automatically split.
# corner_table.h
# namespace draco
# class CornerTable
cdef extern from "draco/mesh/corner_table.h" namespace "draco" nogil:
    cdef cppclass CornerTable:
        # public:
        CornerTable()
        # static std::unique_ptr<CornerTable> Create(const IndexTypeVector<FaceIndex, FaceType> &faces)
        # 
        # Initializes the CornerTable from provides set of indexed faces.
        # The input faces can represent a non-manifold topolgy, in which case the
        # non-manifold edges and vertices are going to be split.
        # bool Initialize(const IndexTypeVector<FaceIndex, FaceType> &faces);
        
        # Resets the corner table to the given number of invalid faces.
        # bool Reset(int num_faces);
        
        # inline int num_vertices() const
        # inline int num_corners() const
        # inline int num_faces() const
        
        # inline CornerIndex Opposite(CornerIndex corner) const
        # inline CornerIndex Next(CornerIndex corner) const
        # inline CornerIndex Previous(CornerIndex corner) const
        # inline VertexIndex Vertex(CornerIndex corner) const
        # inline FaceIndex Face(CornerIndex corner) const
        # inline CornerIndex FirstCorner(FaceIndex face) const
        # inline std::array<CornerIndex, 3> AllCorners(FaceIndex face) const
        # inline int LocalIndex(CornerIndex corner) const
        # inline FaceType FaceData(FaceIndex face) const
        # 
        # void SetFaceData(FaceIndex face, FaceType data)
        # 
        # Returns the left-most corner of a single vertex 1-ring. If a vertex is not
        # on a boundary (in which case it has a full 1-ring), this function returns
        # any of the corners mapped to the given vertex.
        # inline CornerIndex LeftMostCorner(VertexIndex v) const
        # 
        # Returns the parent vertex index of a given corner table vertex.
        # VertexIndex VertexParent(VertexIndex vertex) const
        # 
        # Returns true if the corner is valid.
        # inline bool IsValid(CornerIndex c) const
        # 
        # Returns the valence (or degree) of a vertex.
        # Returns -1 if the given vertex index is not valid.
        # int Valence(VertexIndex v) const;
        
        # Returns the valence of the vertex at the given corner.
        # inline int Valence(CornerIndex c) const 
        
        # Returns true if the specified vertex is on a boundary.
        # inline bool IsOnBoundary(VertexIndex vert) const 
        
        #     *-------*
        #    / \     / \
        #   /   \   /   \
        #  /   sl\c/sr   \
        # *-------v-------*
        # Returns the corner on the adjacent face on the right that maps to
        # the same vertex as the given corner (sr in the above diagram).
        # inline CornerIndex SwingRight(CornerIndex corner) const 
        
        # Returns the corner on the left face that maps to the same vertex as the
        # given corner (sl in the above diagram).
        # inline CornerIndex SwingLeft(CornerIndex corner) const 
        
        # Get opposite corners on the left and right faces respecitively (see image
        # below, where L and R are the left and right corners of a corner X.
        # 
        # *-------*-------*
        #  \L    /X\    R/
        #   \   /   \   /
        #    \ /     \ /
        #     *-------*
        # CornerIndex GetLeftCorner(CornerIndex corner_id) const 
        # 
        # CornerIndex GetRightCorner(CornerIndex corner_id) const 
        # 
        # Returns the number of new vertices that were created as a result of
        # spliting of non-manifold vertices of the input geometry.
        # int NumNewVertices() const 
        # int NumOriginalVertices() const 
        
        # Returns the number of faces with duplicated vertex indices.
        # int NumDegeneratedFaces() const 
        
        # Returns the number of isolated vertices (vertices that have
        # vertex_corners_ mapping set to kInvalidCornerIndex.
        # int NumIsolatedVertices() const 
        
        # bool IsDegenerated(FaceIndex face) const;
        
        # Methods that modify an existing corner table.
        # Sets the opposite corner mapping between two corners. Caller must ensure
        # that the indices are valid.
        # inline void SetOppositeCorner(CornerIndex corner_id,
        #                               CornerIndex opp_corner_id) 

        # Sets opposite corners for both input corners.
        # inline void SetOppositeCorners(CornerIndex corner_0, CornerIndex corner_1) 
        
        # Updates mapping betweeh a corner and a vertex.
        # inline void MapCornerToVertex(CornerIndex corner_id, VertexIndex vert_id) 
        
        # VertexIndex AddNewVertex() 
        # 
        # Sets a new left most corner for a given vertex.
        # void SetLeftMostCorner(VertexIndex vert, CornerIndex corner) 
        
        # Updates the vertex to corner map on a specified vertex. This should be
        # called in cases where the mapping may be invalid (e.g. when the corner
        # table was constructed manually).
        # void UpdateVertexToCornerMap(VertexIndex vert) 
        
        # Sets the new number of vertices. It's a responsibility of the caller to
        # ensure that no corner is mapped beyond the range of the new number of
        # vertices.
        # inline void SetNumVertices(int num_vertices) 
        # 
        # Makes a vertex isolated (not attached to any corner).
        # void MakeVertexIsolated(VertexIndex vert) 
        # 
        # Returns true if a vertex is not attached to any face.
        # inline bool IsVertexIsolated(VertexIndex v) const 
        # 
        # Makes a given face invalid (all corners are marked as invalid).
        # void MakeFaceInvalid(FaceIndex face) 
        # 
        # Updates mapping between faces and a vertex using the corners mapped to
        # the provided vertex.
        # void UpdateFaceToVertexMap(const VertexIndex vertex);


###

# TODO(ostava): All these iterators will be moved into a new file in a separate
# CL.

# corner_table.h
# namespace draco
# Class for iterating over vertices in a 1-ring around the specified vertex.
# class VertexRingIterator : public std::iterator<std::forward_iterator_tag, VertexIndex>
# cdef extern from "draco/mesh/corner_table.h" namespace "draco" nogil:
#     cdef cppclass VertexRingIterator:
        # public:
        # std::iterator interface requires a default constructor.
        # VertexRingIterator()
        # VertexRingIterator()
        #     : corner_table_(nullptr),
        #     start_corner_(kInvalidCornerIndex),
        #     corner_(start_corner_),
        #     left_traversal_(true) {}
        
        # Create the iterator from the provided corner table and the central vertex.
        # VertexRingIterator(const CornerTable *table, VertexIndex vert_id)
        # : corner_table_(table),
        # start_corner_(table->LeftMostCorner(vert_id)),
        # corner_(start_corner_),
        # left_traversal_(true) {}        
        # Gets the last visited ring vertex.
        # VertexIndex Vertex() const         
        # Returns true when all ring vertices have been visited.
        # bool End() const { return corner_ < 0; }        
        # Proceeds to the next ring vertex if possible.
        # void Next()         
        # std::iterator interface.
        # value_type operator*() const { return Vertex(); }
        # VertexRingIterator &operator++() 
        # VertexRingIterator operator++(int) 
        # bool operator!=(const VertexRingIterator &other) const 
        # bool operator==(const VertexRingIterator &other) const         
        # Helper function for getting a valid end iterator.
        # static VertexRingIterator EndIterator(VertexRingIterator other) 


###

# corner_table.h
# namespace draco
# Class for iterating over faces adjacent to the specified input face.
# class FaceAdjacencyIterator : public std::iterator<std::forward_iterator_tag, FaceIndex> {
cdef extern from "draco/mesh/corner_table.h" namespace "draco" nogil:
    cdef cppclass VertexRingIterator:
        # public:
        # std::iterator interface requires a default constructor.
        FaceAdjacencyIterator()
        # : corner_table_(nullptr),
        # start_corner_(kInvalidCornerIndex),
        # corner_(start_corner_) {}
        # 
        # Create the iterator from the provided corner table and the central vertex.
        # FaceAdjacencyIterator(const CornerTable *table, FaceIndex face_id)
        #       : corner_table_(table),
        #     start_corner_(table->FirstCorner(face_id)),
        #     corner_(start_corner_) 
        # 
        # Gets the last visited adjacent face.
        # FaceIndex Face() const 
        # 
        # Returns true when all adjacent faces have been visited.
        # bool End() const
        # 
        # Proceeds to the next adjacen face if possible.
        # void Next()
        # 
        # std::iterator interface.
        # value_type operator*() const { return Face(); }
        # FaceAdjacencyIterator &operator++() 
        # 
        # FaceAdjacencyIterator operator++(int) 
        # 
        # bool operator!=(const FaceAdjacencyIterator &other) const 
        # bool operator==(const FaceAdjacencyIterator &other) const 
        # 
        # Helper function for getting a valid end iterator.
        # static FaceAdjacencyIterator EndIterator(FaceAdjacencyIterator other) 


###

# corner_table_indices.h
# namespace draco
# Vertex index in a corner table.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, VertexIndex);

# corner_table_indices.h
# namespace draco
# Corner index that identifies each corner of every corner table face.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, CornerIndex);

# corner_table_indices.h
# namespace draco
# Constants denoting invalid indices.
# static constexpr VertexIndex kInvalidVertexIndex(std::numeric_limits<int32_t>::min() / 2);
# static constexpr CornerIndex kInvalidCornerIndex(std::numeric_limits<int32_t>::min() / 2);

# Corner table face type.
# typedef std::array<VertexIndex, 3> FaceType;

# A special case to denote an invalid corner table triangle.
# static constexpr std::array<VertexIndex, 3> kInvalidFace({{kInvalidVertexIndex, kInvalidVertexIndex, kInvalidVertexIndex}});


###

# corner_table_iterators.h
# namespace draco
# TODO(ostava): Move the remaining corner table iterators here.
# Class for iterating over corners attached to a specified vertex.
# template <class CornerTableT = CornerTable>
# class VertexCornersIterator : public std::iterator<std::forward_iterator_tag, CornerIndex> {
# cdef extern from "draco/mesh/corner_table_iterators.h" namespace "draco" nogil:
#     cdef cppclass VertexCornersIterator(iterator[forward_iterator_tag, CornerIndex]):
        # public:
        # std::iterator interface requires a default constructor.
        VertexCornersIterator()
        # Create the iterator from the provided corner table and the central vertex.
        # VertexCornersIterator(const CornerTableT *table, VertexIndex vert_id)
        # Create the iterator from the provided corner table and the first corner.
        # VertexCornersIterator(const CornerTableT *table, CornerIndex corner_id)
        # Gets the last visited corner.
        # CornerIndex Corner() const
        # Returns true when all ring vertices have been visited.
        # bool End() const
        # Proceeds to the next corner if possible.
        # void Next()
        # std::iterator interface.
        # CornerIndex operator*() const
        # VertexCornersIterator &operator++()
        # VertexCornersIterator operator++(int)
        # bool operator!=(const VertexCornersIterator &other) const
        # bool operator==(const VertexCornersIterator &other) const
        # 
        # Helper function for getting a valid end iterator.
        # static VertexCornersIterator EndIterator(VertexCornersIterator other)


###

# corner_table_traversal_processor.h
# namespace draco
# Class providing the basic traversal funcionality needed by traversers (such
# as the EdgeBreakerTraverser, see edgebreaker_traverser.h). It is used to
# return the corner table that is used for the traversal, plus it provides a
# basic book-keeping of visited faces and vertices during the traversal.
# template <class CornerTableT>
# class CornerTableTraversalProcessor {
cdef extern from "draco/mesh/corner_table_traversal_processor.h" namespace "draco" nogil:
    cdef cppclass CornerTableTraversalProcessor[CornerTableT]:
        # public:
        # typedef CornerTableT CornerTable;
        # CornerTableTraversalProcessor() : corner_table_(nullptr)
        CornerTableTraversalProcessor()
        # virtual ~CornerTableTraversalProcessor() = default;
        # 
        # void ResetProcessor(const CornerTable *corner_table)
        # const CornerTable &GetCornerTable() const
        # inline bool IsFaceVisited(FaceIndex face_id) const
        # 
        # Returns true if the face containing the given corner was visited.
        # inline bool IsFaceVisited(CornerIndex corner_id) const
        # 
        # inline void MarkFaceVisited(FaceIndex face_id)
        # inline bool IsVertexVisited(VertexIndex vert_id) const
        # inline void MarkVertexVisited(VertexIndex vert_id)


###

# edgebreaker_observer.h
# namespace draco
# A default implementation of the observer that can be used inside of the
# EdgeBreakerTraverser (edgebreaker_traverser.h). The default implementation
# results in no-op for all calls.
# class EdgeBreakerObserver {
# cdef extern from "draco/mesh/edgebreaker_observer.h" namespace "draco" nogil:
#     cdef cppclass EdgeBreakerObserver:
        # EdgeBreakerObserver()
        # public:
        # inline void OnSymbolC()
        # inline void OnSymbolL()
        # inline void OnSymbolR()
        # inline void OnSymbolS()
        # inline void OnSymbolE()


###

# edgebreaker_traverser.h
# namespace draco
# Basic framework for edgebreaker traversal over a corner table data
# structure. The traversal and the callbacks are handled through the template
# arguments TraversalProcessorT, TraversalObserverT and EdgeBreakerObserverT.
# TraversalProcessorT is used to provide infrastructure for handling of visited
# vertices and faces, TraversalObserverT can be used to implement custom
# callbacks for varous traversal events, and EdgeBreakerObserverT can be used
# to provide handling of edgebreaker symbols.
# TraversalProcessorT needs to define the type of the corner table as:
# //
# TraversalProcessorT::CornerTable
# //
# and it needs to implement the following methods:
# //
# CornerTable GetCornerTable();
#    - Returns corner table for a given processor.
# //
# bool IsVertexVisited(VertexIndex vertex);
#    - Returns true if the vertex has been already marked as visited during
#      the traversal.
# //
# void MarkVertexVisited(VertexIndex vertex);
#    - Informs the processor that the vertex has been reached.
# //
# bool IsFaceVisited(FaceIndex face);
#    - Returns true if the face has been already marked as visited during
#      the traversal.
# //
# void MarkFaceVisited(FaceIndex face);
#    - Should be used to mark the newly visited face.
# //
# ----------------------------------------------------
# //
# TraversalObserverT can perform an action on a traversal event such as newly
# visited face, or corner, but it does not affect the traversal itself.
# It needs to implement the following methods:
# //
# void OnNewFaceVisited(FaceIndex face);
#    - Called whenever a previously unvisited face is reached.
# //
# void OnNewVertexVisited(VertexIndex vert, CornerIndex corner)
#    - Called when a new vertex is visited. |corner| is used to indicate the
#      which of the vertex's corners has been reached.
# //
# ----------------------------------------------------
# //
# EdgeBreakerObserverT then needs to implement following methods:
# //
# void OnSymbolC();
# void OnSymbolL();
# void OnSymbolR();
# void OnSymbolE();
# void OnSymbolS();
#    - Informs the observer about the configuration of a newly visited face.
# 
# template <class TraversalProcessorT, class TraversalObserverT, class EdgeBreakerObserverT = EdgeBreakerObserver>
# class EdgeBreakerTraverser {
cdef extern from "draco/mesh/edgebreaker_traverser.h" namespace "draco" nogil:
    cdef cppclass EdgeBreakerTraverser[TraversalProcessorT, TraversalObserverT, EdgeBreakerObserverT]:
        EdgeBreakerTraverser()
        # public:
        # typedef TraversalProcessorT TraversalProcessor;
        # typedef TraversalObserverT TraversalObserver;
        # typedef typename TraversalProcessorT::CornerTable CornerTable;
        # 
        # void Init(TraversalProcessorT processor)
        # void Init(TraversalProcessorT processor, TraversalObserverT traversal_observer)
        # void Init(TraversalProcessorT processor,
        #       TraversalObserverT traversal_observer,
        #       EdgeBreakerObserverT edgebreaker_observer)
        # 
        # Called before any traversing starts.
        # void OnTraversalStart()
        # 
        # Called when all the traversing is done.
        # void OnTraversalEnd()
        # 
        # void TraverseFromCorner(CornerIndex corner_id)
        # 
        # const CornerTable *corner_table() const
        # const TraversalProcessorT &traversal_processor() const
        # const TraversalObserverT &traversal_observer() const


###


# mesh.h
# namespace draco
# Mesh class can be used to represent general triangular meshes. Internally,
# Mesh is just an extended PointCloud with extra connectivity data that defines
# what points are connected together in triangles.
# class Mesh : public PointCloud {
cdef extern from "draco/mesh/mesh.h" namespace "draco" nogil:
    cdef cppclass Mesh(point_cloud.PointCloud):
        Mesh()
        # public:
        # typedef std::array<PointIndex, 3> Face;
        # 
        # void AddFace(const Face &face)
        # void SetFace(FaceIndex face_id, const Face &face)
        # 
        # Sets the total number of faces. Creates new empty faces or deletes
        # existings ones if necessary.
        void SetNumFaces(size_t num_faces)
        
        # FaceIndex::ValueType num_faces() const
        int num_faces()
        
        # const Face &face(FaceIndex face_id) const
        # Face &face(FaceIndex face_id)
        
        # void SetAttribute(int att_id, std::unique_ptr<PointAttribute> pa) override
        # void SetAttribute(int att_id, unique_ptr[PointAttribute] pa) override
        
        # MeshAttributeElementType GetAttributeElementType(int att_id) const
        MeshAttributeElementType GetAttributeElementType(int att_id)
        
        # void SetAttributeElementType(int att_id, MeshAttributeElementType et)
        void SetAttributeElementType(int att_id, MeshAttributeElementType et)
        
        # struct AttributeData
        #   AttributeData() : element_type(MESH_CORNER_ATTRIBUTE) {}
        #   MeshAttributeElementType element_type;
        # };


###

# mesh.h
# namespace draco
# Functor for computing a hash from data stored within a mesh.
# Note that this can be quite slow. Two meshes will have the same hash only
# when they have exactly the same connectivity and attribute values.
# struct MeshHasher {
#   size_t operator()(const Mesh &mesh) const
# };


###

# mesh_are_equivalent.h
# namespace draco
# This file defines a functor to compare two meshes for equivalency up to permutation of the vertices.
# A functor to compare two meshes for equivalency up to permutation of the vertices.
# class MeshAreEquivalent
cdef extern from "draco/mesh/mesh_are_equivalent.h" namespace "draco" nogil:
    cdef cppclass MeshAreEquivalent:
        # public:
        # Returns true if both meshes are equivalent up to permutation of
        # the internal order of vertices. This includes all attributes.
        # bool operator()(const Mesh &mesh0, const Mesh &mesh1);
        bool eq "operator()"(const Mesh, const Mesh) nogil


###

# mesh_are_equivalent.h
# namespace draco
#   // Prepare functor for actual comparision.
#   void Init(const Mesh &mesh0, const Mesh &mesh1);


###

# mesh_are_equivalent.h
# namespace draco
#   // Get position as Vector3f of corner c of face f.
#   static Vector3f GetPostion(const Mesh &mesh, FaceIndex f, int32_t c);
#   // Internal helper function mostly for debugging.
#   void PrintPostion(const Mesh &mesh, FaceIndex f, int32_t c);
#   // Get the corner index of the lex smallest vertex of face f.
#   static int32_t ComputeCornerIndexOfSmallestPointXYZ(const Mesh &mesh, FaceIndex f);
# 
# mesh_are_equivalent.h
# namespace draco
#   // Less compare functor for two faces (represented by their indices)
#   // with respect to their lex order.
#   struct FaceIndexLess {
#     FaceIndexLess(const MeshInfo &in_mesh_info) : mesh_info(in_mesh_info) {}
#     bool operator()(FaceIndex f0, FaceIndex f1) const;
#     const MeshInfo &mesh_info;
#   };
# 
# mesh_are_equivalent.h
# namespace draco
#   void InitCornerIndexOfSmallestPointXYZ();
#   void InitOrderedFaceIndex();
#   std::vector<MeshInfo> mesh_infos_;
#   int32_t num_faces_;


###

# mesh_attribute_corner_table.h
# namespace draco
# Class for storing connectivity of mesh attributes. The connectivity is stored
# as a difference from the base mesh's corner table, where the differences are
# represnted by attribute seam edges. This class provides a basic funcionality
# for detecting the seam edges for a given attribute and for traversing the
# constrained corner table with the seam edges.
# class MeshAttributeCornerTable {
cdef extern from "draco/mesh/mesh_attribute_corner_table.h" namespace "draco" nogil:
    cdef cppclass MeshAttributeCornerTable:
        # public:
        MeshAttributeCornerTable()
        # bool InitEmpty(const CornerTable *table)
        # bool InitFromAttribute(const Mesh *mesh, const CornerTable *table, const PointAttribute *att)
        # 
        # void AddSeamEdge(CornerIndex opp_corner)
        # 
        # Recomputes vertices using the newly added seam edges (needs to be called whenever the seam edges are updated).
        # |mesh| and |att| can be null, in which case mapping between vertices and attribute value ids is set to identity.
        # void RecomputeVertices(const Mesh *mesh, const PointAttribute *att)
        # 
        # inline bool IsCornerOppositeToSeamEdge(CornerIndex corner) const
        # inline CornerIndex Opposite(CornerIndex corner) const
        # inline CornerIndex Next(CornerIndex corner) const
        # inline CornerIndex Previous(CornerIndex corner) const
        # Returns true when a corner is attached to any attribute seam.
        # inline bool IsCornerOnSeam(CornerIndex corner) const
        # Similar to CornerTable::GetLeftCorner and CornerTable::GetRightCorner, but
        # does not go over seam edges.
        # inline CornerIndex GetLeftCorner(CornerIndex corner) const
        # inline CornerIndex GetRightCorner(CornerIndex corner) const
        # 
        # Similar to CornerTable::SwingRight, but it does not go over seam edges.
        # inline CornerIndex SwingRight(CornerIndex corner) const
        # 
        # Similar to CornerTable::SwingLeft, but it does not go over seam edges.
        # inline CornerIndex SwingLeft(CornerIndex corner) const
        # 
        # int num_vertices() const
        # int num_faces() const
        # 
        # VertexIndex Vertex(CornerIndex corner) const
        # 
        # Returns the attribute entry id associated to the given vertex.
        # VertexIndex VertexParent(VertexIndex vert) const
        # 
        # inline CornerIndex LeftMostCorner(VertexIndex v) const
        # inline bool IsOnBoundary(VertexIndex vert) const
        # 
        # bool no_interior_seams() const
        # const CornerTable *corner_table() const


###

# mesh_cleanup.h
# namespace draco 
# Options used by the MeshCleanup class.
# struct MeshCleanupOptions {
# cdef extern from "draco/mesh/mesh_cleanup.h" namespace "draco" nogil:
#     cdef cppclass MeshCleanupOptions:
#         MeshCleanupOptions()
        # If true, the cleanup tool removes any face where two or more vertices
        # share the same position index.
        # bool remove_degenerated_faces;
        # If true, the cleanup tool removes any unusued attribute value or unused
        # point id. For example, it can be used to remove isolated vertices.
        # bool remove_unused_attributes;


###

# mesh_cleanup.h
# namespace draco 
# Tool that can be used for removing bad or unused data from draco::Meshes.
# class MeshCleanup {
#  public:
#   // Performs in-place cleanup of the input mesh according to the input options.
#   bool operator()(Mesh *mesh, const MeshCleanupOptions &options);
# };


###

# mesh_indices.h
# namespace draco
# 
# Face index used in a corner table.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, FaceIndex);
# 
# Constants denoting invalid indices.
# static constexpr FaceIndex kInvalidFaceIndex(std::numeric_limits<int32_t>::min() / 2);


###

# mesh_misc_functions.h
# namespace draco
# 
# Creates a CornerTable from |*mesh|. Returns nullptr on error.
# std::unique_ptr<CornerTable> CreateCornerTable(const Mesh *mesh);
# 
# Returns the point id stored on corner |ci|.
# PointIndex CornerToPointId(CornerIndex ci, const CornerTable *ct, const Mesh *mesh);
# 
# Returns the point id stored on corner |c|.
# PointIndex CornerToPointId(int c, const CornerTable *ct, const Mesh *mesh);
# 
# Returns the point id of |c| without using a corner table.
# inline PointIndex CornerToPointId(int c, const Mesh *mesh)


###

# prediction_degree_traverser.h
# namespace draco
# PredictionDegreeTraverser provides framework for traversal over a corner
# table data structure following paper "Multi-way Geometry Encoding" by
# Cohen-or at al.'02. The traversal is implicitly guided by prediction degree
# of the destination vertices. A prediction degree is computed as the number of
# possible faces that can be used as source points for traversal to the given
# destination vertex (see image below, where faces F1 and F2 are already
# traversed and face F0 is not traversed yet. The prediction degree of vertex
# V is then equal to two).
# 
#            X-----V-----o
#           / \   / \   / \
#          / F0\ /   \ / F2\
#         X-----o-----o-----B
#                \ F1/
#                 \ /
#                  A
# 
# The class implements the same interface as the EdgebreakerTraverser
# (edgebreaker_traverser.h) and it can be controlled via the same template
# trait classes |TraversalProcessorT| and |TraversalObserverT|, that are used
# for controlling and monitoring of the traversal respectively. For details,
# please see edgebreaker_traverser.h.
# template <class TraversalProcessorT, class TraversalObserverT>
# class PredictionDegreeTraverser {
cdef extern from "draco/mesh/prediction_degree_traverser.h" namespace "draco" nogil:
    cdef cppclass PredictionDegreeTraverser[TraversalProcessorT, TraversalObserverT]:
        # public:
        # typedef TraversalProcessorT TraversalProcessor;
        # typedef TraversalObserverT TraversalObserver;
        # typedef typename TraversalProcessorT::CornerTable CornerTable;
        # 
        PredictionDegreeTraverser()
        # void Init(TraversalProcessorT &&processor)
        # void Init(TraversalProcessorT &&processor,
        #           TraversalObserverT traversal_observer)
        # 
        # Called before any traversing starts.
        # void OnTraversalStart()
        # 
        # Called when all the traversing is done.
        # void OnTraversalEnd()
        # void TraverseFromCorner(CornerIndex corner_id)
        # 
        # const CornerTable *corner_table() const
        # const TraversalProcessorT &traversal_processor() const
        # const TraversalObserverT &traversal_observer() const


###

# triangle_soup_mesh_builder.h
# namespace draco
# Class for building meshes directly from attribute values that can be
# specified for each face corner. All attributes are automatically
# deduplicated.
# class TriangleSoupMeshBuilder
cdef extern from "draco/mesh/triangle_soup_mesh_builder.h" namespace "draco" nogil:
    cdef cppclass TriangleSoupMeshBuilder:
        # Starts mesh building for a given number of faces.
        # TODO(ostava): Currently it's necessary to select the correct number of
        # faces upfront. This should be generalized, but it will require us to
        # rewrite our attribute resizing functions.
        void Start(int num_faces)

        # Adds an empty attribute to the mesh. Returns the new attribute's id.
        int AddAttribute(attribute.Type2 attribute_type, int8_t num_components, core.DataType data_type)

        # Sets values for a given attribute on all corners of a given face.
        # void SetAttributeValuesForFace(int att_id, FaceIndex face_id,
        #                                   const void *corner_value_0,
        #                                   const void *corner_value_1,
        #                                   const void *corner_value_2);

        # Sets value for a per-face attribute. If all faces of a given attribute are
        # set with this method, the attribute will be marked as per-face, otherwise
        # it will be marked as per-corner attribute.
        # void SetPerFaceAttributeValueForFace(int att_id, FaceIndex face_id, const void *value);
        # void SetPerFaceAttributeValueForFace(int att_id, FaceIndex face_id, const void *value)

        # Finalizes the mesh or returns nullptr on error.
        # Once this function is called, the builder becomes invalid and cannot be
        # used until the method Start() is called again.
        # std::unique_ptr<Mesh> Finalize();


###

###############################################################################
# Enum
###############################################################################

# mesh.h
# namespace draco
# List of different variants of mesh attributes.
cdef extern from "draco/mesh/mesh.h" namespace "draco":
    cdef enum MeshAttributeElementType:
        # All corners attached to a vertex share the same attribute value. A typical
        # example are the vertex positions and often vertex colors.
        MESH_VERTEX_ATTRIBUTE = 0
        # The most general attribute where every corner of the mesh can have a
        # different attribute value. Often used for texture coordinates or normals.
        MESH_CORNER_ATTRIBUTE
        # All corners of a single face share the same value.
        MESH_FACE_ATTRIBUTE


###
