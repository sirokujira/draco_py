#include "attr_draco.h"

int mapped_index2(draco::PointAttribute* pointAttr, int index)
{
    draco::AttributeValueIndex attrIndex;
    draco::PointIndex pt_index(index);
    attrIndex = pointAttr->mapped_index(pt_index);
    return attrIndex.value();
}

bool ConvertValueFloatPointer(draco::GeometryAttribute* geoAttr, int a, float* b)
{
    // Test
    // float* test_pointer = new float[3];
    // printf("%d", a);
    const draco::AttributeValueIndex attrIndex(a);
    bool isFlag = false;
    isFlag = geoAttr->ConvertValue<float, 3>(attrIndex, b);
    // Test
    // isFlag = geoAttr->ConvertValue<float, 3>(attrIndex, test_pointer);
    // isFlag = pointAttr->ConvertValue<float, 3>(attrIndex, b);
    // if (!pos_att->ConvertValue<float, 3>(val_index, unity_mesh->position + i.value() * 3)) {

    // Test
    // printf("%f %f %f.\n", test_pointer[0], test_pointer[1], test_pointer[2]);
    // b = test_pointer;

    return isFlag;
}

