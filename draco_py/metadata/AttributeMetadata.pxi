# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
cimport draco_metadata_defs as metadata

from cython cimport address

from libcpp cimport bool
from libcpp.vector cimport vector

from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t

cimport _draco

cdef class AttributeMetadata:
    """
    AttributeMetadata 
    """
    def __cinit__(self, attribute_id=None, meta_data=None):
        """
        """
        if attribute_id is not None and meta_data is None:
            self.meta = new metadata.AttributeMetadata()
        elif attribute_id is not None and meta_data is not None:
            self.meta = new metadata.AttributeMetadata(<const metadata.Metadata &> meta_data)
        elif attribute_id is None and meta_data is None:
            pass
        else:
            raise TypeError("Can't initialize a AttributeMetadata")


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    # return uint32_t
    # def attribute_id(self):
    #     if self.meta <> NULL:
    #         return self.meta.attribute_id()
    #     elif self.cns_meta <> NULL:
    #         return self.cls_meta.attribute_id()


    # def attribute_id(self):
    def att_unique_id(self):
        if self.meta <> NULL:
            return self.meta.att_unique_id()
        elif self.cns_meta <> NULL:
            return self.cls_meta.att_unique_id()


    # base Metadata
    def AddEntryInt(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryInt(self._encode(name), <int32_t>value)


    # return bool
    def GetEntryInt(self, name, value):
        cdef int32_t* retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryInt(self._encode(name), retValue)
        return retResult


    def AddEntryIntArray(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryIntArray(self._encode(name), <const vector[int32_t]&> value)


#     # return bool
#     def GetEntryIntArray(self, name, value):
#         cdef vector[int32_t] *retValue
#         cdef bool retResult
#         retResult = return (<metadata.Metadata*> self.meta).GetEntryIntArray(self._encode(name), retValue)
#         return retResult


    def AddEntryDouble(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryDouble(self._encode(name), <double>value)


    # return bool
    def GetEntryDouble(self, name, value):
        cdef double *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryDouble(self._encode(name), retValue)
        return retResult


    def AddEntryDouble(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryDoubleArray(self._encode(name), <vector[double]&> value)


    # return bool
    def GetEntryDoubleArray(self, name, value):
        cdef vector[double] *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryDoubleArray(self._encode(name), retValue)
        return retResult


    def AddEntryString(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryString(self._encode(name), self._encode(value))


    # return bool
    def GetEntryString(self, name, value):
        cdef string *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryString(self._encode(name), retValue)
        return retResult


    def AddEntryBinary(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryBinary(self._encode(name), <const vector[uint8_t] &>value)


    # return bool
    def GetEntryBinary(self, name, value):
        cdef vector[uint8_t] *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryBinary(self._encode(name), retValue)
        return retResult


#     def AddSubMetadata(self, name, sub_metadata):
#         return (<metadata.Metadata*> self.meta).AddSubMetadata(self._encode(name), unique_ptr[Metadata] sub_metadata)


#     def GetSubMetadata(self, name):
#         # const Metadata* GetSubMetadata(const string &name)
#         (<metadata.Metadata*> self.meta).GetSubMetadata(self._encode(name))


    def RemoveEntry(self, name):
        (<metadata.Metadata*> self.meta).RemoveEntry(self._encode(name))


    def num_entries(self):
        return (<metadata.Metadata*> self.meta).num_entries()

###

