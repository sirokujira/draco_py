# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
cimport draco_metadata_defs as metadata

from cython cimport address
from cython.operator cimport dereference as deref, preincrement as inc

from libcpp cimport bool
from libcpp.vector cimport vector

from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t

cimport _draco

cdef class GeometryMetadata:
    """
    GeometryMetadata 
    """
    def __cinit__(self, meta_data=None):
        """
        """
        if meta_data is not None:
            self.meta = new metadata.GeometryMetadata(<const metadata.Metadata &> meta_data)
        elif meta_data is None:
            self.meta = new metadata.GeometryMetadata()
        else:
            raise TypeError("Can't initialize a GeometryMetadata")


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    # const AttributeMetadata*
    # def GetAttributeMetadataByStringEntry(self, string entry_name, string entry_value):
    def GetAttributeMetadataByStringEntry(self, entry_name, entry_value):
        # cdef const AttributeMetadata *attrMeta
        # attrMeta = self.meta.GetAttributeMetadataByStringEntry(self._encode(entry_name), self._encode(entry_value))
        # cdef const AttributeMetadata* attrMeta = self.meta.GetAttributeMetadataByStringEntry(self._encode(entry_name), self._encode(entry_value))
        attrMeta = AttributeMetadata()
        attrMeta.cns_meta = self.meta.GetAttributeMetadataByStringEntry(self._encode(entry_name), self._encode(entry_value))
        return attrMeta


    # return bool
    def AddAttributeMetadata(self, att_metadata):
        # return self.meta.AddAttributeMetadata(unique_ptr[AttributeMetadata] att_metadata)
        pass


    # const AttributeMetadata *
    def GetAttributeMetadata(self, attribute_id):
        # cdef const AttributeMetadata *attrMeta
        attrMeta = AttributeMetadata()
        attrMeta.cns_meta = self.meta.attribute_metadata(<int32_t> attribute_id)
        return attrMeta


    # return AttributeMetadata *
    def attribute_metadata(self, attribute_id):
        attrMeta = AttributeMetadata(attribute_id)
        attrMeta.meta = self.meta.attribute_metadata(<int32_t> attribute_id)
        return attrMeta


    # return const vector[unique_ptr[AttributeMetadata]] &
    def attribute_metadatas(self):
        self.meta.attribute_metadatas()


    # base Metadata class function
    def AddEntryInt(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryInt(self._encode(name), <int32_t>value)


    # return bool
    def GetEntryInt(self, name, value):
        cdef int32_t* retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryInt(self._encode(name), retValue)
        return retResult


    def AddEntryIntArray(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryIntArray(self._encode(name), <const vector[int32_t]&> value)


#     # return bool
#     def GetEntryIntArray(self, name, value):
#         cdef vector[int32_t] *retValue
#         cdef bool retResult
#         retResult = return (<metadata.Metadata*> self.meta).GetEntryIntArray(self._encode(name), retValue)
#         return retResult


    def AddEntryDouble(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryDouble(self._encode(name), <double>value)


    # return bool
    def GetEntryDouble(self, name, value):
        cdef double *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryDouble(self._encode(name), retValue)
        return retResult


    def AddEntryDouble(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryDoubleArray(self._encode(name), <vector[double]&> value)


    # return bool
    def GetEntryDoubleArray(self, name, value):
        cdef vector[double] *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryDoubleArray(self._encode(name), retValue)
        return retResult


    def AddEntryString(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryString(self._encode(name), self._encode(value))


    # return bool
    def GetEntryString(self, name, value):
        cdef string *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryString(self._encode(name), retValue)
        return retResult


    def AddEntryBinary(self, name, value):
        (<metadata.Metadata*> self.meta).AddEntryBinary(self._encode(name), <const vector[uint8_t] &>value)


    # return bool
    def GetEntryBinary(self, name, value):
        cdef vector[uint8_t] *retValue
        cdef bool retResult
        retResult = (<metadata.Metadata*> self.meta).GetEntryBinary(self._encode(name), retValue)
        return retResult


#     def AddSubMetadata(self, name, sub_metadata):
#         return (<metadata.Metadata*> self.meta).AddSubMetadata(self._encode(name), unique_ptr[Metadata] sub_metadata)


#     def GetSubMetadata(self, name):
#         # const Metadata* GetSubMetadata(const string &name)
#         (<metadata.Metadata*> self.meta).GetSubMetadata(self._encode(name))


    def RemoveEntry(self, name):
        (<metadata.Metadata*> self.meta).RemoveEntry(self._encode(name))


    def num_entries(self):
        return (<metadata.Metadata*> self.meta).num_entries()

###

