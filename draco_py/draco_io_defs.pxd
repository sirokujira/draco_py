# -*- coding: utf-8 -*-
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.memory cimport unique_ptr

# main
cimport draco_core_defs as core
cimport draco_compress_defs as compress
cimport draco_point_cloud_defs as point_cloud
cimport draco_mesh_defs as mesh


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t

###

# mesh_io.h
# namespace draco
# Reads a mesh from a file. The function automatically chooses the correct
# decoder based on the extension of the files. Currently, .obj and .ply files
# are supported. Other file extensions are processed by the default
# draco::MeshDecoder.
cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    OutStreamT WriteMeshIntoStream [OutStreamT](const mesh.Mesh *mesh, OutStreamT &&os, compress.MeshEncoderMethod method, const compress.EncoderOptions &options)

cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    OutStreamT WriteMeshIntoStream [OutStreamT](const mesh.Mesh *mesh, OutStreamT &&os, compress.MeshEncoderMethod method)

cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    OutStreamT &WriteMeshIntoStream [OutStreamT](const mesh.Mesh *mesh, OutStreamT &&os)

cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    InStreamT &ReadMeshFromStream [InStreamT](unique_ptr[mesh.Mesh] *mesh, InStreamT &&ins)

# Returns nullptr if the decoding failed.
cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    # unique_ptr[mesh.Mesh] ReadMeshFromFile(const string &file_name)
    core.StatusOr[ unique_ptr[mesh.Mesh] ] ReadMeshFromFile(const string &file_name)

cdef extern from "draco/io/mesh_io.h" namespace "draco" nogil:
    core.StatusOr[ unique_ptr[mesh.Mesh] ] ReadMeshFromFile_IsUseMetadata "ReadMeshFromFile" (const string &file_name, bool use_metadata)


###

# obj_decoder.h
# namespace draco
# Decodes a Wavefront OBJ file into draco::Mesh (or draco::PointCloud if the
# connectivity data is not needed).. This decoder can handle decoding of
# positions, texture coordinates, normals and triangular faces.
# All other geometry properties are ignored.
# class ObjDecoder {
cdef extern from "draco/io/obj_decoder.h" namespace "draco" nogil:
    cdef cppclass ObjDecoder:
        # public:
        ObjDecoder()

        # Decodes an obj file stored in the input file.
        # Returns nullptr if the decoding failed.
        # bool DecodeFromFile(const string &file_name, const mesh.Mesh *out_mesh)
        # bool DecodeFromFile_PointCloud "DecodeFromFile" (const string &file_name, const point_cloud.PointCloud *out_point_cloud)
        # bool DecodeFromBuffer(core.DecoderBuffer *buffer, mesh.Mesh *out_mesh)
        # bool DecodeFromBuffer_PointCloud "DecodeFromBuffer" (core.DecoderBuffer *buffer, point_cloud.PointCloud *out_point_cloud)
        core.Status DecodeFromFile(const string &file_name, const mesh.Mesh *out_mesh)
        core.Status DecodeFromFile_PointCloud "DecodeFromFile" (const string &file_name, const point_cloud.PointCloud *out_point_cloud)
        core.Status DecodeFromBuffer(core.DecoderBuffer *buffer, mesh.Mesh *out_mesh)
        core.Status DecodeFromBuffer_PointCloud "DecodeFromBuffer" (core.DecoderBuffer *buffer, const point_cloud.PointCloud *out_point_cloud)

        # Flag that can be used to turn on/off deduplication of input values.
        # This should be disabled only when we are sure that the input data doesn not
        # contain any duplicate entries.
        # Default: true
        # void set_deduplicate_input_values(bool v) { deduplicate_input_values_ = v; }

        # Flag for whether using metadata to record other information in the obj
        # file, e.g. material names, object names.
        void set_use_metadata(bool flag)


###

# obj_encoder.h
# namespace draco
# Class for encoding input draco::Mesh or draco::PointCloud into the Wavefront
# OBJ format.
# class ObjEncoder {
cdef extern from "draco/io/obj_encoder.h" namespace "draco" nogil:
    cdef cppclass ObjEncoder:
        # public:
        ObjEncoder()
        
        # Encodes the mesh or a point cloud  and saves it into a file.
        # Returns false when either the encoding failed or when the file couldn't be
        # opened.
        bool EncodeToFile(const mesh.Mesh &mesh, const string &file_name)
        # NG(cpp)
        # bool EncodeToFile(mesh.Mesh* mesh, const string &file_name)
        
        # bool EncodeToFile(const PointCloud &pc, const string &file_name)
        bool EncodeToFile_PointCloud "EncodeToFile" (const point_cloud.PointCloud &pc, const string &file_name)
        # NG(cpp)
        # bool EncodeToFile_PointCloud "EncodeToFile" (point_cloud.PointCloud* pc, const string &file_name)
        
        # Encodes the mesh or the point cloud into a buffer.
        bool EncodeToBuffer(const mesh.Mesh &mesh, core.EncoderBuffer *out_buffer)
        bool EncodeToBuffer_PointCloud "EncodeToBuffer" (const point_cloud.PointCloud &pc, core.EncoderBuffer *out_buffer)


###

# parser_utils.h
# namespace draco {
# namespace parser {
# Skips to first character not included in |skip_chars|.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# void SkipCharacters(DecoderBuffer *buffer, const char *skip_chars);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Skips any whitespace until a regular character is reached.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# void SkipWhitespace(DecoderBuffer *buffer);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Returns true if the next character is a whitespace.
# |end_reached| is set to true when the end of the stream is reached.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# bool PeekWhitespace(DecoderBuffer *buffer, bool *end_reached);

# parser_utils.h
# namespace draco {
# namespace parser {
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# void SkipLine(DecoderBuffer *buffer);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Parses signed floating point number or returns false on error.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# bool ParseFloat(DecoderBuffer *buffer, float *value);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Parses a signed integer (can be preceded by '-' or '+' characters.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# bool ParseSignedInt(DecoderBuffer *buffer, int32_t *value);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Parses an unsigned integer. It cannot be preceded by '-' or '+'
# characters.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# bool ParseUnsignedInt(DecoderBuffer *buffer, uint32_t *value);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Returns -1 if c == '-'.
# Returns +1 if c == '+'.
# Returns 0 otherwise.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# int GetSignValue(char c);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Parses a string until a whitespace or end of file is reached.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# bool ParseString(DecoderBuffer *buffer, std::string *out_string);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Parses the entire line into the buffer (excluding the new line character).
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# void ParseLine(DecoderBuffer *buffer, std::string *out_string);
# 
# parser_utils.h
# namespace draco {
# namespace parser {
# Returns a string with all characters converted to lower case.
# cdef extern from "draco/io/parser_utils.h" namespace "draco::parser":
# std::string ToLower(const std::string &str);
# 


###

# ply_decoder.h
# namespace draco
# Decodes a Wavefront OBJ file into draco::Mesh (or draco::PointCloud if the
# connectivity data is not needed).
# TODO(ostava): The current implementation assumes that the input vertices are
# defined with x, y, z properties. The decoder also reads uint8 red, green,
# blue, alpha color information, but all other attributes are ignored for now.
# class PlyDecoder {
cdef extern from "draco/io/ply_decoder.h" namespace "draco" nogil:
    cdef cppclass PlyDecoder:
        # public:
        PlyDecoder()
        
        # Decodes an obj file stored in the input file.
        # Returns nullptr if the decoding failed.
        bool DecodeFromFile(const string &file_name, const mesh.Mesh *out_mesh)
        bool DecodeFromFile_PointCloud "DecodeFromFile" (const string &file_name, const point_cloud.PointCloud *out_point_cloud)
        bool DecodeFromBuffer(core.DecoderBuffer *buffer, mesh.Mesh *out_mesh)
        bool DecodeFromBuffer_PointCloud "DecodeFromBuffer" (core.DecoderBuffer *buffer, const point_cloud.PointCloud *out_point_cloud)


###

# ply_encoder.h
# namespace draco
# Class for encoding draco::Mesh or draco::PointCloud into the PLY file format.
# class PlyEncoder {
cdef extern from "draco/io/ply_encoder.h" namespace "draco" nogil:
    cdef cppclass PlyEncoder:
        # public:
        PlyEncoder()
        
        # Encodes the mesh or a point cloud  and saves it into a file.
        # Returns false when either the encoding failed or when the file couldn't be
        # opened.
        bool EncodeToFile(const mesh.Mesh &mesh, const string &file_name)
        # bool EncodeToFile(const point_cloud.PointCloud &pc, const string &file_name)
        bool EncodeToFile_PointCloud "EncodeToFile" (const point_cloud.PointCloud &pc, const string &file_name)
        
        # Encodes the mesh or the point cloud into a buffer.
        # bool EncodeToBuffer(const Mesh &mesh, core.EncoderBuffer *out_buffer);
        bool EncodeToBuffer(const mesh.Mesh &mesh, core.EncoderBuffer &out_buffer)
        # bool EncodeToBuffer(const PointCloud &pc, core.EncoderBuffer *out_buffer);
        bool EncodeToBuffer_PointCloud "EncodeToBuffer" (const point_cloud.PointCloud &pc, core.EncoderBuffer &out_buffer)


###

# ply_property_reader.h
# namespace draco
# Class for reading PlyProperty with a given type, performing data conversion
# if necessary.
# template <typename ReadTypeT>
# class PlyPropertyReader {
# cdef extern from "draco/io/ply_property_reader.h" namespace "draco" nogil:
#     cdef cppclass PlyPropertyReader:
#         public:
#         PlyPropertyReader(const PlyProperty *property) : property_(property)
#         ReadTypeT ReadValue(int value_id) const


###

# ply_property_writer.h
# namespace draco
# Class for writing PlyProperty with a given type, performing data conversion
# if necessary.
# template <typename WriteTypeT>
# class PlyPropertyWriter {
# cdef extern from "draco/io/ply_property_writer.h" namespace "draco" nogil:
#     cdef cppclass PlyPropertyWriter:
#         # public:
#         PlyPropertyWriter(PlyProperty *property) : property_(property)
#         # void PushBackValue(WriteTypeT value) const


###

# ply_reader.h
# namespace draco
# A single PLY property of a given PLY element. For "vertex" element this can
# contain data such as "x", "y", or "z" coordinate of the vertex, while for
# "face" element this usually contains corner indices.
# class PlyProperty {
cdef extern from "draco/io/ply_reader.h" namespace "draco" nogil:
    cdef cppclass PlyProperty:
        # public:
        # friend class PlyReader;
        # PlyProperty(const std::string &name, DataType data_type, DataType list_type);
        PlyProperty(const string &name, core.DataType data_type, core.DataType list_type)
        void ReserveData(int num_entries)

        int64_t GetListEntryOffset(int entry_id)
        int64_t GetListEntryNumValues(int entry_id)
        const void *GetDataEntryAddress(int entry_id)
        void push_back_value(const void *data)

        const string &name()
        bool is_list()
        core.DataType data_type()
        int data_type_num_bytes()
        core.DataType list_data_type()
        int list_data_type_num_bytes()


###

# ply_reader.h
# namespace draco
# A single PLY element such as "vertex" or "face". Each element can store
# arbitrary properties such as vertex coordinates or face indices.
# class PlyElement {
cdef extern from "draco/io/ply_reader.h" namespace "draco" nogil:
    cdef cppclass PlyElement:
        # public:
        # PlyElement(const std::string &name, int64_t num_entries)
        PlyElement(const string &name, int64_t num_entries)

        void AddProperty(const PlyProperty &prop)
        const PlyProperty *GetPropertyByName(const string &name)
        int num_properties()
        int num_entries()
        # const PlyProperty &property(int prop_index)
        PlyProperty &property(int prop_index)


###

# ply_reader.h
# namespace draco
# Class responsible for parsing PLY data. It produces a list of PLY elements
# and their properties that can be used to construct a mesh or a point cloud.
# class PlyReader {
cdef extern from "draco/io/ply_reader.h" namespace "draco" nogil:
    cdef cppclass PlyReader:
        # public:
        PlyReader();
        bool Read(core.DecoderBuffer *buffer)

        # const PlyElement *GetElementByName(const std::string &name) const
        const PlyElement* GetElementByName(const string &name)

        int num_elements()
        # const PlyElement& element(int element_index)
        # PlyElement* element(int element_index)
        const PlyElement* element(int element_index)


###

# point_cloud_io.h
# namespace draco
# template <typename OutStreamT>
# OutStreamT WritePointCloudIntoStream(const PointCloud *pc, OutStreamT &&os, PointCloudEncodingMethod method, const EncoderOptions &options)
cdef extern from "draco/io/point_cloud_io.h" namespace "draco" nogil:
    void WritePointCloudIntoStream [OutStreamT](const point_cloud.PointCloud *pc, OutStreamT &&os, compress.PointCloudEncodingMethod method, const compress.EncoderOptions &options)


# io/point_cloud_io.h
# namespace draco
# template <typename OutStreamT>
# OutStreamT WritePointCloudIntoStream(const PointCloud *pc, OutStreamT &&os, PointCloudEncodingMethod method)
cdef extern from "draco/io/point_cloud_io.h" namespace "draco" nogil:
    OutStreamT WritePointCloudIntoStream [OutStreamT](const point_cloud.PointCloud *pc, OutStreamT &&os, compress.PointCloudEncodingMethod method)


# point_cloud_io.h
# namespace draco
# template <typename OutStreamT>
# OutStreamT &WritePointCloudIntoStream(const PointCloud *pc, OutStreamT &&os)
cdef extern from "draco/io/point_cloud_io.h" namespace "draco" nogil:
    OutStreamT WritePointCloudIntoStream [OutStreamT](const point_cloud.PointCloud *pc, OutStreamT &&os)


# point_cloud_io.h
# namespace draco
# template <typename InStreamT>
# InStreamT &ReadPointCloudFromStream(std::unique_ptr<PointCloud> *point_cloud, InStreamT &&is)
# cdef extern from "draco/io/point_cloud_io.h" namespace "draco" nogil:
#     InStreamT ReadPointCloudFromStream [InStreamT](unique_ptr[point_cloud.PointCloud] *point_cloud, InStreamT &&is)


# point_cloud_io.h
# namespace draco
# Reads a point cloud from a file. The function automatically chooses the
# correct decoder based on the extension of the files. Currently, .obj and .ply
# files are supported. Other file extensions are processed by the default
# draco::PointCloudDecoder.
# Returns nullptr if the decoding failed.
# std::unique_ptr<PointCloud> ReadPointCloudFromFile(const std::string &file_name);
cdef extern from "draco/io/point_cloud_io.h" namespace "draco" nogil:
    # unique_ptr[point_cloud.PointCloud] ReadPointCloudFromFile(const string &file_name)
    core.StatusOr[ unique_ptr[point_cloud.PointCloud] ] ReadPointCloudFromFile(const string &file_name)


###

