# -*- coding: utf-8 -*-
from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.memory cimport unique_ptr

# main
cimport draco_attribute_defs as attribute
cimport draco_core_defs as core
cimport draco_mesh_defs as mesh
cimport draco_metadata_defs as metadata


from libc.stddef cimport size_t
from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t

###############################################################################
# Types
###############################################################################


# point_cloud/geometry_indices.h
# namespace draco
# 
# # Index of an attribute value entry stored in a GeometryAttribute.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, AttributeValueIndex)
# 
# # Index of a point in a PointCloud.
# DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, PointIndex)
# 
# # Constants denoting invalid indices.
# static constexpr AttributeValueIndex kInvalidAttributeValueIndex(std::numeric_limits<int32_t>::min() / 2);
# static constexpr PointIndex kInvalidPointIndex(std::numeric_limits<int32_t>::min() / 2);
# 
# }  # namespace draco
###


# point_cloud/point_cloud.h
# Copyright 2016 The Draco Authors.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#      http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# namespace draco
# PointCloud is a collection of n-dimensional points that are described by a
# set of PointAttributes that can represent data such as positions or colors
# of individual points (see point_attribute.h).
# class PointCloud {
cdef extern from "draco/point_cloud/point_cloud.h" namespace "draco" nogil:
    cdef cppclass PointCloud:
        # public:
        PointCloud();
        # virtual ~PointCloud() = default;

        # Returns the number of named attributes of a given type.
        # int32_t NumNamedAttributes(GeometryAttribute::Type type) const
        int32_t NumNamedAttributes(attribute.Type2 type)

        # Returns attribute id of the first named attribute with a given type or -1
        # when the attribute is not used by the point cloud.
        # int32_t GetNamedAttributeId(GeometryAttribute::Type type) const;
        int32_t GetNamedAttributeId(attribute.Type2 type)

        # Returns the id of the i-th named attribute of a given type.
        # int32_t GetNamedAttributeId(GeometryAttribute::Type type, int i) const;
        # int GetNamedAttributeId(attribute.Type2 type, int i)
        int32_t GetNamedAttributeId_Index "GetNamedAttributeId"(attribute.Type2 type, int i)

        # Returns the first named attribute of a given type or nullptr if the
        # attribute is not used by the point cloud.
        # const PointAttribute *GetNamedAttribute(GeometryAttribute::Type type) const;
        const attribute.PointAttribute* GetNamedAttribute(attribute.Type2 type)

        # Returns the i-th named attribute of a given type.
        # const PointAttribute *GetNamedAttribute(GeometryAttribute::Type type, int i) const;
        const attribute.PointAttribute* GetNamedAttribute(attribute.Type2 type, int i)

        # Returns the named attribute of a given custom id.
        # const PointAttribute *GetNamedAttributeByCustomId(GeometryAttribute::Type type, uint16_t id) const
        const attribute.PointAttribute* GetNamedAttributeByCustomId(attribute.Type2 type, short id)

        int num_attributes()

        # const PointAttribute *attribute(int32_t att_id) const
        # const attribute.PointAttribute *attribute(int32_t att_id)

        # Returned attribute can be modified, but it's caller's responsibility to
        # maintain the attribute's consistency with draco::PointCloud.
        attribute.PointAttribute* attribute(int32_t att_id)

        # Adds a new attribute to the point cloud.
        # Returns the attribute id.
        # int AddAttribute(std::unique_ptr<PointAttribute> pa)
        # int AddAttribute(unique_ptr[attribute.PointAttribute] pointattr)
        # int AddAttribute(attribute.PointAttributeUnique_Ptr_t pa)

        # Creates and adds a new attribute to the point cloud. The attribute has
        # properties derived from the provided GeometryAttribute |att|.
        # If |identity_mapping| is set to true, the attribute will use identity
        # mapping between point indices and attribute value indices (i.e., each point has a unique attribute value).
        # If |identity_mapping| is false, the mapping between point indices and
        # attribute value indices is set to explicit, and it needs to be initialized
        # manually using the PointAttribute::SetPointMapEntry() method.
        # |num_attribute_values| can be used to specify the number of attribute
        # values that are going to be stored in the newly created attribute.
        # Returns attribute id of the newly created attribute.
        # int AddAttribute(const GeometryAttribute &att, bool identity_mapping, AttributeValueIndex::ValueType num_attribute_values);
        # int AddGeometoryAttribute "AddAttribute" (attribute.GeometryAttribute att, bool identity_mapping, AttributeValueIndex::ValueType num_attribute_values)

        # Assigns an attribute id to a given PointAttribute. If an attribute with the
        # same attribute id already exists, it is deleted.
        # virtual void SetAttribute(int att_id, std::unique_ptr<PointAttribute> pa);

        # Deduplicates all attribute values (all attribute entries with the same
        # value are merged into a single entry).
        # virtual bool DeduplicateAttributeValues();

        # Removes duplicate point ids (two point ids are duplicate when all of their
        # attributes are mapped to the same entry ids).
        # virtual void DeduplicatePointIds();

        # Add metadata.
        void AddMetadata(unique_ptr[metadata.GeometryMetadata] metadata)

        # Add metadata for an attribute.
        void AddAttributeMetadata(unique_ptr[metadata.AttributeMetadata] metadata)

        # Returns the attribute metadata that has the requested metadata entry.
        const metadata.AttributeMetadata *GetAttributeMetadataByStringEntry(const string &name, const string &value)

        # Returns the first attribute that has the requested metadata entry.
        int GetAttributeIdByMetadataEntry(const string &name, const string &value)

        # Get a const pointer of the metadata of the point cloud.
        # const metadata.GeometryMetadata* GetMetadata()

        # Get a pointer to the metadata of the point cloud.
        metadata.GeometryMetadata* metadata()

        # Returns the number of n-dimensional points stored within the point cloud.
        size_t num_points()

        # Sets the number of points. It's the caller's responsibility to ensure the
        # new number is valid with respect to the PointAttributes stored in the point cloud.
        # void set_num_points(PointIndex::ValueType num)


###

# Functor for computing a hash from data stored within a point cloud.
# Note that this can be quite slow. Two point clouds will have the same hash
# only when all points have the same order and when all attribute values are exactly the same.
# point_cloud/point_cloud.h
# namespace draco {
# struct PointCloudHasher {
#   size_t operator()(const PointCloud &pc) const


###

# point_cloud/point_cloud_builder.h
# namespace draco {
# A helper class for constructing PointCloud instances from other data sources.
# Usage:
#   PointCloudBuilder builder;
#   # Initialize the builder for a given number of points (required).
#   builder.Start(num_points);
#   # Specify desired attributes.
#   int pos_att_id =
#       builder.AddAttribute(GeometryAttribute::POSITION, 3, DT_FLOAT32);
#   # Add attribute values.
#   for (PointIndex i(0); i < num_points; ++i) {
#     builder.SetAttributeValueForPoint(pos_att_id, i, input_pos[i.value()]);
#   }
#   # Get the final PointCloud.
#   constexpr bool deduplicate_points = false;
#   std::unique_ptr<PointCloud> pc = builder.Finalize(deduplicate_points);
# class PointCloudBuilder {
# public:
# PointCloudBuilder();
# 
# Starts collecting point cloud data.
# The bahavior of other functions is undefined before this method is called.
# void Start(PointIndex::ValueType num_points);
# 
# int AddAttribute(GeometryAttribute::Type attribute_type,
#                  int8_t num_components, DataType data_type);
# 
# Sets attribute value for a specific point.
# |attribute_value| must contain data in the format specified by the
# AddAttribute method.
# void SetAttributeValueForPoint(int att_id, PointIndex point_index,
#                                const void *attribute_value);
# 
# Sets attribute values for all points. All the values must be stored in the
# input |attribute_values| buffer. |stride| can be used to define the byte
# offset between two consecutive attribute values. If |stride| is set to 0,
# the stride is automatically computed based on the format of the given
# attribute.
# void SetAttributeValuesForAllPoints(int att_id, const void *attribute_values, int stride);
# 
# Finalizes the PointCloud or returns nullptr on error.
# If |deduplicate_points| is set to true, the following happens:
#   1. Attribute values with duplicate entries are deduplicated.
#   2. Point ids that are mapped to the same attribute values are
#      deduplicated.
# Therefore, if |deduplicate_points| is true the final PointCloud can have
# a different number of point from the value specified in the Start method.
# Once this function is called, the builder becomes invalid and cannot be
# used until the method Start() is called again.
# std::unique_ptr<PointCloud> Finalize(bool deduplicate_points);


###

###############################################################################
# Enum
###############################################################################


###############################################################################
# Activation
###############################################################################

