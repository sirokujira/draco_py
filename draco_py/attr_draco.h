#ifndef _ATTRDRACO_H_
#define _ATTRDRACO_H_

#include <attributes/geometry_indices.h>
#include <attributes/point_attribute.h>

// draco::AttributeValueIndex mapped_index2(draco::PointIndex index);
int mapped_index2(draco::PointAttribute* pointAttr, int index);
bool ConvertValueFloatPointer(draco::GeometryAttribute* geoAttr, int a, float* b);

#endif // _ATTRDRACO_H_

