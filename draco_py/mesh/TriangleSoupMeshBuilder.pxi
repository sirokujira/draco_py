# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.memory cimport unique_ptr

from move cimport move

cdef class TriangleSoupMeshBuilder:
    """
    Mesh class
    """
    def __cinit__(self, init=None):
        self.mesh = new mesh.TriangleSoupMeshBuilder()


    def Start(self, int index):
        self.mesh.Start(index)


    # def AddAttribute(self, attribute_type, int8_t num_components, core.DataType data_type):
    def AddAttribute(self, attribute_type, int num_components, data_type):
        self.mesh.AddAttribute(<attribute.Type2>(attribute_type), num_components, <core.DataType>(data_type))


#     # def SetAttributeValuesForFace(self, int att_id, FaceIndex face_id, corner_value_0, corner_value_1, corner_value_2):
#     def SetAttributeValuesForFace(self, int att_id, face_id, corner_value_0, corner_value_1, corner_value_2):
#         self.mesh.SetAttributeValuesForFace(att_id, <mesh.FaceIndex>(face_id), <const void *>(corner_value_0), <const void *>(corner_value_1), <const void *>(corner_value_2))
#         pass

