# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
cimport draco_metadata_defs as metadata

from libcpp cimport bool
from libcpp.memory cimport unique_ptr

from move cimport move

cimport _draco

cdef class Mesh:
    """
    Mesh class
    """
    def __cinit__(self, init=None):
        if init is None:
            self.mesh = new mesh.Mesh()
        elif isinstance(init, _draco.DecoderBuffer):
            self.decode = new compress.Decoder()
            self.Decode(init)
        else:
            raise TypeError("Can't initialize a Mesh from a %s" % type(init))


    # custom methods
    def Decode(self, _draco.DecoderBuffer buffer):
        """
        """
        cdef unique_ptr[mesh.Mesh] unique_mesh
        # unique_mesh = compress.DecodeMeshFromBuffer(buffer.module)
        # unique_mesh = self.decode.DecodeMeshFromBuffer(buffer.module)
        # StatusOr
        unique_mesh = (self.decode.DecodeMeshFromBuffer(buffer.module)).value()
        
        # NG
        # self.mesh = <mesh.Mesh*>unique_mesh.get()
        # pointer manage move
        self.mesh = <mesh.Mesh*>unique_mesh.release()
        # std::move(in_mesh)
        # https://groups.google.com/forum/#!topic/cython-users/-U8r0Lc_fU4
        # https://stackoverflow.com/questions/29571780/cython-avoid-copy-through-stdmove-not-working
        # self.points = <point_cloud.PointCloud*>(unique_mesh.release()
        # self.points = move(self.mesh)
        self.points = move(self.mesh)


    def GetPointCloud(self):
    # def GetPointCloud(self, _draco.PointCloud p):
        """
        """
        # p = _draco.PointCloud()
        p = PointCloud()
        p.points = self.points
        return p


    # Mesh methods
    def num_faces(self):
        """
        """
        return self.mesh.num_faces ()


    # base class(PointCloud) methods
    def num_attributes(self):
        """
        """
        return (<point_cloud.PointCloud*>self.mesh).num_attributes ()


    def attribute(self, int index):
    # def attribute(self, _draco.PointAttribute attribute, int index):
        """
        """
        # attribute = _draco.PointAttribute()
        pt_attr = PointAttribute()
        print(pt_attr)
        pt_attr.attr = (<point_cloud.PointCloud*> self.mesh).attribute(index)
        return pt_attr


    def NumNamedAttributes(self, int type):
        """
        """
        (<point_cloud.PointCloud*>self.mesh).NumNamedAttributes (<attribute.Type2> type)


    def GetNamedAttributeId(self, int type):
        """
        """
        id = (<point_cloud.PointCloud*>self.mesh).GetNamedAttributeId (<attribute.Type2> type)
        return <int>id


    # def GetNamedAttributeId(self, int type, int i):
    #     (<point_cloud.PointCloud*>self.mesh).GetNamedAttributeId (<attribute.Type2> type, i)


    def GetNamedAttribute(self, int type):
        pt_attr = PointAttribute()
        pt_attr.attr = <attribute.PointAttribute*>(<point_cloud.PointCloud*> self.mesh).GetNamedAttribute (<attribute.Type2> type)
        return pt_attr


    def GetMetadata(self):
        meta = GeometryMetadata()
        # cdef metadata.GeometryMetadata* geo_metadata
        # meta.meta = (<point_cloud.PointCloud*> self.mesh).GetMetadata ()
        meta.meta = (<point_cloud.PointCloud*> self.mesh).metadata ()
        return meta


    def num_points(self):
        """
        """
        return (<point_cloud.PointCloud*>self.mesh).num_points ()


###

