# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.memory cimport unique_ptr

from move cimport move
cimport _draco

cdef class MeshAreEquivalent:
    """
    Mesh class
    """
    def __cinit__(self, init=None):
        if init is None:
            self.mesh = new mesh.MeshAreEquivalent()
        else:
            raise TypeError("Can't initialize a MeshAreEquivalent from a %s" % type(init))


    def __call__(self, _draco.Mesh mesh1, _draco.Mesh mesh2):
        # return self.mesh.eq(mesh1.mesh, mesh2.mesh)
        return self.mesh.eq(deref(mesh1.mesh), deref(mesh2.mesh))


