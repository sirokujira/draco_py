# -*- coding: utf-8 -*-
import sys
import os.path

import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cimport _draco

cdef class PlyProperty:
    """
    Class responsible for parsing PLY data. It produces a list of PLY elements
    and their properties that can be used to construct a mesh or a point cloud.
    """
    # def __cinit__(self, const string &name, core.DataType data_type, core.DataType list_type):
    def __cinit__(self, name=None, data_type=None, list_type=None):
        if name is not None and data_type is not None and list_type is not None:
            self.property = new io.PlyProperty(self._encode(name), data_type, list_type)
        elif name is None and data_type is None and list_type is None:
            pass
        else:
            raise TypeError("Can't initialize a PlyProperty")


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    def ReserveData(self, int num_entries):
        self.property.ReserveData(num_entries)


    # return int64_t
    def GetListEntryOffset(self, int entry_id):
        return self.property.GetListEntryOffset(entry_id)


    # return int64_t
    def GetListEntryNumValues(self, int entry_id):
        return self.property.GetListEntryNumValues(entry_id)


    # return const void *
    def GetDataEntryAddress(self, int entry_id):
        self.property.GetDataEntryAddress(entry_id)


    # def push_back_value(self, const void *data):
    #     self.property.push_back_value(data)


    # const string &
    def name(self):
        return self.property.name()


    # bool
    def is_list(self):
        return self.property.is_list()


    # core.DataType 
    def data_type(self):
        return self.property.data_type()


    # return int
    def data_type_num_bytes(self):
        return self.property.data_type_num_bytes()


    # core.DataType
    def list_data_type(self):
        return self.property.list_data_type()


    # return int
    def list_data_type_num_bytes(self):
        return self.property.list_data_type_num_bytes()


    def _encode(path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


###

