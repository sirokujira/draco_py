# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cimport _draco

cdef class PlyEncoder:
    """
    Represents a cloud of points in 3-d space.
    A point cloud can be initialized from either a NumPy ndarray of shape
    (n_points, 3), from a list of triples, or from an integer n to create an
    "empty" cloud of n points.
    To load a point cloud from disk, use pcl.load.
    """
    def __cinit__(self, init=None):
        self.encoder = new io.PlyEncoder()

    # return bool
    def EncodeToFile_Mesh(self,  _draco.Mesh mesh, string filename):
        # result = self.encoder.EncodeToFile(mesh.mesh, filename)
        result = self.encoder.EncodeToFile(deref(mesh.mesh), filename)
        return result

    # return bool
    def EncodeToFile_PointCloud(self, _draco.PointCloud pointcloud, string filename):
        # result = self.encoder.EncodeToFile_PointCloud(pointcloud.points, filename)
        result = self.encoder.EncodeToFile_PointCloud(deref(pointcloud.points), filename)
        return result


###

