# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cimport _draco

cdef class PlyDecoder:
    """
    Represents a cloud of points in 3-d space.
    A point cloud can be initialized from either a NumPy ndarray of shape
    (n_points, 3), from a list of triples, or from an integer n to create an
    "empty" cloud of n points.
    To load a point cloud from disk, use pcl.load.
    """
    def __cinit__(self, init=None):
        self.decoder = new io.PlyDecoder()

    # return bool
    def DecodeFromFile_Mesh(self, string filename, _draco.Mesh mesh):
        result = self.decoder.DecodeFromFile(filename, mesh.mesh)
        # result = self.decoder.DecodeFromFile(filename, deref(mesh.mesh))
        return result

    # return bool
    def DecodeFromFile_PointCloud(self, string filename, _draco.PointCloud pointcloud):
        result = self.decoder.DecodeFromFile_PointCloud(filename, pointcloud.points)
        # result = self.decoder.DecodeFromFile_PointCloud(filename, deref(pointcloud.points))
        return result


###

