# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cdef class MeshIO:
    """
    """

    def __cinit__(self, init=None):
        if init is None:
            self.points = new point_cloud.PointCloud()
        elif isinstance(init, DecoderBuffer):
            self.Decode(init)
        else:
            raise TypeError("Can't initialize a MeshIO from a %s" % type(init))

    # custom?
    def Decode(self, DecoderBuffer buffer):
        cdef unique_ptr[point_cloud.PointCloud] unique_point
        unique_point = compress.DecodePointCloudFromBuffer(buffer.module)
        # NG
        # self.points = <mesh.Mesh*>unique_point.get()
        # pointer manage move
        self.points = unique_point.release()

    # return bool
    def ReadMeshFromFile(self, string filename):
        cdef mesh.Mesh ret_mesh
        result = io.ReadMeshFromFile(filename)
        ret_mesh = <mesh.Mesh*>result.release()
        return ret_mesh

###

