# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cimport _draco

cdef class PlyReader:
    """
    Class responsible for parsing PLY data. It produces a list of PLY elements
    and their properties that can be used to construct a mesh or a point cloud.
    """
    def __cinit__(self, init=None):
        self.reader = new io.PlyReader()


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    def Read(self, DecoderBuffer buffer):
        return self.reader.Read(buffer.module)


    def GetElementByName(self, name):
        plyElement = PlyElement()
        # plyElement.element = self.reader.GetElementByName(self._encode(name))
        plyElement.cns_element = self.reader.GetElementByName(self._encode(name))
        return plyElement


    def num_elements(self):
        return self.reader.num_elements()


    def element(self, int element_index):
        plyElement = PlyElement()
        # plyElement.element = <io.PlyElement*> self.reader.element(element_index)
        # plyElement.cns_element = <const io.PlyElement*> self.reader.element(element_index)
        # cdef const io.PlyElement* emt
        # emt = self.reader.element(element_index)
        return plyElement


###

