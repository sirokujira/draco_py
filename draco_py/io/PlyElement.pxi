# -*- coding: utf-8 -*-
import sys
import os.path

import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
from libcpp.string cimport string

# cimport indexing as idx

# from boost_shared_ptr cimport sp_assign

cimport _draco

cdef class PlyElement:
    """
    Class responsible for parsing PLY data. It produces a list of PLY elements
    and their properties that can be used to construct a mesh or a point cloud.
    """
    def __cinit__(self, name=None, num_entries=None):
        if name is not None and num_entries is not None:
            # public:
            # PlyElement(const std::string &name, int64_t num_entries)
            self.element = new io.PlyElement(self._encode(name), num_entries)
        elif name is None and num_entries is None:
            pass
        else:
            raise TypeError("Can't initialize a PlyElement")


    def _encode(self, path):
        # Encode path for use in C++.
        if isinstance(path, bytes):
            return path
        else:
            return path.encode(sys.getfilesystemencoding())


    def AddProperty(self, _draco.PlyProperty prop):
        # self.element.AddProperty(prop.property)
        self.element.AddProperty(deref(prop.property))


    # return const PlyProperty*
    def GetPropertyByName(self, name):
        # if (self.element is None):
        #     pass
        property = PlyProperty()
        property.cns_property = self.element.GetPropertyByName(self._encode(name))
        return property


    def num_properties(self):
        self.element.num_properties()


    def num_entries(self):
        return self.element.num_entries()


    # const PlyProperty &
    # PlyProperty &
    def property(self, int prop_index):
        self.element.property(prop_index)


###

