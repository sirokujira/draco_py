#ifndef _MINIDRACO_H_
#define _MINIDRACO_H_

#include <stdio.h>
#include <string>
#include <compression/encode.h>
#include <compression/decode.h>

// void draco_set_default_configure(draco::EncoderOptions* options);
draco::EncoderOptions* draco_get_default_encode_configure();
draco::EncoderOptions* draco_get_empty_encode_configure();
// draco::DecoderOptions* draco_get_default_decode_configure();

#endif // _MINIDRACO_H_

