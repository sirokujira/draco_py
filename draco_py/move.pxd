# point_cloud
cimport draco_point_cloud_defs as point_cloud

cdef extern from "<utility>" namespace "std":
    point_cloud.PointCloud* move(point_cloud.PointCloud*) # Cython has no function templates
