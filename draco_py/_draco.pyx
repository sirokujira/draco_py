# -*- coding: utf-8 -*-
from collections import Sequence
import numbers
import numpy as np
cimport numpy as cnp

# Attribute
cimport draco_attribute_defs as attribute
# Compress
cimport draco_compress_defs as compress
# Core
cimport draco_core_defs as core
# io
cimport draco_io_defs as io
# mesh
cimport draco_mesh_defs as mesh
# point_cloud
cimport draco_point_cloud_defs as point_cloud


cimport cython
from cython.operator cimport dereference as deref, preincrement as inc
from cython cimport address

from cpython cimport Py_buffer

from libcpp.string cimport string
from libcpp cimport bool
from libcpp.vector cimport vector

from libcpp.memory cimport unique_ptr
from move cimport move

cnp.import_array()

# cimport _draco

### Enum ###

## Enum Setting
## compress
# EncodedGeometryType
EncodedGeometryType_INVALID_GEOMETRY_TYPE = compress.INVALID_GEOMETRY_TYPE
EncodedGeometryType_POINT_CLOUD = compress.POINT_CLOUD
EncodedGeometryType_TRIANGULAR_MESH = compress.TRIANGULAR_MESH

# PointCloudEncodingMethod
PointCloudEncodingMethod_POINT_CLOUD_SEQUENTIAL_ENCODING = compress.POINT_CLOUD_SEQUENTIAL_ENCODING
PointCloudEncodingMethod_POINT_CLOUD_KD_TREE_ENCODING = compress.POINT_CLOUD_KD_TREE_ENCODING

# MeshEncoderMethod
MeshEncoderMethod_MESH_SEQUENTIAL_ENCODING = compress.MESH_SEQUENTIAL_ENCODING
MeshEncoderMethod_MESH_EDGEBREAKER_ENCODING = compress.MESH_EDGEBREAKER_ENCODING

# AttributeEncoderType
AttributeEncoderType_BASIC_ATTRIBUTE_ENCODER = compress.BASIC_ATTRIBUTE_ENCODER
AttributeEncoderType_MESH_TRAVERSAL_ATTRIBUTE_ENCODER = compress.MESH_TRAVERSAL_ATTRIBUTE_ENCODER
AttributeEncoderType_KD_TREE_ATTRIBUTE_ENCODER = compress.KD_TREE_ATTRIBUTE_ENCODER

# SequentialAttributeEncoderType
SequentialAttributeEncoderType_SEQUENTIAL_ATTRIBUTE_ENCODER_GENERIC = compress.SEQUENTIAL_ATTRIBUTE_ENCODER_GENERIC
SequentialAttributeEncoderType_SEQUENTIAL_ATTRIBUTE_ENCODER_INTEGER = compress.SEQUENTIAL_ATTRIBUTE_ENCODER_INTEGER
SequentialAttributeEncoderType_SEQUENTIAL_ATTRIBUTE_ENCODER_QUANTIZATION = compress.SEQUENTIAL_ATTRIBUTE_ENCODER_QUANTIZATION
SequentialAttributeEncoderType_SEQUENTIAL_ATTRIBUTE_ENCODER_NORMALS = compress.SEQUENTIAL_ATTRIBUTE_ENCODER_NORMALS

# PredictionSchemeMethod
PredictionSchemeMethod_PREDICTION_NONE                                  = compress.PREDICTION_NONE
PredictionSchemeMethod_PREDICTION_UNDEFINED                             = compress.PREDICTION_UNDEFINED
PredictionSchemeMethod_PREDICTION_DIFFERENCE                            = compress.PREDICTION_DIFFERENCE
PredictionSchemeMethod_MESH_PREDICTION_PARALLELOGRAM                    = compress.MESH_PREDICTION_PARALLELOGRAM
PredictionSchemeMethod_MESH_PREDICTION_MULTI_PARALLELOGRAM              = compress.MESH_PREDICTION_MULTI_PARALLELOGRAM
PredictionSchemeMethod_MESH_PREDICTION_TEX_COORDS                       = compress.MESH_PREDICTION_TEX_COORDS_DEPRECATED
# PredictionSchemeMethod_MESH_PREDICTION_TEX_COORDS_DEPRECATED            = compress.MESH_PREDICTION_TEX_COORDS_DEPRECATED
PredictionSchemeMethod_MESH_PREDICTION_CONSTRAINED_MULTI_PARALLELOGRAM  = compress.MESH_PREDICTION_CONSTRAINED_MULTI_PARALLELOGRAM
PredictionSchemeMethod_NUM_PREDICTION_SCHEMES                           = compress.NUM_PREDICTION_SCHEMES


# PredictionSchemeTransformType
PredictionSchemeTransformType_PREDICTION_TRANSFORM_NONE                            = compress.PREDICTION_TRANSFORM_NONE
PredictionSchemeTransformType_PREDICTION_TRANSFORM_DELTA                           = compress.PREDICTION_TRANSFORM_DELTA
PredictionSchemeTransformType_PREDICTION_TRANSFORM_WRAP                            = compress.PREDICTION_TRANSFORM_WRAP
PredictionSchemeTransformType_PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON               = compress.PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON
PredictionSchemeTransformType_PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON_CANONICALIZED = compress.PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON_CANONICALIZED


## core
# DataType
DataType_DT_INVALID = core.DT_INVALID
DataType_DT_INT8 = core.DT_INT8
DataType_DT_UINT8 = core.DT_UINT8
DataType_DT_INT16 = core.DT_INT16
DataType_DT_UINT16 = core.DT_UINT16
DataType_DT_INT32 = core.DT_INT32
DataType_DT_UINT32 = core.DT_UINT32
DataType_DT_INT64 = core.DT_INT64
DataType_DT_UINT64 = core.DT_UINT64
DataType_DT_FLOAT32 = core.DT_FLOAT32
DataType_DT_FLOAT64 = core.DT_FLOAT64
DataType_DT_BOOL = core.DT_BOOL
DataType_DT_TYPES_COUNT = core.DT_TYPES_COUNT


## attribute
# GeometryAttributeType
GeometryAttributeType_INVALID                = attribute.Type_INVALID
GeometryAttributeType_POSITION               = attribute.Type_POSITION
GeometryAttributeType_NORMAL                 = attribute.Type_NORMAL
GeometryAttributeType_COLOR                  = attribute.Type_COLOR
GeometryAttributeType_TEX_COORD              = attribute.Type_TEX_COORD
GeometryAttributeType_NAMED_ATTRIBUTES_COUNT = attribute.Type_NAMED_ATTRIBUTES_COUNT
GeometryAttributeType_GENERIC                = attribute.Type_GENERIC

### Enum Setting(define Class InternalType) ###

include "pxiInclude.pxi"

### CompressEncoded.pxi
def GetEncodedGeometryType(DecoderBuffer buffer):
    # geom_type = compress.GetEncodedGeometryType(buffer.module)
    # StatusOr
    geom_type = (compress.GetEncodedGeometryType(buffer.module)).value()
    return geom_type


def DecodeMeshFromBuffer(DecoderBuffer buffer):
    mesh = Mesh(buffer)
    return mesh


def DecodePointCloudFromBuffer(DecoderBuffer buffer):
    pointcloud = PointCloud(buffer)
    return pointcloud


###

def GetDecodedData(path):
    """
    get Point_Cloud Object
    """
    infile = open(path, 'rb')
    data = infile.read()
    data_len = len(data)
    
    buffer = DecoderBuffer()
    buffer.Init(data)
    
    # def GetEncodedGeometryType(DecoderBuffer buffer):
    # geom_type = compress.GetEncodedGeometryType(buffer.module)
    # return geom_type
    cdef compress.Decoder* decoder
    decoder = new compress.Decoder()
    # geom_type = compress.GetEncodedGeometryType(buffer.module)
    geom_type = (compress.GetEncodedGeometryType(buffer.module)).value()
    
    mesh = None
    
    # geom_type = GetEncodedGeometryType(buffer)
    if geom_type == EncodedGeometryType_TRIANGULAR_MESH:
        # mesh = DecodeMeshFromBuffer(buffer)
        mesh = Mesh(buffer)
    elif geom_type == EncodedGeometryType_POINT_CLOUD:
        # mesh = DecodePointCloudFromBuffer(buffer)
        mesh = PointCloud(buffer)
    else:
        pass

    return mesh


# def SetEncodedData(path):
#     infile = open(path, 'r')
#     data = infile.read()
#     
#     buffer = EncoderBuffer()
#     buffer.Init(data)
#     
#     geom_type = compress.GetEncodedGeometryType(buffer)
#     if geom_type == draco.EncodedGeometryType_TRIANGULAR_MESH:
#         mesh = compress.DecodeMeshFromBuffer(buffer)
#     elseif geom_type == draco.EncodedGeometryType_POINT_CLOUD:
#         pc = compress.DecodePointCloudFromBuffer(buffer)
#     
#     # p = PointCloud()


# def ReadMeshFromFile(path, _draco.Mesh mesh_data):
def ReadMeshFromFile(path):
    # NG: 
    # mesh_data = _draco.Mesh()
    mesh_data = Mesh()
    # cdef unique_ptr[mesh.Mesh] unique_mesh
    cdef core.StatusOr[ unique_ptr[mesh.Mesh] ] unique_mesh
    # NG
    # unique_mesh = io.ReadMeshFromFile(path)
    # unique_mesh = io.ReadMeshFromFile_IsUseMetadata(path, False)
    # NG(unique_mesh function leave pointer release)
    # mesh_data.mesh = unique_mesh.get()
    # (<unique_ptr[mesh.Mesh]>unique_mesh)
    # mesh_data.mesh = unique_mesh.release()
    cdef unique_ptr[mesh.Mesh] unique_mesh2
    # unique_mesh2 = unique_mesh.value()
    unique_mesh2 = io.ReadMeshFromFile(path).value()
    mesh_data.mesh = unique_mesh2.release()
    mesh_data.points = move(mesh_data.mesh)
    return mesh_data


def ReadPointCloudFromFile(path):
    # NG: 
    # point_data = _draco.PointCloud()
    point_data = PointCloud()
    # cdef unique_ptr[point_cloud.PointCloud] unique_points
    # cdef core.StatusOr[ unique_ptr[point_cloud.PointCloud] ] unique_points
    # unique_points = io.ReadPointCloudFromFile(path)
    cdef  unique_ptr[point_cloud.PointCloud] unique_points2
    # unique_points2 = unique_points.value()
    unique_points2 = io.ReadPointCloudFromFile(path).value()
    # point_data.points = unique_points.release()
    return point_data


# def ReadPointCloudFromStream(ss):
#     point_data = PointCloud()
#     # stringstream ss
#     cdef unique_ptr[point_cloud.PointCloud] unique_points
#     ssOut = io.ReadPointCloudFromStream(unique_points.get(), ss)
#     point_data.points = unique_points.release()
#     return point_data
