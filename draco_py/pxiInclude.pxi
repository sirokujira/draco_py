# -*- coding: utf-8 -*-

### include ###
# Attribute
include "attribute/PointAttribute.pxi"
include "attribute/GeometryAttribute.pxi"
include "attribute/AttributeTransformData.pxi"

# Compression
# include "compression/CompressEncoded.pxi"
# separate Decoder/Encoder class(1.1.0)
include "compression/Decoder.pxi"
include "compression/Encoder.pxi"
# include "compression/EncoderOptions.pxi"
# include "compression/DecoderOptions.pxi"
include "compression/ExpertEncoder.pxi"
# 0.2.0?
include "compression/dracoEncoderOptions.pxi"
include "compression/dracoDecoderOptions.pxi"

include "compression/point_cloud/PointCloudKdTreeDecoder.pxi"
include "compression/point_cloud/PointCloudKdTreeEncoder.pxi"
include "compression/point_cloud/PointCloudSequentialDecoder.pxi"
include "compression/point_cloud/PointCloudSequentialEncoder.pxi"
###

# Core
include "core/DecoderBuffer.pxi"
include "core/EncoderBuffer.pxi"
include "core/Status.pxi"
# include "core/StatusOr.pxi"
include "core/StatusOr_UniqueMesh.pxi"
###

# IO
include "io/ObjEncoder.pxi"
include "io/PlyEncoder.pxi"
include "io/ObjDecoder.pxi"
include "io/PlyDecoder.pxi"
include "io/PlyReader.pxi"
include "io/PlyElement.pxi"
include "io/PlyProperty.pxi"
# include "io/MeshIO.pxi"
###

# Mesh
include "mesh/Mesh.pxi"
include "mesh/MeshAreEquivalent.pxi"
include "mesh/TriangleSoupMeshBuilder.pxi"
###

# PointCloud
include "pointcloud/PointCloud.pxi"
###

# Metadata
include "metadata/AttributeMetadata.pxi"
include "metadata/GeometryMetadata.pxi"
###

