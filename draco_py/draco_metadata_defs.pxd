# -*- coding: utf-8 -*-

from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.memory cimport unique_ptr
from libcpp.string cimport string


# main
cimport draco_attribute_defs as attribute
cimport draco_core_defs as core
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t

###############################################################################
# Types
###############################################################################

### base ###
# metadata.h
# namespace draco {
# Class for storing a value of an entry in Metadata. Internally it is
# represented by a buffer of data. It can be accessed by various data types,
# e.g. int, float, binary data or string.
# class EntryValue {
# cdef extern from "draco/metadata/metadata.h" namespace "draco" nogil:
#     cdef cppclass EntryValue:
#         EntryValue()
# 
#         EntryValue [DataTypeT](const DataTypeT &data)
#         EntryValue [DataTypeT](const vector[DataTypeT] &data)
#         EntryValue(const EntryValue &value)
#         EntryValue(const string &value)
# 
#         bool GetValue [DataTypeT](DataTypeT *value)
#         bool GetValue [DataTypeT](vector[DataTypeT] *value)
# 
#         const vector[uint8_t] &data() const
# 
# 
###

# metadata.h
# Functor for computing a hash from data stored within an EntryValue.
# struct EntryValueHasher {
#   size_t operator()(const EntryValue &ev)
# };
# 

# metadata.h
# Class for holding generic metadata. It has a list of entries which consist of
# an entry name and an entry value. Each Metadata could also have nested metadata.
# class Metadata {
cdef extern from "draco/metadata/metadata.h" namespace "draco" nogil:
    cdef cppclass Metadata:
        Metadata()
        Metadata(const Metadata &metadata)
        # In theory, we support all types of data as long as it could be serialized
        # to binary data. We provide the following functions for inserting and
        # accessing entries of common data types. For now, developers need to know
        # the type of entries they are requesting.
        void AddEntryInt(const string &name, int32_t value)
        bool GetEntryInt(const string &name, int32_t *value)

        void AddEntryIntArray(const string &name, const vector[int32_t] &value)
        bool GetEntryIntArray(const string &name, vector[int32_t] *value)

        void AddEntryDouble(const string &name, double value)
        bool GetEntryDouble(const string &name, double *value)

        void AddEntryDoubleArray(const string &name, vector[double] &value)
        bool GetEntryDoubleArray(const string &name, vector[double] *value)

        void AddEntryString(const string &name, const string &value)
        bool GetEntryString(const string &name, string *value)

        # Add a blob of data as an entry.
        void AddEntryBinary(const string &name, const vector[uint8_t] &value)
        bool GetEntryBinary(const string &name, vector[uint8_t] *value)

        bool AddSubMetadata(const string &name, unique_ptr[Metadata] sub_metadata)
        const Metadata *GetSubMetadata(const string &name)

        void RemoveEntry(const string &name)

        int num_entries()
        # const unordered_map<string, EntryValue> &entries()
        # const unordered_map[string, unique_ptr[Metadata] ] &sub_metadatas()


###

# Functor for computing a hash from data stored within a metadata class.
# struct MetadataHasher {
#   size_t operator()(const Metadata &metadata) const {
#     size_t hash =
#         HashCombine(metadata.entries_.size(), metadata.sub_metadatas_.size());
#     EntryValueHasher entry_value_hasher;
#     for (const auto &entry : metadata.entries_) {
#       hash = HashCombine(entry.first, hash);
#       hash = HashCombine(entry_value_hasher(entry.second), hash);
#     }
#     MetadataHasher metadata_hasher;
#     for (auto &&sub_metadata : metadata.sub_metadatas_) {
#       hash = HashCombine(sub_metadata.first, hash);
#       hash = HashCombine(metadata_hasher(*sub_metadata.second), hash);
#     }
#     return hash;
#   }
# };


# geometry_metadata.h
# namespace draco {
# Class for representing specifically metadata of attributes. It must have an
# attribute id which should be identical to it's counterpart attribute in
# the point cloud it belongs to.
# class AttributeMetadata : public Metadata {
cdef extern from "draco/metadata/geometry_metadata.h" namespace "draco" nogil:
    cdef cppclass AttributeMetadata(Metadata):
        AttributeMetadata()
        # AttributeMetadata(int attribute_id)
        # AttributeMetadata(int attribute_id, const Metadata &metadata)
        AttributeMetadata(const Metadata &metadata)

        void set_att_unique_id(uint32_t att_unique_id)

        # The unique id of the attribute that this metadata belongs to.
        uint32_t att_unique_id()
        # uint32_t attribute_id()


###

# Functor for computing a hash from data stored in a AttributeMetadata class.
# struct AttributeMetadataHasher {
#   size_t operator()(const AttributeMetadata &metadata) const
###

# Class for representing the metadata for a point cloud. 
# It could have a list of attribute metadata.
# class GeometryMetadata : public Metadata {
cdef extern from "draco/metadata/geometry_metadata.h" namespace "draco" nogil:
    cdef cppclass GeometryMetadata(Metadata):
        GeometryMetadata()
        GeometryMetadata(const Metadata &metadata)

        const AttributeMetadata *GetAttributeMetadataByStringEntry(const string &entry_name, const string &entry_value) const
        bool AddAttributeMetadata(unique_ptr[AttributeMetadata] att_metadata)
        void DeleteAttributeMetadataByUniqueId(int32_t att_unique_id)
        const AttributeMetadata *GetAttributeMetadataByUniqueId(int32_t att_unique_id) const

        AttributeMetadata *attribute_metadata(int32_t attribute_id)
        const vector[unique_ptr[AttributeMetadata]] &attribute_metadatas()


###


# Functor for computing a hash from data stored in a GeometryMetadata class.
# struct GeometryMetadataHasher 
# {
#   size_t operator()(const GeometryMetadata &metadata) const
# };
###


###########################

# metadata_decoder.h
# namespace draco {
# Class for decoding the metadata.
# class MetadataDecoder {
cdef extern from "draco/metadata/metadata_decoder.h" namespace "draco" nogil:
    cdef cppclass MetadataDecoder:
        MetadataDecoder()

        bool DecodeMetadata(core.DecoderBuffer *in_buffer, Metadata *metadata)
        bool DecodeGeometryMetadata(core.DecoderBuffer *in_buffer, GeometryMetadata *metadata)


###

# metadata_encoder.h
# namespace draco {
# Class for encoding metadata. It could encode either base Metadata class or
# a metadata of a geometry, e.g. a point cloud.
# class MetadataEncoder {
cdef extern from "draco/metadata/metadata_encoder.h" namespace "draco" nogil:
    cdef cppclass MetadataEncoder:
        MetadataEncoder()
        bool EncodeGeometryMetadata(core.EncoderBuffer *out_buffer, const GeometryMetadata *metadata)
        bool EncodeMetadata(core.EncoderBuffer *out_buffer, const Metadata *metadata)


###

###############################################################################
# Enum
###############################################################################

