# -*- coding: utf-8 -*-
# Header for _draco.pyx functionality that needs sharing with other modules.

# Attribute
cimport draco_attribute_defs as attribute
# Compress
cimport draco_compress_defs as compress
# Core
cimport draco_core_defs as core
# io
cimport draco_io_defs as io
# mesh
cimport draco_mesh_defs as mesh
# point_cloud
cimport draco_point_cloud_defs as point_cloud
# metadata
cimport draco_metadata_defs as metadata

from libcpp.memory cimport unique_ptr

# class override(DracoModule)
# cdef class DracoModule:


### Attribute ###

# class override(AttributeTransformData)
cdef class AttributeTransformData:
    # cdef const attribute.AttributeTransformData* attrTdata
    cdef attribute.AttributeTransformData* attrTdata


# class override(PointAttribute)
cdef class PointAttribute:
    # cdef const attribute.PointAttribute* attr
    cdef attribute.PointAttribute* attr


# class override(GeometryAttribute)
cdef class GeometryAttribute:
    cdef attribute.GeometryAttribute* geometry


###


### Compress ###
# Replace EncoderOptions
cdef class dracoEncoderOptions:
    cdef compress.EncoderOptions* options


cdef class EncoderOptions:
    cdef compress.EncoderOptions* options


# Replace DecoderOptions
cdef class dracoDecoderOptions:
    cdef compress.DecoderOptions* options


cdef class DecoderOptions:
    cdef compress.DecoderOptions* options


cdef class Decoder:
    cdef compress.Decoder* me


cdef class Encoder:
    cdef compress.Encoder* me


cdef class ExpertEncoder:
    cdef compress.ExpertEncoder* me


cdef class PointCloudKdTreeDecoder:
    cdef compress.PointCloudKdTreeDecoder* points


cdef class PointCloudKdTreeEncoder:
    cdef compress.PointCloudKdTreeEncoder* points


cdef class PointCloudSequentialDecoder:
    cdef compress.PointCloudSequentialDecoder* points


cdef class PointCloudSequentialEncoder:
    cdef compress.PointCloudSequentialEncoder* points


### Core ###

# class override(DecoderBuffer)
cdef class DecoderBuffer:
    cdef core.DecoderBuffer* module


# class override(EncoderBuffer)
cdef class EncoderBuffer:
    cdef core.EncoderBuffer* module


# class override(DataBuffer)
cdef class DataBuffer:
    cdef core.DataBuffer* module


# class override(Status)
cdef class Status:
    # cdef core.Status* status
    cdef core.Status status


# class override(StatusOr)
cdef class StatusOr_UniqueMesh:
    # cdef core.StatusOr* status
    cdef core.StatusOr[ unique_ptr[mesh.Mesh] ] status


###


### IO ###

# class override(ObjDecoder)
cdef class ObjDecoder:
    cdef io.ObjDecoder* decoder


# class override(ObjEncoder)
cdef class ObjEncoder:
    cdef io.ObjEncoder* encoder


# class override(PlyEncoder)
cdef class PlyEncoder:
    cdef io.PlyEncoder* encoder


# class override(PlyDecoder)
cdef class PlyDecoder:
    cdef io.PlyDecoder* decoder


# class override(PlyReader)
cdef class PlyReader:
    cdef io.PlyReader* reader


# class override(PlyElement)
cdef class PlyElement:
    cdef io.PlyElement* element
    # cdef const io.PlyElement& cns_element
    cdef const io.PlyElement* cns_element


# class override(PlyProperty)
cdef class PlyProperty:
    cdef io.PlyProperty* property
    cdef const io.PlyProperty* cns_property


###


### Mesh ###
# class override(Mesh)
cdef class Mesh:
    cdef compress.Decoder* decode
    cdef mesh.Mesh* mesh
    cdef point_cloud.PointCloud* points


# class override(MeshAreEquivalent)
cdef class MeshAreEquivalent:
    cdef mesh.MeshAreEquivalent* mesh


# class override(TriangleSoupMeshBuilder)
cdef class TriangleSoupMeshBuilder:
    cdef mesh.TriangleSoupMeshBuilder* mesh


###

### PointCloud ###

# class override(PointCloud)
cdef class PointCloud:
    cdef compress.Decoder* decode
    cdef point_cloud.PointCloud* points


###

### Metadata ###

# class override(GeometryMetadata)
cdef class GeometryMetadata:
    cdef metadata.GeometryMetadata* meta
    cdef const metadata.GeometryMetadata* cns_meta


# class override(AttributeMetadata)
cdef class AttributeMetadata:
    cdef metadata.AttributeMetadata* meta
    cdef const metadata.AttributeMetadata* cns_meta


###