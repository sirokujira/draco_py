# -*- coding: utf-8 -*-
from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.string cimport string

# main
# cimport draco_core_defs as core


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t


###############################################################################
# Types
###############################################################################

# adaptive_rans_bit_coding_shared.h
###

# adaptive_rans_bit_decoder.h
###

# adaptive_rans_bit_encoder.h
###

# ans.h
###

# bit_coder.h
###

# bit_utils.h
###

# cycle_timer.h
###

# data_buffer.h
# Buffer descriptor servers as a unique identifier of a buffer.
# struct DataBufferDescriptor {
#   DataBufferDescriptor() : buffer_id(0), buffer_update_count(0) {}
#   # Id of the data buffer.
#   int64_t buffer_id;
#   # The number of times the buffer content was updated.
#   int64_t buffer_update_count;
# };

# data_buffer.h
# Class used for storing raw buffer data.
cdef extern from "draco/core/data_buffer.h" namespace "draco" nogil:
    cdef cppclass DataBuffer:
        DataBuffer();
        bool Update(const void *data, int64_t size);
        # TODO(zhafang): The two update functions should be combined. I will
        # leave for now in case it breaks any geometry compression tools.
        bool Update(const void *data, int64_t size, int64_t offset)

        # Reallocate the buffer storage to a new size keeping the data unchanged.
        void Resize(int64_t new_size)
        # void WriteDataToStream(std::ostream &stream)
        
        # Reads data from the buffer. Potentially unsafe, called needs to ensure
        # the accessed memory is valid.
        void Read(int64_t byte_pos, void *out_data, size_t data_size)

        # Writes data to the buffer. Unsafe, caller must ensure the accessed memory
        # is valid.
        void Write(int64_t byte_pos, const void *in_data, size_t data_size)

        # Copies data from another buffer to this buffer.
        void Copy(int64_t dst_offset, const DataBuffer *src_buf, int64_t src_offset, int64_t size)

        void set_update_count(int64_t buffer_update_count)
        int64_t update_count()
        size_t data_size()
        const uint8_t *data()
        uint8_t *data()
        int64_t buffer_id()
        void set_buffer_id(int64_t buffer_id)


##

# decoder_buffer.h
# namespace draco
# Class is a wrapper around input data used by MeshDecoder. It provides a
# basic interface for decoding either typed or variable-bit sized data.
# class DecoderBuffer
cdef extern from "draco/core/decoder_buffer.h" namespace "draco" nogil:
    cdef cppclass DecoderBuffer:
        # public:
        DecoderBuffer()
        # DecoderBuffer(const DecoderBuffer &buf)
        # DecoderBuffer &operator=(const DecoderBuffer &buf) = default;

        # Sets the buffer's internal data. Note that no copy of the input data is
        # made so the data owner needs to keep the data valid and unchaged for
        # runtime of the decoder.
        # void Init(const char *data, size_t data_size);
        void Init(const char *data, size_t data_size)

        # Starts decoding a bit sequence.
        # decode_size must be true if the size of the encoded bit data was included,
        # during encoding. The size is then returned to out_size.
        # Returns false on error.
        # bool StartBitDecoding(bool decode_size, uint64_t *out_size);
        bool StartBitDecoding(bool decode_size, unsigned long *out_size)

        # Ends the decoding of the bit sequence and return to the default
        # byte-aligned decoding.
        void EndBitDecoding()

        # Decodes up to 32 bits into out_val. Can be called only in between
        # StartBitDecoding and EndBitDeoding. Otherwise returns false.
        # bool DecodeLeastSignificantBits32(int nbits, uint32_t *out_value)
        bool DecodeLeastSignificantBits32(int nbits, unsigned int *out_value)

        # Decodes an arbitrary data type.
        # Can be used only when we are not decoding a bit-sequence.
        # Returns false on error.
        # template <typename T> bool Decode(T *out_val)
        # bool Decode [T](T *out_val)
        bool Decode(int8_t *out_val)
        bool Decode(void *out_data, size_t size_to_decode)

        # Decodes an arbitrary data, but does not advance the reading position.
        # template <typename T> bool Peek(T *out_val)
        bool Peek [T](T *out_val)
        bool Peek(void *out_data, size_t size_to_peek)
        
        # Discards #bytes from the input buffer.
        # void Advance(int64_t bytes)
        void Advance(int bytes)

        # Moves the parsing position to a specific offset from the beggining of the
        # input data.
        # void StartDecodingFrom(int64_t offset)
        void StartDecodingFrom(int offset)

        # Returns the data array at the current decoder position.
        # const char *data_head() const
        # int64_t remaining_size() const
        long remaining_size()

        # int64_t decoded_size() const
        long decoded_size()

        # BitDecoder *bit_decoder()
        # bool bit_decoder_active() const


###


# direct_bit_decoder.h
###

# direct_bit_encoder.h
###

# divide.h
###

# draco_index_type.h
# # DEFINE_NEW_DRACO_INDEX_TYPE(int32_t, FaceIndex);
# struct FaceIndex
# ctypedef IndexType[int32_t, FaceIndex_tag_type_] FaceIndex
# namespace draco
# define DEFINE_NEW_DRACO_INDEX_TYPE(value_type, name) \
# struct name##_tag_type_ {};                         \
# typedef IndexType<value_type, name##_tag_type_> name;

# draco_index_type.h
# template <class ValueTypeT, class TagT>
# class IndexType {
# cdef extern from "draco/core/draco_index_type.h" namespace "draco" nogil:
#     cdef cppclass IndexType[ValueTypeT, TagT]:
#         # public:
#         IndexType()
# 
#         # public:
#         # typedef IndexType<ValueTypeT, TagT> ThisIndexType;
#         # typedef ValueTypeT ValueType;
#         ctypedef ThisIndexType[ValueTypeT, TagT] ThisIndexType
#         ctypedef ValueTypeT ValueType
# 
#         # constexpr IndexType() : value_(ValueTypeT()) {}
#         # constexpr explicit IndexType(ValueTypeT value) : value_(value) {}
# 
#         # constexpr ValueTypeT value()
# 
#         constexpr bool operator==(const IndexType &i) const
#         constexpr bool operator==(const ValueTypeT &val) const
#         constexpr bool operator!=(const IndexType &i) const
#         constexpr bool operator!=(const ValueTypeT &val) const
#         constexpr bool operator<(const IndexType &i) const
#         constexpr bool operator<(const ValueTypeT &val) const
#         constexpr bool operator>(const IndexType &i) const
#         constexpr bool operator>(const ValueTypeT &val) const
#         constexpr bool operator>=(const IndexType &i) const
#         constexpr bool operator>=(const ValueTypeT &val) const
# 
#         inline ThisIndexType &operator++()
#         inline ThisIndexType operator++(int)
#         inline ThisIndexType &operator--()
#         inline ThisIndexType operator--(int)
# 
#         constexpr ThisIndexType operator+(const IndexType &i)
#         constexpr ThisIndexType operator+(const ValueTypeT &val)
#         constexpr ThisIndexType operator-(const IndexType &i)
#         constexpr ThisIndexType operator-(const ValueTypeT &val)
# 
#         inline ThisIndexType &operator+=(const IndexType &i)
#         inline ThisIndexType operator+=(const ValueTypeT &val)
#         inline ThisIndexType &operator-=(const IndexType &i)
#         inline ThisIndexType operator-=(const ValueTypeT &val)
#         inline ThisIndexType &operator=(const ThisIndexType &i)
#         inline ThisIndexType &operator=(const ValueTypeT &val)
# 
# 
# Stream operator << provided for logging purposes.
# template <class ValueTypeT, class TagT>
# std::ostream &operator<<(std::ostream &os, IndexType<ValueTypeT, TagT> index) {
#   return os << index.value();
# }

#  Specialize std::hash for the strongly indexed types.
# namespace std {
# 
# template <class ValueTypeT, class TagT>
# struct hash<draco::IndexType<ValueTypeT, TagT>> {
#   size_t operator()(const draco::IndexType<ValueTypeT, TagT> &i) const {
#     return static_cast<size_t>(i.value());
#   }
# };
# 
# // namespace std


###

# draco_index_type_vector.h
###

# draco_test_base.h
###

# draco_test_utils.h
###

# draco_types.h
###

# draco_version.h
###

# encoder_buffer.h
# namespace draco
# Class representing a buffer that can be used for either for byte-aligned
# encoding of arbitrary data structures or for encoding of varialble-length
# bit data.
# class EncoderBuffer {
cdef extern from "draco/core/encoder_buffer.h" namespace "draco" nogil:
    cdef cppclass EncoderBuffer:
        # public:
        EncoderBuffer()
        void Clear()
        # void Resize(int64_t nbytes)
        # 
        # Start encoding a bit sequence. A maximum size of the sequence needs to
        # be known upfront.
        # If encode_size is true, the size of encoded bit sequence is stored before
        # the sequence. Decoder can then use this size to skip over the bit sequence
        # if needed.
        # Returns false on error.
        # bool StartBitEncoding(int64_t required_bits, bool encode_size);
        bool StartBitEncoding(int required_bits, bool encode_size)

        # End the encoding of the bit sequence and return to the default byte-aligned encoding.
        void EndBitEncoding()

        # Encode up to 32 bits into the buffer. Can be called only in between
        # StartBitEncoding and EndBitEncoding. Otherwise returns false.
        # bool EncodeLeastSignificantBits32(int nbits, uint32_t value)
        bool EncodeLeastSignificantBits32(int nbits, unsigned int value)

        # public:
        # Encode an arbitrary data type.
        # Can be used only when we are not encoding a bit-sequence.
        # Returns false when the value couldn't be encoded.
        # template <typename T> bool Encode(const T &data)
        # bool Encode [T](const T &data)
        # 
        # bool Encode(const void *data, size_t data_size)
        # bool Encode_data_size "Encode"(const void *data, size_t data_size)
        # 
        bool bit_encoder_active()

        const char *data()

        size_t size()

        # std::vector<char> *buffer()
        # BitEncoder *bit_encoder()


###

# folded_integer_bit_decoder.h
###

# folded_integer_bit_encoder.h
###


# hash_utils.h
###

# macros.h
###

# math_utils.h
###

# options.h
# Class for storing generic options as a <name, value> pair in a string map.
# The API provides helper methods for directly storing values of various types
# such as ints and bools. One named option should be set with only a single
# data type.
# class Options
cdef extern from "draco/core/options.h" namespace "draco" nogil:
    cdef cppclass Options:
        # public:
        Options()
        void SetInt(const string &name, int val);
        void SetBool(const string &name, bool val);
        void SetString(const string &name, const string &val);

        # Getters will return a default value if the entry is not found. The default
        # value can be specified in the overloaded version of each function.
        int GetInt(const string &name)
        int GetInt(const string &name, int default_val)
        bool GetBool(const string &name)
        bool GetBool(const string &name, bool default_val)
        string GetString(const string &name)
        string GetString(const string &name, const string &default_val)

        bool IsOptionSet(const string &name)


###

# quantization_utils.h
###

# rans_bit_decoder.h
###

# rans_bit_encoder.h
###

# rans_symbol_coding.h
###

# rans_symbol_decoder.h
###

# rans_symbol_encoder.h
###

# shannon_entropy.h
###

# status.h
# namespace draco
# Class encapsulating a return status of an operation with an optional error
# message. Intended to be used as a return type for functions instead of bool.
cdef extern from "draco/core/status.h" namespace "draco" nogil:
    cdef cppclass Status:
        Status()
        # Status(const Status &status)
        # Status(Status &&status)
        # Status(Code2 code)
        # Status(Code2 code, const string &error_msg)
        # 
        Code2 code() const
        # const string &error_msg_string()
        # const char *error_msg() const
        # 
        # bool operator==(Code code) const
        # bool operator==(Code2 code) const
        bool ok()
        # Status &operator=(const Status &) = default;


###

# statusor.h
# Class StatusOr is used to wrap a Status along with a value of a specified
# type |T|. StatusOr is intended to be returned from functions in situations
# where it is desirable to carry over more information about the potential
# errors encountered during the function execution. If there are not errors,
# the caller can simply use the return value, otherwise the Status object
# provides more info about the encountered problem.
cdef extern from "draco/core/decoder_buffer.h" namespace "draco" nogil:
    cdef cppclass StatusOr[T]:
        StatusOr()
        
        # Note: Constructors are intentionally not explicit to allow returning
        # Status or the return value directly from functions.
        # StatusOr(const StatusOr &) = default;
        # StatusOr(StatusOr &&) = default;
        # StatusOr(const Status &status)
        # StatusOr(const T &value)
        StatusOr(T &&value)
        # StatusOr(const Status &status, const T &value)

        # const Status &status() const
        # const T &value() const &
        # const T &&value() const &&
        # T &&value() &&
        T value()

        #  For consistency with existing Google StatusOr API we also include
        #  ValueOrDie() that currently returns the value().
        # const T &ValueOrDie() const &
        # T &&ValueOrDie() && 
        # const T &ValueOrDie() const &
        T ValueOrDie()

        # bool ok() const
        bool ok()


##

# symbol_bit_decoder.h
###

# symbol_bit_encoder.h
###

# symbol_decoding.h
###

# symbol_encoding.h
###

# varint_decoding.h
###

# varint_encoding.h
###

# vector_d.h
###

###############################################################################
# Enum
###############################################################################


cdef extern from "draco/core/draco_types.h" namespace "draco":
    cdef enum DataType:
        DT_INVALID = 0,
        DT_INT8,
        DT_UINT8,
        DT_INT16,
        DT_UINT16,
        DT_INT32,
        DT_UINT32,
        DT_INT64,
        DT_UINT64,
        DT_FLOAT32,
        DT_FLOAT64,
        DT_BOOL,
        DT_TYPES_COUNT


cdef extern from "draco/core/status.h" namespace "draco":
    ctypedef enum Code2 "draco::Status::Code":
        Core_OK "draco::Status::OK"
        Core_ERROR "draco::Status::ERROR"                             # Used for general errors.
        Core_IO_ERROR "draco::Status::IO_ERROR",                      # Error when handling input or output stream.
        Core_INVALID_PARAMETER "draco::Status::INVALID_PARAMETER"     # Invalid parameter passed to a function.
        Core_UNSUPPORTED_VERSION "draco::Status::UNSUPPORTED_VERSION" # Input not compatible with the current version.
        Core_UNKNOWN_VERSION "draco::Status::UNKNOWN_VERSION"         # Input was created with an unknown version of the library.


###

###############################################################################
# Activation
###############################################################################

