# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_core_defs as core
from libcpp.string cimport string

# from cython.operator import address

cdef class EncoderBuffer:
    """
    """
    # cdef core.EncoderBuffer* module

    def __cinit__(self):
        self.module = new core.EncoderBuffer()

    def Clear(self):
        self.module.Clear()

    # void Resize(int64_t nbytes)
    # bool StartBitEncoding(int64_t required_bits, bool encode_size);

    def EndBitEncoding(self):
        self.module.EndBitEncoding()

    # bool EncodeLeastSignificantBits32(int nbits, uint32_t value)
    def EncodeLeastSignificantBits32(self, int nbits, unsigned int value):
        return self.module.EncodeLeastSignificantBits32(nbits, value)

    # template <typename T> bool Encode(const T &data)
    # bool Encode(const void *data, size_t data_size)


    # bool bit_encoder_active() const
    def Bit_Encoder_Active(self):
        return self.module.bit_encoder_active()


    def GetData(self):
        # NG : string separate newline character? or 0x00?
        # return self.module.data()
        length = self.module.size()
        py_bytes_string = self.module.data()[:length]
        return py_bytes_string


    def GetSize(self):
        return self.module.size()


    def WriteFile(self, string filename):
        length = self.module.size()
        py_bytes_string = self.module.data()[:length]
        # print(py_bytes_string)
        out_file = open(filename, 'wb')
        out_file.write(py_bytes_string)
        out_file.close

    # std::vector<char> *buffer()
    # BitEncoder *bit_encoder()


