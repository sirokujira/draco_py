# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_core_defs as core

cdef class StatusOr:
    """
    """

    def __cinit__(self):
        # self.status = new core.Status()
        pass


    def __dealloc__(self):
        # del self.status
        pass


    # return bool
    def ok(self):
        # return (<core.StatusOr[ unique_ptr[mesh.Mesh] ]> self.status).ok()
        return self.status.ok()
