# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cimport draco_core_defs as core
from cython cimport address

cdef class DecoderBuffer:
    """
    """
    # cdef core.DecoderBuffer* module


    def __cinit__(self):
        self.module = new core.DecoderBuffer()


    def __dealloc__(self):
        del self.module


    def Init(self, data):
        data_len = len(data)
        # self.module.Init(data, data_len)
        cdef const char* testdata = data
        self.module.Init(testdata, data_len)


    # def StartBitDecoding(self, flag):
    #     self.module.StartBitDecoding(bool decode_size, uint64_t *out_size)


    # return int64_t
    def Advance(self, param):
        self.module.Advance(param)


    # return int8_t
    def Decode(self):
        cdef signed char encoded_method
        self.module.Decode(address(encoded_method))
        return encoded_method


