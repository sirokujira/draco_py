# -*- coding: utf-8 -*-
from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.memory cimport unique_ptr
from libcpp.string cimport string

# 
cimport draco_attribute_defs as attribute
cimport draco_core_defs as core
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud


from libc.stdint cimport int8_t
from libc.stdint cimport int16_t
from libc.stdint cimport int32_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint8_t
from libc.stdint cimport uint16_t
from libc.stdint cimport uint32_t
from libc.stdint cimport uint64_t

###############################################################################
# Types
###############################################################################


### base ###

# compression/config/draco_options.h
# namespace draco
# Base option class used to control encoding and decoding. The geometry coding
# can be controlled through the following options:
#   1. Global options - Options specific to overall geometry or options common
#                       for all attributes
#   2. Per attribute options - Options specific to a given attribute.
#                              Each attribute is identified by the template
#                              argument AttributeKeyT that can be for example
#                              the attribute type or the attribute id.
#
# Example:
#
#   DracoOptions<AttributeKey> options;
#
#   // Set an option common for all attributes.
#   options.SetGlobalInt("some_option_name", 2);
#
#   // Geometry with two attributes.
#   AttributeKey att_key0 = in_key0;
#   AttributeKey att_key1 = in_key1;
#
#   options.SetAttributeInt(att_key0, "some_option_name", 3);
#
#   options.GetAttributeInt(att_key0, "some_option_name");  // Returns 3
#   options.GetAttributeInt(att_key1, "some_option_name");  // Returns 2
#   options.GetGlobalInt("some_option_name");               // Returns 2
#
# template <typename AttributeKeyT>
# class DracoOptions {
cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
    cdef cppclass DracoOptions[AttributeKeyT]:
        DracoOptions()
        # public:
        # typedef AttributeKeyT AttributeKey;

        # Get an option for a specific attribute key. If the option is not found in
        # an attribute specific storage, the implementation will return a global
        # option of the given name (if available). If the option is not found, 
        # the provided default value |default_val| is returned instead.
        int GetAttributeInt(const AttributeKeyT &att_key, const string &name, int default_val)

        # Sets an option for a specific attribute key.
        void SetAttributeInt(const AttributeKeyT &att_key, const string &name, int val)

        bool GetAttributeBool(const AttributeKeyT &att_key, const string &name, bool default_val)
        void SetAttributeBool(const AttributeKeyT &att_key, const string &name, bool val)
        bool IsAttributeOptionSet(const AttributeKeyT &att_key, const string &name)

        # Gets/sets a global option that is not specific to any attribute.
        int GetGlobalInt(const string &name, int default_val)
        void SetGlobalInt(const string &name, int val)
        bool GetGlobalBool(const string &name, bool default_val)
        void SetGlobalBool(const string &name, bool val)
        bool IsGlobalOptionSet(const string &name)

        # Sets or replaces attribute options with the provided |options|.
        void SetAttributeOptions(const AttributeKeyT &att_key, const core.Options &options)
        void SetGlobalOptions(const core.Options &options)

        # Returns |Options| instance for the specified options class if it exists.
        const core.Options *FindAttributeOptions(const AttributeKeyT &att_key)
        const core.Options &GetGlobalOptions()


# # template <typename AttributeKeyT>
# # const Options *DracoOptions<AttributeKeyT>::FindAttributeOptions(const AttributeKeyT &att_key)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         const core.Options FindAttributeOptions(const AttributeKeyT &attkey)
# 
# 
# # template <typename AttributeKeyT>
# # Options *DracoOptions<AttributeKeyT>::GetAttributeOptions(const AttributeKeyT &att_key)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         core.Options* GetAttributeOptions(const AttributeKeyT &attkey)
# 
# 
# # template <typename AttributeKeyT>
# # int DracoOptions<AttributeKeyT>::GetAttributeInt(const AttributeKeyT &att_key,
# #                                                  const std::string &name,
# #                                                  int default_val)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         int GetAttributeInt(const AttributeKeyT &attkey, const string &name, int default_val)
# 
# 
# # template <typename AttributeKeyT>
# # void DracoOptions<AttributeKeyT>::SetAttributeInt(const AttributeKeyT &att_key,
# #                                                   const std::string &name,
# #                                                   int val)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         void SetAttributeInt(const AttributeKeyT &attkey, const string &name, int val)
# 
# 
# 
# # template <typename AttributeKeyT>
# # bool DracoOptions<AttributeKeyT>::GetAttributeBool(const AttributeKeyT &att_key,
# #                                                    const std::string &name,
# #                                                    bool default_val)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         bool GetAttributeBool(const AttributeKeyT &attkey, const string &name, bool default_val)
# 
# 
# # template <typename AttributeKeyT>
# # void DracoOptions<AttributeKeyT>::SetAttributeBool(const AttributeKeyT &att_key,
# #                                                    const std::string &name,
# #                                                    bool val)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         void SetAttributeBool(const AttributeKeyT &attkey, const string &name, bool val)
# 
# 
# # template <typename AttributeKeyT>
# # bool DracoOptions<AttributeKeyT>::IsAttributeOptionSet(const AttributeKey &att_key, const std::string &name) const
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         bool IsAttributeOptionSet(const AttributeKeyT &attkey, const string &name)
# 
# 
# # template <typename AttributeKeyT>
# # void DracoOptions<AttributeKeyT>::SetAttributeOptions(
# #     const AttributeKey &att_key, const Options &options)
# cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
#     cdef cppclass DracoOptions[AttributeKeyT]:
#         bool SetAttributeOptions(const AttributeKeyT &attkey, const core.Options &options)


# ctypedef DracoOptions[attribute.Type2] DecoderOptions
ctypedef DracoOptions[int32_t] EncoderBaseOptions_t
###

# compression/config/decoder_options.h
# namespace draco
# Class containing options that can be passed to PointCloudDecoder to control
# decoding of the input geometry. The options can be specified either for the
# whole geometry or for a specific attribute type. Each option is identified
# by a unique name stored as an std::string.
cdef extern from "draco/compression/config/decoder_options.h" namespace "draco" nogil:
    ctypedef DracoOptions[attribute.Type2] DecoderOptions


###


# compression/attributes
# attributes_decoder_interface.h
# namespace draco {
# class PointCloudDecoder;
# Interface class for decoding one or more attributes that were encoded with a
# matching AttributesEncoder. It provides only the basic interface
# that is used by the PointCloudDecoder. The actual decoding must be
# implemented in derived classes using the DecodeAttributes() method.
cdef extern from "draco/compression/attributes/attributes_decoder_interface.h" namespace "draco" nogil:
    cdef cppclass AttributesDecoderInterface:
        AttributesDecoderInterface()
#         AttributesDecoderInterface() = default;
#         virtual ~AttributesDecoderInterface() = default;
# 
#         # Called after all attribute decoders are created. It can be used to perform
#         # any custom initialization.
#         virtual bool Initialize(PointCloudDecoder *decoder, PointCloud *pc) = 0;
# 
#         # Decodes any attribute decoder specific data from the |in_buffer|.
#         virtual bool DecodeAttributesDecoderData(DecoderBuffer *in_buffer) = 0;
# 
#         # Decode attribute data from the source buffer. Needs to be implemented by
#         # the derived classes.
#         virtual bool DecodeAttributes(DecoderBuffer *in_buffer) = 0;
# 
#         virtual int32_t GetAttributeId(int i) const = 0;
#         virtual int32_t GetNumAttributes() const = 0;
#         virtual PointCloudDecoder *GetDecoder() const = 0;
# 
#         # Returns an attribute containing data processed by the attribute transform.
#         # (see TransformToPortableFormat() method). This data is guaranteed to be
#         # same for encoder and decoder and it can be used by predictors.
#         virtual const PointAttribute *GetPortableAttribute(int32_t /* point_attribute_id */)
# 
# 
###

# attributes_decoder.h
# namespace draco {
# Base class for decoding one or more attributes that were encoded with a
# matching AttributesEncoder. It is a basic implementation of
# AttributesDecoderInterface that provides functionality that is shared between
# all AttributesDecoders.
# class AttributesDecoder : public AttributesDecoderInterface {
#     public:
cdef extern from "draco/compression/attributes/attributes_decoder.h" namespace "draco" nogil:
    cdef cppclass AttributesDecoder(AttributesDecoderInterface):
        AttributesDecoder();
#         virtual ~AttributesDecoder() = default;
# 
#         # Called after all attribute decoders are created. It can be used to perform
#         # any custom initialization.
#         bool Initialize(PointCloudDecoder *decoder, PointCloud *pc) override;
# 
#         # Decodes any attribute decoder specific data from the |in_buffer|.
#         bool DecodeAttributesDecoderData(DecoderBuffer *in_buffer) override;
# 
#         int32_t GetAttributeId(int i) const override
#         int32_t GetNumAttributes() const override
#         PointCloudDecoder *GetDecoder() const override
# 
#         # Decodes attribute data from the source buffer.
#         bool DecodeAttributes(DecoderBuffer *in_buffer) override
# 
# 
###

# attributes_encoder.h
# namespace draco {
# class PointCloudEncoder;
# Base class for encoding one or more attributes of a PointCloud (or other
#  geometry). This base class provides only the basic interface that is used
# by the PointCloudEncoder.
# class AttributesEncoder {
cdef extern from "draco/compression/attributes/attributes_decoder.h" namespace "draco" nogil:
    cdef cppclass AttributesEncoder:
        AttributesEncoder();
#         # Constructs an attribute encoder associated with a given point attribute.
#         explicit AttributesEncoder(int point_attrib_id);
#         virtual ~AttributesEncoder() = default;
# 
#         # Called after all attribute encoders are created. It can be used to perform
#         # any custom initialization, including setting up attribute dependencies.
#         # Note: no data should be encoded in this function, because the decoder may
#         # process encoders in a different order from the decoder.
#         virtual bool Initialize(PointCloudEncoder *encoder, const PointCloud *pc);
# 
#         # Encodes data needed by the target attribute decoder.
#         virtual bool EncodeAttributesEncoderData(EncoderBuffer *out_buffer);
# 
#         # Returns a unique identifier of the given encoder type, that is used during
#         # decoding to construct the corresponding attribute decoder.
#         virtual uint8_t GetUniqueId() const = 0;
# 
#         # Encode attribute data to the target buffer.
#         virtual bool EncodeAttributes(EncoderBuffer *out_buffer)
# 
#         # Returns the number of attributes that need to be encoded before the
#         # specified attribute is encoded.
#         # Note that the attribute is specified by its point attribute id.
#         virtual int NumParentAttributes(int32_t /* point_attribute_id */) const
# 
#         virtual int GetParentAttributeId(int32_t /* point_attribute_id */, int32_t /* parent_i */)
# 
#         # Marks a given attribute as a parent of another attribute.
#         virtual bool MarkParentAttribute(int32_t /* point_attribute_id */)
# 
#         # Returns an attribute containing data processed by the attribute transform.
#         # (see TransformToPortableFormat() method). This data is guaranteed to be
#         # encoded losslessly and it can be safely used for predictors.
#         virtual const PointAttribute *GetPortableAttribute(int32_t /* point_attribute_id */)
# 
#         void AddAttributeId(int32_t id)
# 
#         # Sets new attribute point ids (replacing the existing ones).
#         void SetAttributeIds(const std::vector<int32_t> &point_attribute_ids)
# 
#         int32_t GetAttributeId(int i)
#         uint32_t num_attributes()
#         PointCloudEncoder *encoder()
# 
# 
###

# kd_tree_attributes_decoder.h
# namespace draco {
# Decodes attributes encoded with the KdTreeAttributesEncoder.
# class KdTreeAttributesDecoder : public AttributesDecoder {
# public:
cdef extern from "draco/compression/attributes/kd_tree_attributes_decoder.h" namespace "draco" nogil:
    cdef cppclass KdTreeAttributesDecoder(AttributesDecoder):
        KdTreeAttributesDecoder()


###

# kd_tree_attributes_encoder.h
# namespace draco {
# Encodes all attributes of a given PointCloud using one of the available
# Kd-tree compression methods.
# See compression/point_cloud/point_cloud_kd_tree_encoder.h for more details.
# class KdTreeAttributesEncoder : public AttributesEncoder {
# public:
cdef extern from "draco/compression/attributes/kd_tree_attributes_encoder.h" namespace "draco" nogil:
    cdef cppclass KdTreeAttributesEncoder(AttributesEncoder):
        KdTreeAttributesEncoder();
#         explicit KdTreeAttributesEncoder(int att_id)
#         uint8_t GetUniqueId()
# 
# 
###


# linear_sequencer.h
# namespace draco {
#  A simple sequencer that generates a linear sequence [0, num_points - 1].
#  I.e., the order of the points is preserved for the input data.
# class LinearSequencer : public PointsSequencer {
# public:
# cdef extern from "draco/compression/attributes/linear_sequencer.h" namespace "draco" nogil:
#     cdef cppclass LinearSequencer(PointsSequencer):
#         explicit LinearSequencer(int32_t num_points)
#         bool UpdatePointToAttributeIndexMapping(PointAttribute *attribute)
# 
# 
###

# mesh_attribute_indices_encoding_data.h
# namespace draco {
# Data used for encoding and decoding of mesh attributes.
# struct MeshAttributeIndicesEncodingData {
cdef extern from "draco/compression/attributes/mesh_attribute_indices_encoding_data.h" namespace "draco" nogil:
    cdef cppclass MeshAttributeIndicesEncodingData:
        MeshAttributeIndicesEncodingData()
#         # Array for storing the corner ids in the order their associated attribute
#         # entries were encoded/decoded. For every encoded attribute value entry we
#         # store exactly one corner. I.e., this is the mapping between an encoded
#         # attribute entry ids and corner ids. This map is needed for example by
#         # prediction schemes. Note that not all corners are included in this map,
#         # e.g., if multiple corners share the same attribute value, only one of these
#         # corners will be usually included.
#         std::vector<CornerIndex> encoded_attribute_value_index_to_corner_map;
# 
#         # Map for storing encoding order of attribute entries for each vertex.
#         # i.e. Mapping between vertices and their corresponding attribute entry ids
#         # that are going to be used by the decoder.
#         # -1 if an attribute entry hasn't been encoded/decoded yet.
#         std::vector<int32_t> vertex_to_encoded_attribute_value_index_map;
# 
#         # Total number of encoded/decoded attribute entries.
#         int num_values;
# 
# 
###

# mesh_attribute_indices_encoding_observer.h
# namespace draco {
# Class that can be used to generate encoding (and decoding) order of attribute
# values based on the traversal of the encoded mesh. The class should be used
# as the TraversalObserverT member of a Traverser class such as the
# EdgeBreakerTraverser (edgebreaker_traverser.h).
# template <class CornerTableT>
# class MeshAttributeIndicesEncodingObserver {
cdef extern from "draco/compression/attributes/mesh_attribute_indices_encoding_observer.h" namespace "draco" nogil:
    cdef cppclass MeshAttributeIndicesEncodingData:
        MeshAttributeIndicesEncodingObserver()
#         MeshAttributeIndicesEncodingObserver(
#             const CornerTableT *connectivity, const Mesh *mesh,
#             PointsSequencer *sequencer,
#             MeshAttributeIndicesEncodingData *encoding_data)
# 
#         # Interface for TraversalObserverT
#         void OnNewFaceVisited(FaceIndex /* face */) {}
# 
#         void OnNewVertexVisited(VertexIndex vertex, CornerIndex corner)
# 
# 
###


# points_sequencer.h
# namespace draco {
# Class for generating a sequence of point ids that can be used to encode
# or decode attribute values in a specific order.
# See sequential_attribute_encoders/decoders_controller.h for more details.
cdef extern from "draco/compression/attributes/points_sequencer.h" namespace "draco" nogil:
    cdef cppclass PointsSequencer:
        PointsSequencer()
#         virtual ~PointsSequencer() = default;
# 
#         # Fills the |out_point_ids| with the generated sequence of point ids.
#         bool GenerateSequence(std::vector<PointIndex> *out_point_ids)
# 
#         # Appends a point to the sequence.
#         void AddPointId(PointIndex point_id)
# 
#         # Sets the correct mapping between point ids and value ids. I.e., the inverse
#         # of the |out_point_ids|. In general, |out_point_ids_| does not contain
#         # sufficient information to compute the inverse map, because not all point
#         # ids are necessarily contained within the map.
#         # Must be implemented for sequencers that are used by attribute decoders.
#         virtual bool UpdatePointToAttributeIndexMapping(PointAttribute * /* attr */)
# 
# 
###

# mesh_traversal_sequencer.h
# namespace draco {
#  Sequencer that generates point sequence in an order given by a deterministic
#  traversal on the mesh surface. Note that all attributes encoded with this
#  sequence must share the same connectivity.
# template <class TraverserT>
# class MeshTraversalSequencer : public PointsSequencer {
# cdef extern from "draco/compression/attributes/mesh_traversal_sequencer.h" namespace "draco" nogil:
#     cdef cppclass MeshTraversalSequencer(PointsSequencer):
#         MeshTraversalSequencer(const Mesh *mesh, const MeshAttributeIndicesEncodingData *encoding_data)
#         void SetTraverser(const TraverserT &t)
# 
#         # Function that can be used to set an order in which the mesh corners should
#         # be processed. This is an optional flag used usually only by the encoder
#         # to match the same corner order that is going to be used by the decoder.
#         # Note that |corner_order| should contain only one corner per face (it can
#         # have all corners but only the first encountered corner for each face is
#         # going to be used to start a traversal). If the corner order is not set, the
#         # corners are processed sequentially based on their ids.
#         void SetCornerOrder(const std::vector<CornerIndex> &corner_order)
# 
#         bool UpdatePointToAttributeIndexMapping(PointAttribute *attribute)
# 
# 
###

# normal_compression_utils.h
# namespace draco
# class OctahedronToolBox {
cdef extern from "draco/compression/attributes/normal_compression_utils.h" namespace "draco" nogil:
    cdef cppclass OctahedronToolBox:
        OctahedronToolBox()
#         OctahedronToolBox(int32_t q)
# 
#         void SetQuantizationBits(int32_t q)
#         bool IsInitialized() const
# 
#         # Convert all edge points in the top left and bottom right quadrants to
#         # their corresponding position in the bottom left and top right quadrants.
#         # Convert all corner edge points to the top right corner.
#         inline void CanonicalizeOctahedralCoords(int32_t s, int32_t t, int32_t *out_s, int32_t *out_t)
# 
#         # Converts an integer vector to octahedral coordinates.
#         # Precondition: |int_vec| abs sum must equal center value.
#         inline void IntegerVectorToQuantizedOctahedralCoords(const int32_t *int_vec,
#                                                                 int32_t *out_s,
#                                                                 int32_t *out_t)
# 
#         template <class T>
#         void FloatVectorToQuantizedOctahedralCoords(const T *vector, int32_t *out_s, int32_t *out_t)
# 
#         # Normalize |vec| such that its abs sum is equal to the center value;
#         template <class T>
#         void CanonicalizeIntegerVector(T *vec) const
# 
#         template <typename T>
#         void OctaherdalCoordsToUnitVector(T in_s, T in_t, T *out_vector) const
#   
#         template <typename T>
#         void QuantizedOctaherdalCoordsToUnitVector(int32_t in_s, int32_t in_t, T *out_vector)
# 
#         # |s| and |t| are expected to be signed values.
#         inline bool IsInDiamond(const int32_t &s, const int32_t &t)
# 
#         void InvertDiamond(int32_t *s, int32_t *t)
# 
#         void InvertDirection(int32_t *s, int32_t *t)
# 
#         # For correction values.
#         int32_t MakePositive(int32_t x)
# 
#         int32_t quantization_bits()
#         int32_t max_quantized_value()
#         int32_t max_value()
#         int32_t center_value()
# 
# 
###


# sequential_attribute_decoder.h
# namespace draco {
# A base class for decoding attribute values encoded by the
# SequentialAttributeEncoder.
# class SequentialAttributeDecoder {
cdef extern from "draco/compression/attributes/sequential_attribute_decoder.h" namespace "draco" nogil:
    cdef cppclass SequentialAttributeDecoder:
        SequentialAttributeDecoder()
#         # virtual ~SequentialAttributeDecoder() = default;
#         # virtual bool Initialize(PointCloudDecoder *decoder, int attribute_id);
# 
#         # Initialization for a specific attribute. This can be used mostly for
#         # standalone decoding of an attribute without an PointCloudDecoder.
#         virtual bool InitializeStandalone(PointAttribute *attribute);
# 
#         # Performs lossless decoding of the portable attribute data.
#         virtual bool DecodePortableAttribute(const std::vector<PointIndex> &point_ids,
#                                              DecoderBuffer *in_buffer);
# 
#         # Decodes any data needed to revert portable transform of the decoded attribute.
#         virtual bool DecodeDataNeededByPortableTransform(
#                         const std::vector<PointIndex> &point_ids, DecoderBuffer *in_buffer);
# 
#         # Reverts transformation performed by encoder in
#         # SequentialAttributeEncoder::TransformAttributeToPortableFormat() method.
#         virtual bool TransformAttributeToOriginalFormat(const std::vector<PointIndex> &point_ids);
# 
#         const PointAttribute *GetPortableAttribute();
# 
#         const PointAttribute *attribute()
#         PointAttribute *attribute()
#         int attribute_id()
#         PointCloudDecoder *decoder()
# 
# 
###

# sequential_attribute_decoders_controller.h
# namespace draco {
# A basic implementation of an attribute decoder that decodes data encoded by
# the SequentialAttributeEncodersController class. The
# SequentialAttributeDecodersController creates a single
# AttributeIndexedValuesDecoder for each of the decoded attribute, where the
# type of the values decoder is determined by the unique identifier that was
# encoded by the encoder.
# class SequentialAttributeDecodersController : public AttributesDecoder {
# cdef extern from "draco/compression/attributes/sequential_attribute_decoders_controller.h" namespace "draco" nogil:
#     cdef cppclass SequentialAttributeDecodersController(AttributesDecoder):
#         SequentialAttributeDecodersController(std::unique_ptr<PointsSequencer> sequencer)
# 
#         bool DecodeAttributesDecoderData(DecoderBuffer *buffer) override;
#         bool DecodeAttributes(DecoderBuffer *buffer) override;
#         const PointAttribute *GetPortableAttribute(int32_t point_attribute_id)
# 
# 
###

# sequential_attribute_encoder.h
# namespace draco {
# A base class for encoding attribute values of a single attribute using a
# given sequence of point ids. The default implementation encodes all attribute
# values directly to the buffer but derived classes can perform any custom
# encoding (such as quantization) by overriding the EncodeValues() method.
# class SequentialAttributeEncoder {
cdef extern from "draco/compression/attributes/sequential_attribute_encoder.h" namespace "draco" nogil:
    cdef cppclass SequentialAttributeEncoder:
        SequentialAttributeEncoder();
#         virtual ~SequentialAttributeEncoder() = default;
# 
#         # Method that can be used for custom initialization of an attribute encoder,
#         # such as creation of prediction schemes and initialization of attribute
#         # encoder dependencies.
#         # |encoder| is the parent PointCloudEncoder,
#         # |attribute_id| is the id of the attribute that is being encoded by this
#         # encoder.
#         # This method is automatically called by the PointCloudEncoder after all
#         # attribute encoders are created and it should not be called explicitly from
#         # other places.
#         virtual bool Initialize(PointCloudEncoder *encoder, int attribute_id);
# 
#         # Initialization for a specific attribute. This can be used mostly for
#         # standalone encoding of an attribute without an PointCloudEncoder.
#         virtual bool InitializeStandalone(PointAttribute *attribute);
# 
#         # Transforms attribute data into format that is going to be encoded
#         # losslessly. The transform itself can be lossy.
#         virtual bool TransformAttributeToPortableFormat(const std::vector<PointIndex> &point_ids);
# 
#         # Performs lossless encoding of the transformed attribute data.
#         virtual bool EncodePortableAttribute(const std::vector<PointIndex> &point_ids, EncoderBuffer *out_buffer);
# 
#         # Encodes any data related to the portable attribute transform.
#         virtual bool EncodeDataNeededByPortableTransform(EncoderBuffer *out_buffer);
# 
#         virtual bool IsLossyEncoder()
# 
#         int NumParentAttributes()
#         int GetParentAttributeId(int i)
# 
#         const PointAttribute *GetPortableAttribute()
# 
#         # Called when this attribute encoder becomes a parent encoder of another encoder.
#         void MarkParentAttribute();
# 
#         virtual uint8_t GetUniqueId()
# 
#         const PointAttribute *attribute()
#         int attribute_id()
#         PointCloudEncoder *encoder()
# 
# 
###

# sequential_attribute_encoders_controller.h
# namespace draco {
# A basic implementation of an attribute encoder that can be used to encode
# an arbitrary set of attributes. The encoder creates a sequential attribute
# encoder for each encoded attribute (see sequential_attribute_encoder.h) and
# then it encodes all attribute values in an order defined by a point sequence
# generated in the GeneratePointSequence() method. The default implementation
# generates a linear sequence of all points, but derived classes can generate
# any custom sequence.
# class SequentialAttributeEncodersController : public AttributesEncoder {
# cdef extern from "draco/compression/attributes/sequential_attribute_encoders_controller.h" namespace "draco" nogil:
#     cdef cppclass SequentialAttributeEncodersController(AttributesEncoder):
#         explicit SequentialAttributeEncodersController(std::unique_ptr<PointsSequencer> sequencer);
#         SequentialAttributeEncodersController(std::unique_ptr<PointsSequencer> sequencer, int point_attrib_id);
# 
#         bool Initialize(PointCloudEncoder *encoder, const PointCloud *pc) override;
#         bool EncodeAttributesEncoderData(EncoderBuffer *out_buffer) override;
#         bool EncodeAttributes(EncoderBuffer *buffer) override;
#         uint8_t GetUniqueId()
# 
#         int NumParentAttributes(int32_t point_attribute_id)
# 
#         int GetParentAttributeId(int32_t point_attribute_id, int32_t parent_i)
# 
#         bool MarkParentAttribute(int32_t point_attribute_id)
# 
#         const PointAttribute *GetPortableAttribute(int32_t point_attribute_id)
# 
# 
###

# sequential_integer_attribute_decoder.h
# namespace draco {
# Decoder for attributes encoded with the SequentialIntegerAttributeEncoder.
# class SequentialIntegerAttributeDecoder : public SequentialAttributeDecoder {
cdef extern from "draco/compression/attributes/sequential_integer_attribute_decoder.h" namespace "draco" nogil:
    cdef cppclass SequentialIntegerAttributeDecoder(SequentialAttributeDecoder):
        SequentialIntegerAttributeDecoder()
#         bool Initialize(PointCloudDecoder *decoder, int attribute_id) override;
# 
#         bool TransformAttributeToOriginalFormat(const std::vector<PointIndex> &point_ids) override;
# 
# 
###

# sequential_integer_attribute_encoder.h
# namespace draco {
# Attribute encoder designed for lossless encoding of integer attributes. The
# attribute values can be pre-processed by a prediction scheme and compressed
# with a built-in entropy coder.
# class SequentialIntegerAttributeEncoder : public SequentialAttributeEncoder {
cdef extern from "draco/compression/attributes/sequential_integer_attribute_encoder.h" namespace "draco" nogil:
    cdef cppclass SequentialIntegerAttributeEncoder(SequentialAttributeEncoder):
        SequentialIntegerAttributeEncoder()
#         uint8_t GetUniqueId()
# 
#         bool Initialize(PointCloudEncoder *encoder, int attribute_id) override;
#         bool TransformAttributeToPortableFormat(const std::vector<PointIndex> &point_ids) override;
# 
# 
###

# sequential_normal_attribute_decoder.h
# namespace draco {
# Decoder for attributes encoded with SequentialNormalAttributeEncoder.
# class SequentialNormalAttributeDecoder : public SequentialIntegerAttributeDecoder {
cdef extern from "draco/compression/attributes/sequential_normal_attribute_decoder.h" namespace "draco" nogil:
    cdef cppclass SequentialNormalAttributeDecoder(SequentialIntegerAttributeDecoder):
        SequentialNormalAttributeDecoder();
#         bool Initialize(PointCloudDecoder *decoder, int attribute_id) override;
# 
# 
###

# sequential_normal_attribute_encoder.h
# namespace draco {
# Class for encoding normal vectors using an octahedral encoding, see Cigolle
# et al.'14 “A Survey of Efficient Representations for Independent Unit
# Vectors”. Compared to the basic quantization encoder, this encoder results
# in a better compression rate under the same accuracy settings. Note that this
# encoder doesn't preserve the lengths of input vectors, therefore it will not
# work correctly when the input values are not normalized.
# class SequentialNormalAttributeEncoder : public SequentialIntegerAttributeEncoder {
# cdef extern from "draco/compression/attributes/sequential_normal_attribute_encoder.h" namespace "draco" nogil:
#     cdef cppclass SequentialNormalAttributeEncoder(SequentialIntegerAttributeEncoder):
#         uint8_t GetUniqueId() const override
#         bool IsLossyEncoder() const override
#         bool EncodeDataNeededByPortableTransform(EncoderBuffer *out_buffer) override;
# 
# 
###

# sequential_quantization_attribute_decoder.h
# namespace draco {
# Decoder for attribute values encoded with the
# SequentialQuantizationAttributeEncoder.
# class SequentialQuantizationAttributeDecoder : public SequentialIntegerAttributeDecoder {
cdef extern from "draco/compression/attributes/sequential_quantization_attribute_decoder.h" namespace "draco" nogil:
    cdef cppclass SequentialQuantizationAttributeDecoder:
        SequentialQuantizationAttributeDecoder();
#         bool Initialize(PointCloudDecoder *decoder, int attribute_id) override;
# 
# 
###

# sequential_quantization_attribute_encoder.h
# namespace draco {
# class MeshEncoder;
# Attribute encoder that quantizes floating point attribute values. The
# quantized values can be optionally compressed using an entropy coding.
# class SequentialQuantizationAttributeEncoder : public SequentialIntegerAttributeEncoder {
cdef extern from "draco/compression/attributes/sequential_quantization_attribute_encoder.h" namespace "draco" nogil:
    cdef cppclass SequentialQuantizationAttributeEncoder(SequentialIntegerAttributeEncoder):
        SequentialQuantizationAttributeEncoder();
#         uint8_t GetUniqueId() const override
#         bool Initialize(PointCloudEncoder *encoder, int attribute_id) override;
#         bool IsLossyEncoder() const override { return true; }
#         bool EncodeDataNeededByPortableTransform(EncoderBuffer *out_buffer) override;
# 
# 
###

###
# compression/attributes/prediction_schemes
# mesh_prediction_scheme_constrained_multi_parallelogram_decoder.h
# mesh_prediction_scheme_constrained_multi_parallelogram_encoder.h
# mesh_prediction_scheme_constrained_multi_parallelogram_shared.h
# mesh_prediction_scheme_data.h
# mesh_prediction_scheme_decoder.h
# mesh_prediction_scheme_encoder.h
# mesh_prediction_scheme_geometric_normal_decoder.h
# mesh_prediction_scheme_geometric_normal_encoder.h
# mesh_prediction_scheme_geometric_normal_predictor_area.h
# mesh_prediction_scheme_geometric_normal_predictor_base.h
# mesh_prediction_scheme_multi_parallelogram_decoder.h
# mesh_prediction_scheme_multi_parallelogram_encoder.h
# mesh_prediction_scheme_parallelogram_decoder.h
# mesh_prediction_scheme_parallelogram_encoder.h
# mesh_prediction_scheme_parallelogram_shared.h
# mesh_prediction_scheme_tex_coords_decoder.h
# mesh_prediction_scheme_tex_coords_encoder.h
# mesh_prediction_scheme_tex_coords_portable_decoder.h
# mesh_prediction_scheme_tex_coords_portable_encoder.h
# mesh_prediction_scheme_tex_coords_portable_predictor.h
# prediction_scheme_decoder.h
# prediction_scheme_decoder_factory.h
# prediction_scheme_decoder_interface.h
# prediction_scheme_decoding_transform.h
# prediction_scheme_delta_decoder.h
# prediction_scheme_delta_encoder.h
# prediction_scheme_encoder.h
# prediction_scheme_encoder_factory.h
# prediction_scheme_encoder_interface.h
# prediction_scheme_encoding_transform.h
# prediction_scheme_factory.h
# prediction_scheme_interface.h
# prediction_scheme_normal_octahedron_canonicalized_decoding_transform.h
# prediction_scheme_normal_octahedron_canonicalized_encoding_transform.h
# prediction_scheme_normal_octahedron_canonicalized_transform_base.h
# prediction_scheme_normal_octahedron_decoding_transform.h
# prediction_scheme_normal_octahedron_encoding_transform.h
# prediction_scheme_normal_octahedron_transform_base.h
# prediction_scheme_wrap_decoding_transform.h
# prediction_scheme_wrap_encoding_transform.h
# prediction_scheme_wrap_transform_base.h
###


# compression/config/encoder_options.h
# EncoderOptions allow users to specify so called feature options that are used
# to inform the encoder which encoding features can be used (i.e. which
# features are going to be available to the decoder).
# template <typename AttributeKeyT>
# class EncoderOptionsBase : public DracoOptions<AttributeKeyT> {
# cdef extern from "draco/compression/config/encoder_options.h" namespace "draco" nogil:
#     cdef cppclass EncoderOptionsBase(DracoOptions[AttributeKeyT]):
#     # cdef cppclass EncoderOptionsBase(EncoderBaseOptions_t):
#         # EncoderOptionsBase()
#         # private
#         # EncoderOptionsBase();
#         # static EncoderOptionsBase CreateDefaultOptions()
#         # static EncoderOptionsBase CreateEmptyOptions()
# 
#         # Returns speed options with default value of 5.
#         int GetEncodingSpeed() const
#         int GetDecodingSpeed() const
# 
#         # Returns the maximum speed for both encoding/decoding.
#         int GetSpeed() const
#         void SetSpeed(int encoding_speed, int decoding_speed)
# 
#         # Sets a given feature as supported or unsupported by the target decoder.
#         # Encoder will always use only supported features when encoding the input geometry.
#         void SetSupportedFeature(const string &name, bool supported)
#         bool IsFeatureSupported(const string &name)
#         void SetFeatureOptions(const Options &options)
#         const Options& GetFeaturelOptions()
# 
# 
# # Encoder options where attributes are identified by their attribute id.
# # Used to set options that are specific to a given geometry.
# # typedef EncoderOptionsBase<int32_t> EncoderOptions;
# ctypedef EncoderOptionsBase[int32_t] EncoderOptions
# 
# 
###

# compression/config/encoder_options.h
# namespace draco
# Class encapsuling options used by PointCloudEncoder and its derived classes.
# The encoder can be controller through three different options:
#   1. Global options
#   2. Per attribute options - i.e., options specific to a given attribute.
#   3. Feature options - options determining the available set of features on
#                        the target decoder.
# Please refer to mesh/encode.h for helper functions that can be used to
# initialize the options for various use cases.
# class EncoderOptions
cdef extern from "draco/compression/config/encoder_options.h" namespace "draco" nogil:
    cdef cppclass EncoderOptions:
        EncoderOptions()
        # static EncoderOptions CreateDefaultOptions()
        # @staticmethod
        # EncoderOptions CreateDefaultOptions()

        # Sets the global options that serve to control the overal behavior of an
        # encoder as well as a fallback for attribute options if they are not set.
        # void SetGlobalOptions(const core.Options &o)
        # core.Options *GetGlobalOptions()
        # 
        # Sets options for a specific attribute in a target PointCloud.
        # void SetAttributeOptions(int32_t att_id, const core.Options &o)
        # core.Options *GetAttributeOptions(int32_t att_id)
        # 
        # Sets options for all attributes of a given type in the target point cloud.
        # void SetNamedAttributeOptions(const PointCloud &pc, GeometryAttribute::Type att_type, const core.Options &o)
        # void SetNamedAttributeOptions(const point_cloud.PointCloud &pc, attribute.Type2 att_type, const core.Options &o)

        # core.Options *GetNamedAttributeOptions(const PointCloud &pc, GeometryAttribute::Type att_type)
        # core.Options *GetNamedAttributeOptions(const point_cloud.PointCloud &pc, attribute.Type2 att_type)

        # Sets the list of features enabled by the encoder.
        # void SetFeatureOptions(const core.Options &o)
        void SetGlobalInt(const string &name, int val)
        void SetGlobalBool(const string &name, bool val)
        void SetGlobalString(const string &name, const string &val)

        int GetGlobalInt(const string &name, int default_val)
        bool GetGlobalBool(const string &name, bool default_val)
        string GetGlobalString(const string &name, const string &default_val)

        void SetAttributeInt(int32_t att_id, const string &name, int val)
        void SetAttributeBool(int32_t att_id, const string &name, bool val)
        void SetAttributeString(int32_t att_id, const string &name, const string &val)

        # Get an option for a specific attribute. If the option is not found in an
        # attribute specific storage, the implementation will return a global option
        # of the given name (if available).

        # int GetAttributeInt(int32_t att_id, const string &name, int default_val) const;
        # bool GetAttributeBool(int32_t att_id, const string &name, bool default_val) const;
        # string GetAttributeString(int32_t att_id, const string &name, const string &default_val) const;
        int GetAttributeInt(int32_t att_id, const string &name, int default_val)
        bool GetAttributeBool(int32_t att_id, const string &name, bool default_val)
        string GetAttributeString(int32_t att_id, const string &name, const string &default_val)

        # Returns speed options with default value of 5.
        int GetEncodingSpeed()
        int GetDecodingSpeed()

        # Returns the maximum speed for both encoding/decoding.
        int GetSpeed()

        # Sets a given feature as supported or unsupported by the target decoder.
        # Encoder will always use only supported features when encoding the input geometry.
        # void SetSupportedFeature(const string &name, bool supported)
        void SetSupportedFeature(const string &name, bool supported)

        # bool IsFeatureSupported(const string &name) const
        bool IsFeatureSupported(const string &name)


###

# compression/mesh
# mesh_decoder.h
# mesh_decoder_helpers.h
# mesh_edgebreaker_decoder.h
# mesh_edgebreaker_decoder_impl.h
# mesh_edgebreaker_decoder_impl_interface.h
# mesh_edgebreaker_encoder.h
# mesh_edgebreaker_encoder_impl.h
# mesh_edgebreaker_encoder_impl_interface.h
# mesh_edgebreaker_shared.h
# mesh_edgebreaker_traversal_decoder.h
# mesh_edgebreaker_traversal_encoder.h
# mesh_edgebreaker_traversal_predictive_decoder.h
# mesh_edgebreaker_traversal_predictive_encoder.h
# mesh_edgebreaker_traversal_valence_decoder.h
# mesh_edgebreaker_traversal_valence_encoder.h
# mesh_encoder.h
# mesh_encoder_helpers.h
# mesh_sequential_decoder.h
# mesh_sequential_encoder.h
###

# compression/point_cloud
# point_cloud_decoder.h
cdef extern from "draco/compression/point_cloud/point_cloud_decoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudDecoder:
        PointCloudDecoder();
        # virtual ~PointCloudDecoder() = default;
        # virtual EncodedGeometryType GetGeometryType() const
        # 
        # Decodes a Draco header int other provided |out_header|.
        # Returns false on error.
        # static Status DecodeHeader(DecoderBuffer *buffer, DracoHeader *out_header);
        # 
        # The main entry point for point cloud decoding.
        # Status Decode(const DecoderOptions &options, DecoderBuffer *in_buffer, PointCloud *out_point_cloud);
        # core.Status Decode(const DecoderOptions &options, DecoderBuffer *in_buffer, PointCloud *out_point_cloud);
        core.Status Decode(const DecoderOptions &options, core.DecoderBuffer* in_buffer, point_cloud.PointCloud* out_point_cloud)

        # void SetAttributesDecoder(int att_decoder_id, std::unique_ptr<AttributesDecoderInterface> decoder)

        # Returns an attribute containing decoded data in their portable form that
        # is guaranteed to be the same for both encoder and decoder. I.e., it returns
        # an attribute before it was transformed back into its final form which may
        # be slightly different (non-portable) across platforms. For example, for
        # attributes encoded with quantization, this method returns an attribute
        # that contains the quantized values (before the dequantization step).
        const attribute.PointAttribute* GetPortableAttribute(int32_t point_attribute_id)

        uint16_t bitstream_version()

        # const AttributesDecoderInterface* attributes_decoder(int dec_id)

        # int32_t num_attributes_decoders() const
        int32_t num_attributes_decoders()

        # Get a mutable pointer to the decoded point cloud. This is intended to be
        # used mostly by other decoder subsystems.
        point_cloud.PointCloud* point_cloud()
        # const point_cloud.PointCloud* point_cloud()

        core.DecoderBuffer* buffer()
        const DecoderOptions* options() const


###

# point_cloud_encoder.h
# Abstract base class for all point cloud and mesh encoders. It provides a
# basic functionality that's shared between different encoders.
cdef extern from "draco/compression/point_cloud/point_cloud_decoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudEncoder:
        PointCloudEncoder();
        # virtual ~PointCloudEncoder() = default;

        # Sets the point cloud that is going be encoded. Must be called before the
        # Encode() method.
        void SetPointCloud(const point_cloud.PointCloud &pc)

        # The main entry point that encodes provided point cloud.
        core.Status Encode(const EncoderOptions &options, core.EncoderBuffer *out_buffer)

        # virtual EncodedGeometryType GetGeometryType() const

        # Returns the unique identifier of the encoding method (such as Edgebreaker for mesh compression).
        # virtual uint8_t GetEncodingMethod() const = 0;

        int num_attributes_encoders()
        AttributesEncoder* attributes_encoder(int i)

        # Adds a new attribute encoder, returning its id.
        # int AddAttributesEncoder(std::unique_ptr<AttributesEncoder> att_enc)
        # NG
        # int AddAttributesEncoder(unique_ptr[attribute.AttributesEncoder] att_enc)

        # Marks one attribute as a parent of another attribute. Must be called after
        # all attribute encoders are created (usually in the AttributeEncoder::Initialize() method).
        bool MarkParentAttribute(int32_t parent_att_id)

        # Returns an attribute containing portable version of the attribute data that
        # is guaranteed to be encoded losslessly. This attribute can be used safely
        # as predictor for other attributes.
        # const PointAttribute *GetPortableAttribute(int32_t point_attribute_id);
        const attribute.PointAttribute* GetPortableAttribute(int32_t point_attribute_id)

        core.EncoderBuffer *buffer()
        # const EncoderOptions *options() const
        # const PointCloud *point_cloud() const
        const EncoderOptions* options()
        const point_cloud.PointCloud* point_cloud()


###

# point_cloud_kd_tree_decoder.h
# Decodes PointCloud encoded with the PointCloudKdTreeEncoder.
cdef extern from "draco/compression/point_cloud/point_cloud_kd_tree_decoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudKdTreeDecoder(PointCloudDecoder):
        PointCloudKdTreeDecoder()
        # protected:
        # bool DecodeGeometryData() override;
        # bool CreateAttributesDecoder(int32_t att_decoder_id) override;


###

# point_cloud_kd_tree_encoder.h
cdef extern from "draco/compression/point_cloud/point_cloud_kd_tree_encoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudKdTreeEncoder(PointCloudEncoder):
        PointCloudKdTreeEncoder()
        # public:
        # uint8_t GetEncodingMethod() const override
        # protected:
        # bool EncodeGeometryData() override;
        # bool GenerateAttributesEncoder(int32_t att_id) override;


###

# point_cloud_sequential_decoder.h
cdef extern from "draco/compression/point_cloud/point_cloud_sequential_decoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudSequentialDecoder(PointCloudDecoder):
        PointCloudSequentialDecoder()


###

# point_cloud_sequential_encoder.h
cdef extern from "draco/compression/point_cloud/point_cloud_sequential_encoder.h" namespace "draco" nogil:
    cdef cppclass PointCloudSequentialEncoder(PointCloudEncoder):
        PointCloudSequentialEncoder()
        uint8_t GetEncodingMethod()


###

###
# compression/point_cloud/algorithms
# dynamic_integer_points_kd_tree_decoder.h
# dynamic_integer_points_kd_tree_encoder.h
# float_points_tree_decoder.h
# float_points_tree_encoder.h
# integer_points_kd_tree_decoder.h
# integer_points_kd_tree_encoder.h
# point_cloud_compression_method.h
# point_cloud_types.h
# quantize_points_3.h
# queuing_policy.h
###


# compression/decode.h
# namespace draco
# Returns the geometry type encoded in the input |in_buffer|.
# The return value is one of POINT_CLOUD, MESH or INVALID_GEOMETRY in case
# the input data is invalid.
# The decoded geometry type can be used to choose an appropriate decoding
# function for a given geometry type (see below).
cdef extern from "draco/compression/decode.h" namespace "draco::Decoder" nogil:
    # Returns the geometry type encoded in the input |in_buffer|.
    # The return value is one of POINT_CLOUD, MESH or INVALID_GEOMETRY in case
    # the input data is invalid.
    # The decoded geometry type can be used to choose an appropriate decoding
    # function for a given geometry type (see below).
    # EncodedGeometryType GetEncodedGeometryType(core.DecoderBuffer *in_buffer)
    core.StatusOr[EncodedGeometryType] GetEncodedGeometryType(core.DecoderBuffer *in_buffer)

    # Decodes point cloud from the provided buffer. The buffer must be filled
    # with data that was encoded with either the EncodePointCloudToBuffer or
    # EncodeMeshToBuffer methods in encode.h. In case the input buffer contains
    # mesh, the returned instance can be down-casted to Mesh.
    # StatusOr<std::unique_ptr<PointCloud>> DecodePointCloudFromBuffer(DecoderBuffer *in_buffer);
    core.StatusOr[point_cloud.PointCloud] DecodePointCloudFromBuffer(core.DecoderBuffer *in_buffer)

    # Decodes a triangular mesh from the provided buffer. The mesh must be filled
    # with data that was encoded using the EncodeMeshToBuffer method in encode.h.
    # The function will return nullptr in case the input is invalid or if it was
    # encoded with the EncodePointCloudToBuffer method.
    # StatusOr<std::unique_ptr<Mesh>> DecodeMeshFromBuffer(DecoderBuffer *in_buffer);
    core.StatusOr[mesh.Mesh] DecodeMeshFromBuffer(core.DecoderBuffer *in_buffer)

    # Decodes the buffer into a provided geometry. If the geometry is
    # incompatible with the encoded data. For example, when |out_geometry| is
    # draco::Mesh while the data contains a point cloud, the function will return an error status.
    core.Status DecodeBufferToGeometry(core.DecoderBuffer *in_buffer, point_cloud.PointCloud *out_geometry)
    core.Status DecodeBufferToGeometry(core.DecoderBuffer *in_buffer, mesh.Mesh *out_geometry)

    # When set, the decoder is going to skip attribute transform for a given
    # attribute type. For example for quantized attributes, the decoder would
    # skip the dequantization step and the returned geometry would contain an
    # attribute with quantized values. The attribute would also contain an
    # instance of AttributeTransform class that is used to describe the skipped
    # transform, including all parameters that are needed to perform the transform manually.
    # void SetSkipAttributeTransform(GeometryAttribute::Type att_type)
    void SetSkipAttributeTransform(attribute.Type2 att_type)

    # Returns the options instance used by the decoder that can be used by users
    # to control the decoding process.
    DecoderOptions *options()


###

# compression/decode.h
# namespace draco
cdef extern from "draco/compression/decode.h" namespace "draco" nogil:
    cdef cppclass Decoder:
        Decoder()

        # """
        # Decodes point cloud from the provided buffer. The buffer must be filled
        # with data that was encoded with either the EncodePointCloudToBuffer or
        # EncodeMeshToBuffer methods in encode.h. In case the input buffer contains
        # mesh, the returned instance can be down-casted to Mesh.
        # """
        core.StatusOr[unique_ptr[point_cloud.PointCloud]] DecodePointCloudFromBuffer(core.DecoderBuffer *in_buffer)
        # unique_ptr[point_cloud.PointCloud] DecodePointCloudFromBuffer(core.DecoderBuffer *in_buffer)
        
        # """
        # Decodes a triangular mesh from the provided buffer. The mesh must be filled
        # with data that was encoded using the EncodeMeshToBuffer method in encode.h.
        # The function will return nullptr in case the input is invalid or if it was
        # encoded with the EncodePointCloudToBuffer method.
        # """
        core.StatusOr[unique_ptr[mesh.Mesh]] DecodeMeshFromBuffer(core.DecoderBuffer *in_buffer)
        # unique_ptr[mesh.Mesh] DecodeMeshFromBuffer(core.DecoderBuffer *in_buffer)


        # Decodes the buffer into a provided geometry. If the geometry is
        # incompatible with the encoded data. For example, when |out_geometry| is
        # draco::Mesh while the data contains a point cloud, the function will return
        # an error status.
        # Status DecodeBufferToGeometry(DecoderBuffer *in_buffer, PointCloud *out_geometry);
        # Status DecodeBufferToGeometry(DecoderBuffer *in_buffer, Mesh *out_geometry);
        core.Status DecodeBufferToGeometry(core.DecoderBuffer *in_buffer, point_cloud.PointCloud *out_geometry)
        core.Status DecodeBufferToGeometry(core.DecoderBuffer *in_buffer, mesh.Mesh *out_geometry)


        # When set, the decoder is going to skip attribute transform for a given
        # attribute type. For example for quantized attributes, the decoder would
        # skip the dequantization step and the returned geometry would contain an
        # attribute with quantized values. The attribute would also contain an
        # instance of AttributeTransform class that is used to describe the skipped
        # transform, including all parameters that are needed to perform the
        # transform manually.
        # void SetSkipAttributeTransform(GeometryAttribute::Type att_type)
        void SetSkipAttributeTransform(attribute.Type2 att_type)


        # Returns the options instance used by the decoder that can be used by users
        # to control the decoding process.
        DecoderOptions* options()


###


# compression/encode.h
# namespace draco
# Encodes point cloud to the provided buffer. |options| can be used to control
# the encoding of point cloud. See functions below that can be used to generate
# valid options for the encoder covering the most usual use cases.
cdef extern from "draco/compression/encode.h" namespace "draco" nogil:
    cdef cppclass Encoder:
        core.Status EncodePointCloudToBuffer(const point_cloud.PointCloud &pc, core.EncoderBuffer *out_buffer)

        # Encodes mesh to the provided buffer.
        core.Status EncodeMeshToBuffer(const mesh.Mesh &m, core.EncoderBuffer *out_buffer)

        # Creates default encoding options that contain a valid set of features that
        # the encoder can use. Otherwise all options are left unitialized which results
        # in a lossless compression.
        EncoderOptions CreateDefaultEncoderOptions()
        
        # Sets the desired encoding method for a given geometry. By default, encoding
        # method is selected based on the properties of the input geometry and based on
        # the other options selected in the used EncoderOptions (such as desired
        # encoding and decoding speed). This function should be called only when a
        # specific method is required.
        # |encoding_method| can be one of the following as defined in
        # compression/config/compression_shared.h :
        #   POINT_CLOUD_SEQUENTIAL_ENCODING
        #   POINT_CLOUD_KD_TREE_ENCODING
        #   MESH_SEQUENTIAL_ENCODING
        #   MESH_EDGEBREAKER_ENCODING
        # If the selected method cannot be used for the given input, the subsequent
        # call of EncodePointCloudToBuffer or EncodeMeshToBuffer is going to fail.
        void SetEncodingMethod(int encoding_method)
        
        # Sets the desired encoding and decoding speed for the given options.
        #  0 = slowest speed, but the best compression.
        # 10 = fastest, but the worst compression.
        # -1 = undefined.
        # Note that both speed options affect the encoder choice of used methods and
        # algorithms. For example, a requirement for fast decoding may prevent the
        # encoder from using the best compression methods even if the encoding speed is
        # set to 0. In general, the faster of the two options limits the choice of
        # features that can be used by the encoder. Additionally, setting
        # |decoding_speed| to be faster than the |encoding_speed| may allow the encoder
        # to choose the optimal method out of the available features for the given
        # |decoding_speed|.
        void SetSpeedOptions(int encoding_speed, int decoding_speed)
        
        # Sets the quantization compression options for a named attribute. The
        # attribute values will be quantized in a box defined by the maximum extent of
        # the attribute values. I.e., the actual precision of this option depends on
        # the scale of the attribute values.
        # void SetNamedAttributeQuantization(EncoderOptions *options, const point_cloud.PointCloud &pc, attribute.Type2 type, int quantization_bits)
        
        # Sets the above quantization directly for a specific attribute |options|.
        # void SetAttributeQuantization(GeometryAttribute::Type type, int quantization_bits)
        void SetAttributeQuantization(attribute.Type2 type, int quantization_bits)


###

cdef extern from "draco/compression/encode.h" namespace "draco::Encoder" nogil:
    EncoderOptions CreateDefaultEncoderOptions()

# cdef extern from "draco/compression/encode.h" namespace "draco::Encoder" nogil:
#     void SetNamedAttributeQuantization(EncoderOptions *options, const point_cloud.PointCloud &pc, attribute.Type2 type, int quantization_bits)


###



###

# compression/encode.h
# namespace draco
# Enables/disables built in entropy coding of attribute values. Disabling this
# option may be useful to improve the performance when third party compression
# is used on top of the Draco compression.
# Default: [true].
# cdef extern from "draco/compression/encode.h" namespace "draco" nogil:
#     void SetUseBuiltInAttributeCompression(EncoderOptions *options, bool enabled)


###


# Sets the desired prediction method for a given attribute. By default,
# prediction scheme is selected automatically by the encoder using other
# provided options (such as speed) and input geometry type (mesh, point cloud).
# This function should be called only when a specific prediction is prefered
# (e.g., when it is known that the encoder would select a less optimal
# prediction for the given input data).
# 
# |prediction_scheme_method| should be one of the entries defined in
# compression/config/compression_shared.h :
# 
#   PREDICTION_NONE - use no prediction.
#   PREDICTION_DIFFERENCE - delta coding
#   MESH_PREDICTION_PARALLELOGRAM - parallelogram prediction for meshes.
#   MESH_PREDICTION_MULTI_PARALLELOGRAM
#      - better and more costly version of the parallelogram prediction.
#   MESH_PREDICTION_TEX_COORDS - specialized predictor for tex coordinates.
# 
# Note that in case the desired prediction cannot be used, the default
# prediction will be automatically used instead.
# cdef extern from "draco/compression/encode.h" namespace "draco" nogil:
#     cdef class Encoder:
#         # void SetNamedAttributePredictionScheme(EncoderOptions *options, const point_cloud.PointCloud &pc, GeometryAttribute::Type type, int prediction_scheme_method);
#         void SetNamedAttributePredictionScheme(EncoderOptions *options, const point_cloud.PointCloud &pc, attribute.Type2 type, int prediction_scheme_method)

cdef extern from "draco/compression/encode.h" namespace "draco::Encoder" nogil:
    void SetNamedAttributePredictionScheme(EncoderOptions *options, const point_cloud.PointCloud &pc, attribute.Type2 type, int prediction_scheme_method)


# compression/encode.h
# namespace draco
# Sets the prediction scheme directly for a specific attribute |options|.
# cdef extern from "draco/compression/encode.h" namespace "draco" nogil:
#     void SetAttributePredictionScheme(Options *options, int prediction_scheme_method);


# compression/encode_base.h
# Base class for our geometry encoder classes. |EncoderOptionsT| specifies
# options class used by the encoder. Please, see encode.h and expert_encode.h
# for more details and method descriptions.
# cdef extern from "draco/compression/encode_base.h" namespace "draco" nogil:
#     cdef cppclass EncoderBase[EncoderOptionsT]:
#         # EncoderBase() : options_(EncoderOptionsT::CreateDefaultOptions()) {}
#         EncoderBase()
#         # public:
#         # typedef EncoderOptionsT OptionsType;
#         # const EncoderOptionsT &options() const { return options_; }
#         EncoderOptionsT options()
# 
#         # protected:
#         # void Reset(const EncoderOptionsT &options) { options_ = options; }
#         # void Reset() { options_ = EncoderOptionsT::CreateDefaultOptions(); }
#         # void SetSpeedOptions(int encoding_speed, int decoding_speed)
#         # void SetEncodingMethod(int encoding_method)
# 
# 
# ctypedef EncoderBase[EncoderOptions] EncoderBaseOptions_t
###


# compression/expert_encode.h
# Advanced helper class for encoding geometry using the Draco compression
# library. Unlike the basic Encoder (encode.h), this class allows users to
# specify options for each attribute individually using provided attribute ids.
# The drawback of this encoder is that it can be used to encode only one model
# at a time, and for each new model the options need to be set again,
cdef extern from "draco/compression/expert_encode.h" namespace "draco" nogil:
    cdef cppclass ExpertEncoder(EncoderBaseOptions_t):
        # public:
        # typedef EncoderBase<EncoderOptions> Base;
        # typedef EncoderOptions OptionsType;
        # ExpertEncoder(const point_cloud.PointCloud &point_cloud)
        ExpertEncoder(const mesh.Mesh &mesh)
        # NG
        # ExpertEncoder_PointCloud "ExpertEncoder" (const point_cloud.PointCloud &point_cloud)
        # ExpertEncoder_Mesh "ExpertEncoder" (const mesh.Mesh &mesh)

        # Encodes the geometry provided in the constructor to the target buffer.
        core.Status EncodeToBuffer(core.EncoderBuffer *out_buffer)

        # Set encoder options used during the geometry encoding. Note that this call
        # overwrites any modifications to the options done with the functions below.
        # void Reset(const EncoderOptions &options);
        # void Reset();
        void Reset()
        void ResetOptions "Reset" (const EncoderOptions &options)

        # Sets the desired encoding and decoding speed for the given options.
        #  0 = slowest speed, but the best compression.
        # 10 = fastest, but the worst compression.
        # -1 = undefined.
        #
        # Note that both speed options affect the encoder choice of used methods and
        # algorithms. For example, a requirement for fast decoding may prevent the
        # encoder from using the best compression methods even if the encoding speed
        # is set to 0. In general, the faster of the two options limits the choice of
        # features that can be used by the encoder. Additionally, setting
        # |decoding_speed| to be faster than the |encoding_speed| may allow the
        # encoder to choose the optimal method out of the available features for the
        # given |decoding_speed|.
        void SetSpeedOptions(int encoding_speed, int decoding_speed)

        # Sets the quantization compression options for a specific attribute. The
        # attribute values will be quantized in a box defined by the maximum extent
        # of the attribute values. I.e., the actual precision of this option depends
        # on the scale of the attribute values.
        # void SetAttributeQuantization(int32_t attribute_id, int quantization_bits)
        void SetAttributeQuantization(int32_t attribute_id, int quantization_bits)

        # Enables/disables built in entropy coding of attribute values. Disabling
        # this option may be useful to improve the performance when third party
        # compression is used on top of the Draco compression. Default: [true].
        void SetUseBuiltInAttributeCompression(bool enabled)

        # Sets the desired encoding method for a given geometry. By default, encoding
        # method is selected based on the properties of the input geometry and based
        # on the other options selected in the used EncoderOptions (such as desired
        # encoding and decoding speed). This function should be called only when a
        # specific method is required.
        #
        # |encoding_method| can be one of the values defined in
        # compression/config/compression_shared.h based on the type of the input
        # geometry that is going to be encoded. For point clouds, allowed entries are
        #   POINT_CLOUD_SEQUENTIAL_ENCODING
        #   POINT_CLOUD_KD_TREE_ENCODING
        #
        # For meshes the input can be
        #   MESH_SEQUENTIAL_ENCODING
        #   MESH_EDGEBREAKER_ENCODING
        #
        # If the selected method cannot be used for the given input, the subsequent
        # call of EncodePointCloudToBuffer or EncodeMeshToBuffer is going to fail.
        void SetEncodingMethod(int encoding_method)

        # Sets the desired prediction method for a given attribute. By default,
        # prediction scheme is selected automatically by the encoder using other
        # provided options (such as speed) and input geometry type (mesh, point
        # cloud). This function should be called only when a specific prediction is
        # preferred (e.g., when it is known that the encoder would select a less
        # optimal prediction for the given input data).
        #
        # |prediction_scheme_method| should be one of the entries defined in
        # compression/config/compression_shared.h :
        #
        #   PREDICTION_NONE - use no prediction.
        #   PREDICTION_DIFFERENCE - delta coding
        #   MESH_PREDICTION_PARALLELOGRAM - parallelogram prediction for meshes.
        #   MESH_PREDICTION_CONSTRAINED_PARALLELOGRAM
        #      - better and more costly version of the parallelogram prediction.
        #   MESH_PREDICTION_TEX_COORDS_PORTABLE
        #      - specialized predictor for tex coordinates.
        #   MESH_PREDICTION_GEOMETRIC_NORMAL
        #      - specialized predictor for normal coordinates.
        #
        # Note that in case the desired prediction cannot be used, the default
        # prediction will be automatically used instead.
        # void SetAttributePredictionScheme(int32_t attribute_id, int prediction_scheme_method);
        void SetAttributePredictionScheme(int32_t attribute_id, int prediction_scheme_method)


###

### attribute ###
##### prediction_schemes #####
# compression/attributes/prediction_schemes/mesh_prediction_scheme.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_constrained_multi_parallelogram.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_data.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_multi_parallelogram.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_parallelogram.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_parallelogram_shared.h
# compression/attributes/prediction_schemes/mesh_prediction_scheme_tex_coords.h
# compression/attributes/prediction_schemes/prediction_scheme.h
# compression/attributes/prediction_schemes/prediction_scheme_decoder_factory.h
# compression/attributes/prediction_schemes/prediction_scheme_difference.h
# compression/attributes/prediction_schemes/prediction_scheme_encoder_factory.h
# compression/attributes/prediction_schemes/prediction_scheme_factory.h
# compression/attributes/prediction_schemes/prediction_scheme_interface.h
# compression/attributes/prediction_schemes/prediction_scheme_normal_octahedron_canonicalized_transform.h
# compression/attributes/prediction_schemes/prediction_scheme_normal_octahedron_transform.h
# compression/attributes/prediction_schemes/prediction_scheme_transform.h
# compression/attributes/prediction_schemes/prediction_scheme_wrap_transform.h
#####
# compression/attributes/attributes_decoder.h
# compression/attributes/attributes_encoder.h
# compression/attributes/kd_tree_attributes_decoder.h
# compression/attributes/kd_tree_attributes_encoder.h
# compression/attributes/kd_tree_attributes_shared.h
# compression/attributes/linear_sequencer.h
# compression/attributes/mesh_attribute_indices_encoding_data.h
# compression/attributes/mesh_attribute_indices_encoding_observer.h
# compression/attributes/mesh_traversal_sequencer.h
# compression/attributes/normal_compression_utils.h
# compression/attributes/points_sequencer.h
# compression/attributes/sequential_attribute_decoder.h
# compression/attributes/sequential_attribute_decoders_controller.h
# compression/attributes/sequential_attribute_encoder.h
# compression/attributes/sequential_attribute_encoders_controller.h
# compression/attributes/sequential_integer_attribute_decoder.h
# compression/attributes/sequential_integer_attribute_encoder.h
# compression/attributes/sequential_normal_attribute_decoder.h
# compression/attributes/sequential_normal_attribute_encoder.h
# compression/attributes/sequential_quantization_attribute_decoder.h
# compression/attributes/sequential_quantization_attribute_encoder.h
###

### Config ###
# compression/config/compression_shared.h
# namespace draco
# 
# # Latest Draco bit-stream version.
# static constexpr uint8_t kDracoBitstreamVersionMajor = 1;
# static constexpr uint8_t kDracoBitstreamVersionMinor = 2;
# 
# # Macro that converts the Draco bit-stream into one uint16_t number. Useful
# # mostly when checking version numbers.
# #define DRACO_BITSTREAM_VERSION(MAJOR, MINOR) \ ((static_cast<uint16_t>(MAJOR) << 8) | MINOR)
# 
# # Concatenated latest bit-stream version.
# static constexpr uint16_t kDracoBitstreamVersion = DRACO_BITSTREAM_VERSION(kDracoBitstreamVersionMajor, kDracoBitstreamVersionMinor);

# compression/config/compression_shared.h
# namespace draco
# # Draco header V1
# struct DracoHeader
#   int8_t draco_string[5];
#   uint8_t version_major;
#   uint8_t version_minor;
#   uint8_t encoder_type;
#   uint8_t encoder_method;
#   uint16_t flags;
###

# compression/config/encoding_features.h
# namespace draco
# namespace features
# 
# constexpr const char *kEdgebreaker = "standard_edgebreaker";
# constexpr const char *kPredictiveEdgebreaker = "predictive_edgebreaker";
###

### mesh ###
# compression/mesh/mesh_decoder.h
# compression/mesh/mesh_decoder_helpers.h
# compression/mesh/mesh_edgebreaker_decoder.h
# compression/mesh/mesh_edgebreaker_decoder_impl.h
# compression/mesh/mesh_edgebreaker_decoder_impl_interface.h
# compression/mesh/mesh_edgebreaker_encoder.h
# compression/mesh/mesh_edgebreaker_encoder_impl.h
# compression/mesh/mesh_edgebreaker_encoder_impl_interface.h
# compression/mesh/mesh_edgebreaker_shared.h
# compression/mesh/mesh_edgebreaker_traversal_decoder.h
# compression/mesh/mesh_edgebreaker_traversal_encoder.h
# compression/mesh/mesh_edgebreaker_traversal_predictive_decoder.h
# compression/mesh/mesh_edgebreaker_traversal_predictive_encoder.h
# compression/mesh/mesh_edgebreaker_traversal_valence_decoder.h
# compression/mesh/mesh_edgebreaker_traversal_valence_encoder.h
# compression/mesh/mesh_encoder.h
# compression/mesh/mesh_encoder_helpers.h
# compression/mesh/mesh_sequential_decoder.h
# compression/mesh/mesh_sequential_encoder.h
###

### point_cloud ###
# geometry_attribute.h
# geometry_indices.h
# point_attribute.h
# point_cloud.h
# point_cloud_builder.h
###


###############################################################################
# Enum
###############################################################################


# compression/attributekd_tree_attributes_shared.h
# Defines types of kD-tree compression
cdef extern from "draco/compression/attributes/kd_tree_attributes_shared.h" namespace "draco" nogil:
    cdef enum KdTreeAttributesEncodingMethod:
        kKdTreeQuantizationEncoding = 0
        kKdTreeIntegerEncoding


###

# compression/config/compression_shared.h
# namespace draco
# Currently, we support point cloud and triangular mesh encoding.
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum EncodedGeometryType:
        INVALID_GEOMETRY_TYPE = -1
        POINT_CLOUD = 0
        TRIANGULAR_MESH
###

# compression/config/compression_shared.h
# namespace draco
# # List of encoding methods for point clouds.
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum PointCloudEncodingMethod:
        POINT_CLOUD_SEQUENTIAL_ENCODING = 0
        POINT_CLOUD_KD_TREE_ENCODING


###

# compression/config/compression_shared.h
# namespace draco
# # List of encoding methods for meshes.
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum MeshEncoderMethod:
        MESH_SEQUENTIAL_ENCODING = 0
        MESH_EDGEBREAKER_ENCODING


###

# compression/config/compression_shared.h
# namespace draco
# # List of various attribute encoders supported by our framework. The entries
# # are used as unique identifiers of the encoders and their values should not
# # be changed!
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum AttributeEncoderType:
        BASIC_ATTRIBUTE_ENCODER = 0
        MESH_TRAVERSAL_ATTRIBUTE_ENCODER
        KD_TREE_ATTRIBUTE_ENCODER


###

# compression/config/compression_shared.h
# namespace draco
# # List of various sequential attribute encoder/decoders that can be used in our
# # pipeline. The values represent unique identifiers used by the decoder and
# # they should not be changed.
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum SequentialAttributeEncoderType:
        SEQUENTIAL_ATTRIBUTE_ENCODER_GENERIC = 0
        SEQUENTIAL_ATTRIBUTE_ENCODER_INTEGER
        SEQUENTIAL_ATTRIBUTE_ENCODER_QUANTIZATION
        SEQUENTIAL_ATTRIBUTE_ENCODER_NORMALS


###

# compression/config/compression_shared.h
# namespace draco
# # List of all prediction methods currently supported by our framework.
# enum PredictionSchemeMethod
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum PredictionSchemeMethod:
        # Special value indicating that no prediction scheme was used.
        PREDICTION_NONE = -2
        # Used when no specific prediction scheme is required.
        PREDICTION_UNDEFINED = -1
        PREDICTION_DIFFERENCE = 0
        MESH_PREDICTION_PARALLELOGRAM = 1
        MESH_PREDICTION_MULTI_PARALLELOGRAM = 2
        MESH_PREDICTION_TEX_COORDS_DEPRECATED = 3
        MESH_PREDICTION_CONSTRAINED_MULTI_PARALLELOGRAM = 4
        MESH_PREDICTION_TEX_COORDS_PORTABLE = 5,
        MESH_PREDICTION_GEOMETRIC_NORMAL = 6,
        NUM_PREDICTION_SCHEMES


###

# compression/config/compression_shared.h
# namespace draco
# # List of all prediction scheme transforms used by our framework.
# enum PredictionSchemeTransformType
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum PredictionSchemeTransformType:
        PREDICTION_TRANSFORM_NONE = -1
        # Basic delta transform where the prediction is computed as difference the
        # predicted and original value.
        PREDICTION_TRANSFORM_DELTA = 0
        # An improved delta transform where all computed delta values are wrapped
        # around a fixed interval which lowers the entropy.
        PREDICTION_TRANSFORM_WRAP = 1
        # Specialized transform for normal coordinates using inverted tiles.
        PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON = 2
        # Specialized transform for normal coordinates using canonicalized inverted tiles.
        PREDICTION_TRANSFORM_NORMAL_OCTAHEDRON_CANONICALIZED = 3


###

# compression/config/compression_shared.h
# namespace draco
# # List of all mesh traversal methods supported by Draco framework.
# enum MeshTraversalMethod
cdef extern from "draco/compression/config/compression_shared.h" namespace "draco":
    cdef enum MeshTraversalMethod:
        MESH_TRAVERSAL_DEPTH_FIRST = 0
        MESH_TRAVERSAL_PREDICTION_DEGREE = 1
        MESH_TRAVERSAL_RESERVED_1 = 2
        MESH_TRAVERSAL_RESERVED_2 = 3


###