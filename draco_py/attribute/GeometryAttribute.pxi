# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
cimport _draco

cdef class GeometryAttribute:
    """
    GeometryAttribute 
    """
    def __cinit__(self, init=None):
        """Makes a function memoizing the result for each argument and device.

        This decorator provides automatic memoization of the function result.

        Args:
            for_each_device (bool): If ``True``, it memoizes the results for each
                device. Otherwise, it memoizes the results only based on the
                arguments.
        """
        if init is None:
            self.geometry = new attribute.GeometryAttribute()
        else:
            raise TypeError("Can't initialize a GeometryAttribute from a %s" % type(init))

    # Init
    def Init(self, attribute_type, _draco.DataBuffer buffer, int num_components, data_type, bool normalized, int byte_stride, int byte_offset):
        if buffer is None:
            # self.geometry.Init(<attribute.Type2>(attribute_type), <core.DataBuffer*>(None), num_components, <core.DataType>(data_type), normalized, byte_stride, byte_offset)
            self.geometry.Init(<attribute.Type2>(attribute_type), <core.DataBuffer*>(0), num_components, <core.DataType>(data_type), normalized, byte_stride, byte_offset)
        else:
            self.geometry.Init(<attribute.Type2>(attribute_type), buffer.module, num_components, <core.DataType>(data_type), normalized, byte_stride, byte_offset)


    # bool
    def IsValid(self):
        self.geometry.IsValid()


    # def ConvertValueFloatPointer(self, int a, float* out_b):
    #     return self.attr.ConvertValue[float, 3](a, out_b)


###

