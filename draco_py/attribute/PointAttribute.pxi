# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool
cimport _draco

# http://cython.readthedocs.io/en/latest/src/tutorial/array.html
from cpython cimport array
import array

cdef extern from "attr_draco.h":
    int mapped_index2(attribute.PointAttribute* pointAttr, int index) except +
    bool ConvertValueFloatPointer(attribute.GeometryAttribute* geoAttr, int a, float* b) except +
    # bool ConvertValueFloatPointer(attribute.PointAttribute* pointAttr, int a, float* b) except +


cdef class PointAttribute:
    """
    Class for storing point specific data about each attribute. In general,
    multiple points stored in a point cloud can share the same attribute value
    and this class provides the necessary mapping between point ids and attribute
    value ids.
    """
    def __cinit__(self, init=None):
        if init is None:
            self.attr = new attribute.PointAttribute()
            pass
        # elif isinstance(init, PointAttribute):
        #     self = 
        elif isinstance(init, GeometryAttribute):
            # NG
            # self.attr = new point_cloud.PointAttribute(init)
            self.SetGeometryAttribute(init)
        else:
            raise TypeError("Can't initialize a PointAttribute from a %s" % type(init))


    def Init(self, attribute_type, _draco.DataBuffer buffer, int num_components, data_type, bool normalized, int byte_stride, int byte_offset):
        if buffer is None:
            (<attribute.GeometryAttribute*>self.attr).Init(<attribute.Type2>(attribute_type), <core.DataBuffer*>(0), num_components, <core.DataType>(data_type), normalized, byte_stride, byte_offset)
        else:
            (<attribute.GeometryAttribute*>self.attr).Init(<attribute.Type2>(attribute_type), buffer.module, num_components, <core.DataType>(data_type), normalized, byte_stride, byte_offset)


    def SetGeometryAttribute(self, GeometryAttribute geometry):
        """
        """
        # self.attr = new attribute.PointAttribute(deref(geometry.geometry))
        # self.attr = new attribute.PointAttribute_Arg1(deref(geometry.geometry))
        pass


    # Geometry Functions
    # def Custom_id(self):
    #     """
    #     """
    #     return (<attribute.GeometryAttribute*>self.attr).custom_id()

    def Attribute_type(self):
        """
        """
        return (<attribute.GeometryAttribute*>self.attr).attribute_type()

    def GetSize(self):
        """
        Get Attribute Size
        """
        return self.attr.size()


    def Data_type(self):
        return (<attribute.GeometryAttribute*> self.attr).data_type()


#     def Custom_id(self):
#         """
#         Get Custom_id
#         """
#         return (<attribute.GeometryAttribute*> self.attr).custom_id()


    def GetAttributeTransformData(self):
        # cdef attribute.AttributeTransformData* attrTData
        # data = (<attribute.PointAttribute*> self.attr).GetAttributeTransformData()
        # return attrTData
        pass
        # cdef attribute.AttributeTransformData* attrTData
        # data = AttributeTransformData()
        # attrTData = (<attribute.PointAttribute*> self.attr).GetAttributeTransformData()
        # data.attrTdata = attrTData
        # return data


    # def CopyFrom(self, const PointAttribute &src_att):
    #     # self.attr.CopyFrom(src_att)
    #     pass


    # Prepares the attribute storage for the specified number of entries.
    # bool Reset(size_t num_attribute_values);
    # bool
    def Reset(self, size_t num_attribute_values):
        # return (<attribute.PointAttribute*> self.attr).Reset(num_attribute_values)
        return self.attr.Reset(num_attribute_values)


    # AttributeValueIndex
    def mapped_index(self, point_index):
        # cdef attribute.AttributeValueIndex attr_value
        # cdef attribute.PointIndex point_value
        # attr_value = self.attr.mapped_index(point_value)
        # wrapper c++/h
        return mapped_index2(self.attr, point_index)

    # Core.Buffer
    def GetBuffer(self):
        # return self.attr.buffer()
        pass

    # bool
    def is_mapping_identity(self):
        return self.attr.is_mapping_identity()

    # size_t
    def indices_map_size(self):
        return self.attr.indices_map_size()

    # const uint8_t *
    # def GetAddressOfMappedIndex(self, PointIndex point_index):
    #     # return self.attr.GetAddressOfMappedIndex(point_index)
    #     pass


    # void
    def Resize(self, size_t new_num_unique_entries):
        # (<attribute.PointAttribute*> self.attr).Resize(new_num_unique_entries)
        self.attr.Resize(new_num_unique_entries)


    # Functions for setting the type of mapping between point indices and attribute entry ids.
    # This function sets the mapping to implicit, where point indices are equal to attribute entry indices.
    def SetIdentityMapping(self):
        # (<attribute.PointAttribute*> self.attr).SetIdentityMapping()
        self.attr.SetIdentityMapping()


    # This function sets the mapping to be explicitly using the indices_map_
    # array that needs to be initialized by the caller.
    def SetExplicitMapping(self, size_t num_points):
        # (<attribute.PointAttribute*> self.attr).SetExplicitMapping(num_points)
        self.attr.SetExplicitMapping(num_points)


    # Set an explicit map entry for a specific point index.
    # void SetPointMapEntry(PointIndex point_index, AttributeValueIndex entry_index)
    # def SetPointMapEntry(self, PointIndex point_index, AttributeValueIndex entry_index):
    #     # self.attr.SetPointMapEntry(point_index, entry_index)
    #     pass


    # Sets a value of an attribute entry. The input value must be allocated to
    # cover all components of a single attribute entry.
    # void SetAttributeValue(AttributeValueIndex entry_index, const void *value)
    # def SetAttributeValue(self, AttributeValueIndex entry_index, const void *value):
    #     # self.attr.SetAttributeValue(entry_index, <void *>(value))
    #     pass

    # Same as GeometryAttribute::GetValue(), but using point id as the input.
    # Mapping to attribute value index is performed automatically.
    # void GetMappedValue(PointIndex point_index, void *out_data) const
    # def GetMappedValue(self, PointIndex point_index, void *out_data):
    #     # self.attr.GetMappedValue(self, point_index, <void *>(out_data))
    #     pass

    # Deduplicate |in_att| values into |this| attribute. |in_att| can be equal to |this|.
    # Returns -1 if the deduplication failed.
    # AttributeValueIndex::ValueType DeduplicateValues(const GeometryAttribute &in_att);
    # AttributeValueIndex::ValueType 
    # def DeduplicateValues(self, const GeometryAttribute &in_att):
    #     # return self.attr.DeduplicateValues(in_att)
    #     pass

    # Same as above but the values read from |in_att| are sampled with the
    # provided offset |in_att_offset|.
    # AttributeValueIndex::ValueType DeduplicateValues(const GeometryAttribute &in_att, AttributeValueIndex in_att_offset)
    # AttributeValueIndex::ValueType
    # def DeduplicateValues(self, const GeometryAttribute &in_att, AttributeValueIndex in_att_offset):
    #     # return self.attr.DeduplicateValues(in_att, in_att_offset)
    #     pass

    # Set attribute transform data for the attribute. The data is used to store
    # the type and parameters of the transform that is applied on the attribute data (optional).
    # void SetAttributeTransformData(std::unique_ptr<AttributeTransformData> transform_data)
    # def SetAttributeTransformData(self, unique_ptr[AttributeTransformData] transform_data):
    #     # self.attr.DeduplicateValues(transform_data)
    #     pass

    # const AttributeTransformData *GetAttributeTransformData() const
    # const AttributeTransformData* GetAttributeTransformData()
    # const AttributeTransformData*
    def GetAttributeTransformData(self):
        # AttributeTransformData* att_data
        # return self.attr.GetAttributeTransformData()
        self.attr.GetAttributeTransformData()


    def ConvertValueFloatPointer2(self, a, out_b):
        # https://stackoverflow.com/questions/25102409/c-malloc-array-pointer-return-in-cython
        # http://cython.readthedocs.io/en/latest/src/tutorial/array.html
        cdef array.array float_params = array.array('f', [0, 0, 0])
        # access underlying pointer:
        # print(float_params.data.as_floats[0])
        # from libc.string cimport memset
        # memset(float_params.data.as_voidptr, 0, len(float_params) * sizeof(float))

        cdef attribute.GeometryAttribute* geo_attr
        geo_attr = (<attribute.GeometryAttribute*> self.attr)
        # isConvert = geo_attr.ConvertValue [float, attribute.THREE](a, float_params)
        # isConvert = geo_attr.ConvertValue [float](<attribute.AttributeValueIndex>a, float_params)
        # isConvert = geo_attr.ConvertValue [float](attr_value, float_params)
        # return isConvert
        # wrapper c++/h
        isFlag = ConvertValueFloatPointer(geo_attr, <int>a, <float*>float_params.data.as_voidptr)
        # isFlag = ConvertValueFloatPointer(self.attr, <int>a, float_params)
        # print(float_params)
        out_b = float_params
        return float_params


    # def ConvertValue(self, int a, float* ):
    #     return (<attribute.GeometryAttribute*> (self.attr)).ConvertValue


###

