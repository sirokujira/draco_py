# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud

from libcpp cimport bool

cdef class AttributeTransformData:
    """
    """
    def __cinit__(self, init=None):
        if init is None:
            # self.attrTdata = new attribute.AttributeTransformData()
            pass
        elif isinstance(init, GeometryAttribute):
            # NG
            # self.attrTdata = new attribute.PointAttribute(init)
            # self.SetGeometryAttribute(init)
            pass
        else:
            raise TypeError("Can't initialize a AttributeTransformData from a %s" % type(init))


    def transform_type(self):
        return self.attrTdata.transform_type()


    def set_transform_type(self, type):
        """
        """
        self.attrTdata.set_transform_type(<attribute.AttributeTransformType> type)


    # def GetParameterValue(self):
    #     """
    #     """
    #     
    #     return self.attrTdata.GetParameterValue(byte_offset)


    # def SetParameterValue(self):
    #     return self.attrTdata.SetParameterValue()


    # def AppendParameterValue(self):
    #     self.attrTdata.AppendParameterValue [DataTypeT](const DataTypeT &in_data)

###

