# -*- coding: utf-8 -*-
import numpy as np
cimport numpy as cnp

cnp.import_array()

# parts
cimport draco_attribute_defs as attribute
cimport draco_compress_defs as compress
cimport draco_core_defs as core
cimport draco_io_defs as io
cimport draco_mesh_defs as mesh
cimport draco_point_cloud_defs as point_cloud
cimport draco_metadata_defs as metadata

from libcpp cimport bool
from libcpp.memory cimport unique_ptr


cdef class PointCloud:
    """
    """
    def __cinit__(self, init=None):
        if init is None:
            self.points = new point_cloud.PointCloud()
        elif isinstance(init, DecoderBuffer):
            self.decode = new compress.Decoder()
            self.Decode(init)
        else:
            raise TypeError("Can't initialize a PointCloud from a %s" % type(init))


    # custom?
    def Decode(self, DecoderBuffer buffer):
        """
        """
        cdef unique_ptr[point_cloud.PointCloud] unique_point
        # unique_point = compress.DecodePointCloudFromBuffer(buffer.module)
        # unique_point = self.decode.DecodePointCloudFromBuffer(buffer.module)
        # StatusOr
        unique_point = (self.decode.DecodePointCloudFromBuffer(buffer.module)).value()
        
        # NG
        # self.points = <point_cloud.PointCloud*>unique_point.get()
        # pointer manage move
        self.points = unique_point.release()


    # base class PointCloud methods
    def num_attributes(self):
        """
        """
        return self.points.num_attributes ()


    def NumNamedAttributes(self, int type):
        """
        """
        self.points.NumNamedAttributes (<attribute.Type2> type)


    def GetNamedAttribute(self, type):
        point_attr = PointAttribute()
        point_attr.attr = <attribute.PointAttribute*>(self.points.GetNamedAttribute(<attribute.Type2> type))
        return point_attr


    def GetNamedAttributeId(self, type):
        """
        """
        return self.points.GetNamedAttributeId (<attribute.Type2> type)


    def GetNamedAttributeId(self, int type, int i):
        """
        """
        return self.points.GetNamedAttributeId_Index (<attribute.Type2> type, i)


    def num_points(self):
        """
        Get PointDataCount
        """
        return self.points.num_points ()


#     def AddAttribute(self, pa):
#         """
#         """
#         # return self.points.AddAttribute(unique_ptr[attribute.PointAttribute] pa.attr)
#         return self.points.AddAttribute(pa.attr)


    def AddMetadata(self, GeometryMetadata geo_metadata):
        # cdef unique_ptr[point_cloud.PointCloud] unique_point
        # cdef unique_ptr[metadata.GeometryMetadata] tmpMetadata
        # tmpMetadata = unique_ptr[metadata.GeometryMetadata](geo_metadata.meta)
        # tmpMetadata = deref(new unique_ptr[metadata.GeometryMetadata](geo_metadata.meta))
        # self.points.AddMetadata(tmpMetadata)
        self.points.AddMetadata(<unique_ptr[metadata.GeometryMetadata]> geo_metadata.meta)


    def GetMetadata(self):
        geo_meta = GeometryMetadata()
        geo_meta.meta = self.points.metadata()
        return geo_meta


###

