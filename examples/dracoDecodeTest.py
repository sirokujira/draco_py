# python Test
import os
import sys
# import draco
import draco_py as draco
import argparse

parser = argparse.ArgumentParser(description='draco example: dracoDecodeTest')
parser.add_argument('path_root_src',
                    action='store',
                    nargs=None,
                    const=None,
                    default=None,
                    type=str,
                    choices=None,
                    help='Directory path where your taken drc files are located.',
                    metavar=None)
parser.add_argument('-o', '--path-root-dst',
                    action='store',
                    nargs='?',
                    const='test_nm.obj.sequential.0.10.0.ply',
                    default='test_nm.obj.sequential.0.10.0.ply',
                    type=str,
                    choices=None,
                    help='Directory path where you want to create date folder and locate obj/ply files. (default: same as source directory)',
                    metavar=None)

# parser.add_argument('--Help',
#                     help=
#                     'Usage: dracoDecodeTest.py [options] input\n\n'
#                     'Options:\n'
#                     '     -h :                    show help.\n'
#                     '     -o : <output>           output file name.\n')

args = parser.parse_args()

# path = '/home/sirokujira/draco/testdata/test_nm.obj.sequential.0.10.0.drc'
# path = 'E:/draco/testdata/test_nm.obj.sequential.0.10.0.drc'
# path = 'E:/draco/testdata/test_nm.obj.edgebreaker.0.10.0.drc'
path = args.path_root_src
mesh = draco.GetDecodedData(path)
# infile = open(path, 'rb')
# data = infile.read()
# data_len = len(data)
#
# buffer = draco.DecoderBuffer()
# buffer.Init(data)
# geom_type = draco.GetEncodedGeometryType(buffer)
#
# mesh = None
#
# # mesh = draco.DecodeMeshFromBuffer(buffer)
# # pc = DecodePointCloudFromBuffer(buffer)
# # print(geom_type)
# if geom_type == draco.EncodedGeometryType_TRIANGULAR_MESH:
#     mesh = draco.DecodeMeshFromBuffer(buffer)
#     # mesh = draco.Mesh(buffer)
#     # print(mesh)
# elif geom_type == draco.EncodedGeometryType_POINT_CLOUD:
#     pc = draco.DecodePointCloudFromBuffer(buffer)
#     # print(pc)
# else:
#     exit(-1)

# test_nm.obj.sequential.0.10.0.obj
# obj_output = b'test_nm.obj.sequential.0.10.0.obj'
# obj_output = b'test_nm.obj.sequential.0.10.0.ply'
# print(args.path_root_dst)
obj_output = args.path_root_dst

# convert binary
obj_output = obj_output.encode(sys.getfilesystemencoding())

root, extension = os.path.splitext(obj_output)

# print(extension)
# print(mesh.Num_points())
# print(mesh.Num_Arrtibutes())
# print(pc.Num_points())
# print(pc.Num_Arrtibutes())

# OBJ
if extension == b'.obj':
    obj_encoder = draco.ObjEncoder()
    if mesh is not None:
        # NG(Exception)
        if obj_encoder.EncodeToFile_Mesh(mesh, obj_output) == False:
            print("Failed to store the decoded mesh as OBJ.\n")
            exit(-1)
    else:
        if obj_encoder.EncodeToFile_PointCloud(pc, obj_output) == False:
            print("Failed to store the decoded point cloud as OBJ.\n")
            exit(-1)
# PLY
elif extension == b'.ply':
    ply_encoder = draco.PlyEncoder()
    if mesh is not None:
        if ply_encoder.EncodeToFile_Mesh(mesh, obj_output) == False:
            print("Failed to store the decoded mesh as PLY.\n")
            exit(-1)
    else:
        if ply_encoder.EncodeToFile_PointCloud(pc, obj_output) == False:
            print("Failed to store the decoded point cloud as PLY.\n")
            exit(-1)

# print("Decoded geometry saved to %s (%" PRId64 " ms to decode)\n", obj_output, timer.GetInMs())
