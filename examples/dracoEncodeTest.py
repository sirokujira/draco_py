# python Test
import os
import sys
# import draco
import draco_py as draco
import argparse

parser = argparse.ArgumentParser(description='draco example: dracoEncodeTest')
parser.add_argument('path_root_src',
                    action='store',
                    nargs=None,
                    const=None,
                    default=None,
                    type=str,
                    choices=None,
                    help='Directory path where your taken obj/ply files are located.',
                    metavar=None)
parser.add_argument('-o', '--path-root-dst',
                    action='store',
                    nargs='?',
                    const='test.drc',
                    default='test.drc',
                    type=str,
                    choices=None,
                    help='Directory path where you want to create date folder and locate drc files. (default: same as source directory)',
                    metavar=None)

parser.add_argument('-point_cloud', default=False, type=bool,
                    help='-point_cloud forces the input to be encoded as a point cloud.')
parser.add_argument('-qp', default=14, type=int,
                    help='-qp <value> quantization bits for the position attribute, default=14.')
parser.add_argument('-qt', default=12, type=int,
                    help='-qt <value> quantization bits for the position attribute, default=12.')
parser.add_argument('-qn', default=10, type=int,
                    help='-qn <value> quantization bits for the position attribute, default=10.')
parser.add_argument('-cl', default=7, type=float,
                    help='-cl <value> compression level [0-10], most=10, least=0, default=7.')
parser.add_argument('-qg', default=8, type=float,
                    help='-qg <value> quantization bits for any generic attribute, default=8.')

args = parser.parse_args()

# inputpath = 'E:/draco/testdata/bun_zipper.ply'
inputpath = args.path_root_src
# convert binary
inputpath = inputpath.encode(sys.getfilesystemencoding())

is_point_cloud = args.point_cloud
pos_quantization_bits = args.qp
tex_coords_quantization_bits = args.qt
normals_quantization_bits = args.qn
generic_quantization_bits = args.qg
compression_level = args.cl

# Options options;
#     : is_point_cloud(false),
#       pos_quantization_bits(14),
#       tex_coords_quantization_bits(12),
#       normals_quantization_bits(10),
#       compression_level(7) {}


def PrintOptions(pc):
    print('Encoder options: ')
    print('  Compression level = ' + str(compression_level))
    if pos_quantization_bits <= 0:
        print('  Positions: No quantization')
    else:
        print('  Positions: Quantization = ' +
              str(pos_quantization_bits) + ' bits')

    if pc.GetNamedAttributeId(draco.GeometryAttributeType_TEX_COORD) >= 0:
        if tex_coords_quantization_bits <= 0:
            print('  Texture coordinates: No quantization')
        else:
            print('  Texture coordinates: Quantization = ' +
                  str(tex_coords_quantization_bits) + ' bits')
    else:
        pass

    if pc.GetNamedAttributeId(draco.GeometryAttributeType_NORMAL) >= 0:
        if normals_quantization_bits <= 0:
            print('  Normals: No quantization')
        else:
            print('  Normals: Quantization = ' +
                  str(normals_quantization_bits) + ' bits')
    else:
        pass

    if pc.GetNamedAttributeId(draco.GeometryAttributeType_GENERIC) >= 0:
        if generic_quantization_bits <= 0:
            print('  Generic: No quantization')
        else:
            print('  Generic: Quantization = ' +
                  str(generic_quantization_bits) + ' bits')
    else:
        pass


# int
# def EncodePointCloudToFile(draco.PointCloud pc, draco.EncoderOptions options, string file):
# def EncodePointCloudToFile(pc, options, filepath):
def EncodePointCloudToFile(pc, filepath, encoder):
    # Encode the geometry.
    # buffer = draco.EncoderBuffer()
    # if draco.EncodePointCloudToBuffer(pc, options, buffer) == False:
    #     print('Failed to encode the point cloud.');
    #     return -1
    buffer = encoder.EncodePointCloudToBuffer(pc)

    # Save the encoded geometry into a file.
    # std::ofstream out_file(file, std::ios::binary);
    # if (!out_file) {
    #   print('Failed to create the output file.');
    #   return -1;
    # }
    out_file = open(filepath, 'wb')
    out_file.write(buffer.GetData(), buffer.GetSize())

    # print('Encoded point cloud saved to %s (%' PRId64 ' ms to encode)', file.c_str(), timer.GetInMs());
    # print('\nEncoded size = %zu bytes\n', buffer.size());
    return 0


# int
# def EncodeMeshToFile(const draco::Mesh &mesh, const draco::EncoderOptions &options, const std::string &file):
def EncodeMeshToFile(mesh, filepath, encoder):
    print('EncodeMeshToFile() - 1.')
    # Encode the geometry.
    # buffer = draco.EncoderBuffer()
    # if draco.EncodeMeshToBuffer(mesh, options, buffer) == False:
    #     print('Failed to encode the mesh.')
    #     return -1
    print('num_faces = ' + str(mesh.num_faces()))
    print('num_Attributes = ' + str(mesh.num_attributes()))
    print('num_points = ' + str(mesh.num_points()))

    # buffer = draco.EncodeMeshToBuffer(mesh, options)
    buffer = encoder.EncodeMeshToBuffer(mesh)

    # Save the encoded geometry into a file.
    # std::ofstream out_file(file, std::ios::binary);
    # if (!out_file) {
    #   print('Failed to create the output file.');
    #   return -1;
    # }

    # print('EncoderBufferSize = ' + str(buffer.GetSize()))
    # print('EncoderBufferData = ' + str(buffer.GetData()))

    out_file = open(filepath, 'wb')

    size = buffer.GetSize()
    data = buffer.GetData()

    # buffer.WriteFile(filepath)

    out_file.write(data)
    # out_file.write(buffer.GetData())
    # out_file.write(buffer.GetData()[:size])
    out_file.close

    # print('Encoded mesh saved to ' + file.c_str() + ' (' + timer.GetInMs() + ' PRId64 ' ms to encode)');
    # print('\nEncoded size = ' + buffer.size() + ' bytes\n')
    return 0

# main
# const int argc_check = argc - 1;
# argc_check = argc - 1
# Check Invalid
# Start
# End


pc = None
# draco::Mesh *mesh = nullptr;
mesh = None

print(mesh)
print(pc)

if is_point_cloud == False:
    # std::unique_ptr<draco::Mesh> in_mesh = draco::ReadMeshFromFile(options.input);
    # if !in_mesh:
    #     print('Failed loading the input mesh.')
    #     return -1
    #     exit(-1)
    # in_mesh = draco.MeshIO.ReadMeshFromFile(inputpath)
    # mesh = in_mesh.get()
    # pc = std::move(in_mesh)
    mesh = draco.ReadMeshFromFile(inputpath)
    pc = mesh.GetPointCloud()
else:
    pc = draco.ReadPointCloudFromFile(inputpath)
    # if !pc
    #     print('Failed loading the input point cloud.')
    #     return -1
    # pass

# Setup encoder options.
# draco.EncoderOptions encoder_options = draco.CreateDefaultEncoderOptions()
print('draco.dracoEncoderOptions() - enter.')
encoder_options = draco.dracoEncoderOptions()
print('draco.dracoEncoderOptions() - exit.')

print(encoder_options.GetEncodingSpeed())
print(encoder_options.GetDecodingSpeed())
print(encoder_options.GetSpeed())
print(mesh)
print(pc)

encoder = draco.Encoder()
print('1 : pos_quantization_bits = ' + str(pos_quantization_bits))
if pos_quantization_bits > 0:
    encoder.SetAttributeQuantization(
        draco.GeometryAttributeType_POSITION, pos_quantization_bits)

print('2 : tex_coords_quantization_bits = ' + str(tex_coords_quantization_bits))
if tex_coords_quantization_bits > 0:
    encoder.SetAttributeQuantization(
        draco.GeometryAttributeType_TEX_COORD, tex_coords_quantization_bits)

print('3 : normals_quantization_bits = ' + str(normals_quantization_bits))
if normals_quantization_bits > 31:
    encoder.SetAttributeQuantization(
        draco.GeometryAttributeType_NORMAL, normals_quantization_bits)

print('4 : generic_quantization_bits = ' + str(generic_quantization_bits))
if generic_quantization_bits > 31:
    encoder.SetAttributeQuantization(
        pc, draco.GeometryAttributeType_GENERIC, generic_quantization_bits)


# Convert compression level to speed (that 0 = slowest, 10 = fastest).
# const int speed = 10 - options.compression_level;
# draco::SetSpeedOptions(&encoder_options, speed, speed);
print('5 : compression_level = ' + str(compression_level))
speed = 10 - compression_level
encoder.SetSpeedOptions(speed, speed)

# outputpath = b'test.drc'
outputpath = args.path_root_dst
# convert binary
outputpath = outputpath.encode(sys.getfilesystemencoding())
# outputpath = b'test.drc'

# if options.output.empty():
#     // Create a default output file by attaching .drc to the input file name.
#     options.output = options.input + '.drc';

PrintOptions(pc)

ret = -1
# if mesh != None && mesh->num_faces() > 0:
if mesh != None and mesh.num_faces() > 0:
    # ret = EncodeMeshToFile(mesh, encoder_options, outputpath)
    ret = EncodeMeshToFile(mesh, outputpath, encoder)
else:
    # ret = EncodePointCloudToFile(*pc.get(), outputpath, encoder)
    ret = EncodePointCloudToFile(pc, encoder_options, outputpath)

if ret != -1 and compression_level < 10:
    print('For better compression, increase the compression level -cl (up to ''10).\n')
