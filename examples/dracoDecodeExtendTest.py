# python Test
import os
import sys

import draco_py as draco
import draco_py.extension as draco_ext
import argparse

parser = argparse.ArgumentParser(description='draco example: dracoDecodeTest')
parser.add_argument('path_root_src',
                    action='store',
                    nargs=None,
                    const=None,
                    default=None,
                    type=str,
                    choices=None,
                    help='Directory path where your taken drc files are located.',
                    metavar=None)
parser.add_argument('-o', '--path-root-dst',
                    action='store',
                    nargs='?',
                    const='test_nm.obj.sequential.0.10.0.ply',
                    default='test_nm.obj.sequential.0.10.0.ply',
                    type=str,
                    choices=None,
                    help='Directory path where you want to create date folder and locate obj/ply files. (default: same as source directory)',
                    metavar=None)
# parser.add_argument('--Help',
#                     help=
#                     'Usage: dracoDecodeTest.py [options] input\n\n'
#                     'Options:\n'
#                     '     -h :                    show help.\n'
#                     '     -o : <output>           output file name.\n')

args = parser.parse_args()

# path = '/home/sirokujira/draco/testdata/test_nm.obj.sequential.0.10.0.drc'
# path = 'E:/draco/testdata/test_nm.obj.sequential.0.10.0.drc'
# path = 'E:/draco/testdata/test_nm.obj.edgebreaker.0.10.0.drc'
path = args.path_root_src
# mesh = draco.GetDecodedData(path)
point = draco_ext.convert_numpy_data(path)

# external viewer checked
import pcl
import numpy as np
from pcl import pcl_visualization
p = pcl.PointCloud()

# mean_param = np.concatenate([np.mean(point, 0)[0:3], np.zeros(1)])
# ptcloud_centred = point - mean_param
# print(ptcloud_centred)
# p.from_array(np.array(ptcloud_centred, dtype=np.float32))
p.from_array(np.array(point, dtype=np.float32))

## Visualization
visual = pcl_visualization.CloudViewing()
visual.ShowMonochromeCloud(p, b'cloud')
# visual.ShowColorCloud(p, b'cloud')

flag = True
while flag:
    flag != visual.WasStopped()
end
